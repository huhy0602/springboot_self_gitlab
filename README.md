# springboot_self
springboot的技术整合，作为公司技术架构设计开发基础

# 主要模块介绍：
    ##springboot_basedemo:RBAC模型的bootstarp实现
    ##springboot_common:集成springboot 常用工具集成,包含多数据的整合
    ##springboot_easyui:主要集成easyui形成可视化配置RBAC的框架
    ##springboot_email:发送邮件测试
    ##springboot_mongodb:主要做sqlserver到mongodb的数据迁移 ，工具方法的测试
    ##springboot_mybatis:springboot与mybatis的整合
    ##springboot_shiro:shiro的集成
    ##springboot_webflux：springboot2的新特性响应式框架
    ##springboot_dispense:主要实现mongodb到关系数据库的分发相应数据【不更新】
    ##springboot_webservice:webserice的整合
    ##springboot_hadoop:这是springboot与hadoop生态圈的整合运用,这个demo主要测试jdk8和hadoop一些中间件的使用已经api的测试
    ##springboot_jpa :  反向生成的代码
    ##springboot_rabbitmq:消息队列的安装与测试
    ##springboot_weupload:断点续传的实现，后期会继续添加
    ##springboot_echarts：echarts的运用
    ##springboot_nosqls:这是对nosql的整理，和一些其他项目上的攻关技术的测试
                  package:  
                        java8  :java8新特性的测试
                        redis  : redis的单机和集群的整合
                        properties: springboot读取自定义peoperties和yml文件的测试
    ##springboot_mybatisplus: 集成mybatisplus
#其他是属于测试demo