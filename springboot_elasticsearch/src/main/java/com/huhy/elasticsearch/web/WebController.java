package com.huhy.elasticsearch.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author : HUHY
 * @Project_name:springboot_self_gitlab
 * @date:2019/9/5 16:37
 * @email:hhy_0602@163.com
 * @description:{todo}
 */
@RestController
@RequestMapping("/user")
public class WebController {
    @Autowired
    private UserRepository userRepository;
    @PostMapping("")
    public User save1(@RequestBody User user){
        return userRepository.save(user);
    }

    @GetMapping("")
    public Iterable<User> findAll1(){
        return userRepository.findAll();
    }

    @GetMapping("/{name}")
    public User findOne1(@PathVariable String name){
        return userRepository.findByName(name);
    }
}
