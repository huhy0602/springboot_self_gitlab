package com.huhy.elasticsearch.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Date;

/**
 * @author : HUHY
 * @Project_name:springboot_self_gitlab
 * @date:2019/9/5 16:35
 * @email:hhy_0602@163.com
 * @description:{todo}
 */
@Data
@Document(indexName = "user", type = "test")
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    private String id;
    private String name;
    private int age = 18;
    private Date createTime = new Date();
}
