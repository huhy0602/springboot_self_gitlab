package com.huhy.elasticsearch.web;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author : HUHY
 * @Project_name:springboot_self_gitlab
 * @date:2019/9/5 16:36
 * @email:hhy_0602@163.com
 * @description:{todo}
 */
public interface UserRepository extends ElasticsearchRepository<User, String> {
    User findByName(String name);
}
