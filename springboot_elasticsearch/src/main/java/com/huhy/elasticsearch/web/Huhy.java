package com.huhy.elasticsearch.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @author : HUHY    http://www.cnblogs.com/huhongy/
 * @Project_name:springboot_self_gitlab
 * @date:2020/4/24 9:35
 * @email:hhy_0602@163.com
 * @description:{todo}
 */
@Data
@Document(indexName = "huhy", type = "yanfa")
@AllArgsConstructor
@NoArgsConstructor
public class Huhy {
    @Id
    private Integer id;
    private String name;
    private String city;
}
