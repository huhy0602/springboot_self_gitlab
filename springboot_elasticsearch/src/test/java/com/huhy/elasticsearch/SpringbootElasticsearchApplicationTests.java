package com.huhy.elasticsearch;

import com.huhy.elasticsearch.web.Huhy;
import com.huhy.elasticsearch.web.HuhyResiporty;
import com.huhy.elasticsearch.web.User;
import com.huhy.elasticsearch.web.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Iterator;

/**
 * @author HUHY  http://www.cnblogs.com/huhongy/
 * @Description: ${todo}
 * @return:  测试es7的方法
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootElasticsearchApplicationTests {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private HuhyResiporty huhyResiporty;
    @Test
    public void contextLoads() {
    }
    @Test
    public void test1(){
        User user = new User();
        user.setAge(24);
        user.setCreateTime(new Date());
        userRepository.save(user);
    }
    @Test
    public void test2(){
        Iterator<User> iterator = userRepository.findAll().iterator();
        User next = iterator.next();
        System.out.println(next);
    }

    @Test
    public void test3(){
        Huhy huhy = new Huhy();
        huhy.setCity("河南");
        huhy.setName("胡红阳");
        huhyResiporty.save(huhy);
    }

}
