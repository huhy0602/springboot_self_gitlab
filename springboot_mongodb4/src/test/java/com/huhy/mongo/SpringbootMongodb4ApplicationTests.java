package com.huhy.mongo;

import com.huhy.mongo.project.entity.Money;
import com.huhy.mongo.project.entity.User;
import com.huhy.mongo.project.service.UserService;
import com.mongodb.MongoClient;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootMongodb4ApplicationTests {
	@Autowired
	private MongoTemplate mongoTemplate;



	@Autowired
	private UserService userService;


    @Autowired
    private MongoClient mongoClient;
	@Test
	public void contextLoads() {
        System.out.println(mongoTemplate.findById("12",Money.class));
        MongoCollection<Document> money = mongoTemplate.getDb().getCollection("money");

    }

	@Test
	public void test1(){
        User user = new User("123", "huhy",18);
        User build = User.builder().age(1).build();
        User build1 = user.toBuilder().age(111).build();
        System.out.println(build1);
        System.out.println(userService.saveUser(user));
	}
    /**
     * @author huhy
     * @ClassName:SpringbootMongodb4ApplicationTests
     * @date 2018/11/23 11:18 
     * @Description: 测试事务
     */
	@Test
    public void  testInsert(){
        Money yang = Money.builder().id("12").name("huhy").pay(100).build();
        Money hui = Money.builder().id("13").name("hui").pay(0).build();
        List<Money> moneyList = new ArrayList<>();
        moneyList.add(yang);
        moneyList.add(hui);
        mongoTemplate.insertAll(moneyList);

    }
    /**
     * @author huhy
     * @ClassName:SpringbootMongodb4ApplicationTests
     * @date 2018/11/23 11:26 
     * @Description: 测试事务
     */
    @Test
    public void testTro(){
        int a = 1000;
        //获取ClientSession
        ClientSession clientSession = mongoClient.startSession();
        Money yang = mongoTemplate.findById("12", Money.class);
        Money hui = mongoTemplate.findById("13", Money.class);
        int pay = yang.getPay()- a;
        hui.setPay(pay);
        yang.setPay(pay);
        try {
            //开启事务
            clientSession.startTransaction();
            //为mongoTemplate注入的事务特性
            MongoTemplate mongoTemplate = this.mongoTemplate.withSession(clientSession);
            mongoTemplate.save(hui);
            mongoTemplate.save(yang);
            if (pay < 0 ){
                System.out.println("abortTransaction");
                //撤销事务
                clientSession.abortTransaction();
            }
        } catch (Exception e) {
            //提交事务
            clientSession.commitTransaction();
            e.printStackTrace();
        }
    }



}
