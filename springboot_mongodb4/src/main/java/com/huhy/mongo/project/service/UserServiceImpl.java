package com.huhy.mongo.project.service;

import com.huhy.mongo.project.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

/**
 * @author : huhy on 2018/11/23.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.mongo.project.service
 * @description:{todo}
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public User saveUser(User user) {

        //使用 save和insert都可以进行插入
        //区别：当存在"_id"时
        //insert 插入已经存在的id时 会异常
        //save 则会进行更新
        //简单来说 save 就是不存在插入 存在更新
        //mongoTemplate.insert(user);
        mongoTemplate.save(user);
        return null;
    }

    @Override
    public User findUserById(String id) {
        return null;
    }

    @Override
    public User delUserById(String id) {
        return null;
    }
}
