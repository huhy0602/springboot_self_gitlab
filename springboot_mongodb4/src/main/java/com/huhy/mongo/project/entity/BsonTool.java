package com.huhy.mongo.project.entity;

import org.bson.*;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author : huhy on 2018/11/23.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.mongo.project.entity
 * @description:{todo}
 */
public class BsonTool {

    /**
     * Java对象转BsonValue对象
     * @param obj
     * @return
     */
    public static BsonValue objectToBsonValue(Object obj){
        if (obj instanceof Integer){
            return new BsonInt32((Integer) obj);
        }

        if (obj instanceof String){
            return new BsonString((String) obj);
        }

        if (obj instanceof Long){
            return new BsonInt64((Long) obj);
        }

        if (obj instanceof Date){
            return new BsonDateTime(((Date) obj).getTime());
        }
        return new BsonNull();
    }
    public static Map getValue(Object thisObj)
    {
        Map map = new HashMap();
        Class c;
        try
        {
            c = Class.forName(thisObj.getClass().getName());
            Method[] m = c.getMethods();
            for (int i = 0; i < m.length; i++)
            {
                String method = m[i].getName();
                if (method.startsWith("get"))
                {
                    try{
                        Object value = m[i].invoke(thisObj);
                        if (value != null)
                        {
                            String key=method.substring(3);
                            key=key.substring(0,1).toUpperCase()+key.substring(1);
                            map.put(method, value);
                        }
                    }catch (Exception e) {
                        // TODO: handle exception
                        System.out.println("error:"+method);
                    }
                }
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
        return map;
    }

}
