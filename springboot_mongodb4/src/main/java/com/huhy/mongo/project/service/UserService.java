package com.huhy.mongo.project.service;

import com.huhy.mongo.project.entity.User;


/**
 * @author : huhy on 2018/11/23.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.mongo.project.service
 * @description:{todo}
 */
public interface UserService {

    /**
     * 保存数据
     */
    User saveUser(User user);

    /**
     * 根据消息号查找
     */
    User findUserById(String id);



    /**
     * 根据id进行删除 返回删除的对象
     */
    User delUserById(String id);
}
