package com.huhy.mongo.project.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @author : huhy on 2018/11/23.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.mongo.project.entity
 * @description:{todo}
 */
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Document("money")
public class Money  implements Serializable{
    @Id
    private String id;
    private int pay;
    private  String name;

}
