package com.huhy.mongo.project.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @author : huhy on 2018/11/23.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.mongo.project.entity
 * @description:{todo}
 */
@Data
@Document(collection="user_huhy")//集合名
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
public class User implements Serializable{
    @Id
    private  String id;
    private  String name;
    private  int age;

}
