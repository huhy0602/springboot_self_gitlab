package com.huhy.mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMongodb4Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootMongodb4Application.class, args);
	}
}
