package com.huhy.nosql;

import com.huhy.nosql.config.MemcachedRunner;
import net.spy.memcached.MemcachedClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootMemcachedApplicationTests {

    @Resource
    private MemcachedRunner memcachedRunner;


    @Test
	public void contextLoads() {
        MemcachedClient memcachedClient = memcachedRunner.getClient();
        memcachedClient.add("huhy",20,"this is test");
        System.out.println(memcachedClient.get("huhy"));
        memcachedClient.getStats();

	}

}
