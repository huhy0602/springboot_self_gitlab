package com.xf.xf_common;


import com.xf.xf_common.dispense.dao.DataOfTableDao;
import com.xf.xf_common.dispense.entity.DataOfTable;
import com.xf.xf_common.dispense.entity.JsonResult;
import com.xf.xf_common.dispense.entity.TempPro;
import com.xf.xf_common.dispense.utils.DispenseUtil;
import com.xf.xf_common.dispense.utils.StringUtils;
import com.xf.xf_common.etl.entity.Sl6512014;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootDispenseApplicationTests {
	@Autowired
	private MongoTemplate mongoTemplate;
	/*//项目集合对应【临时，将来是被其他数据库代替】
    @Value("${mongodb.temp.tempData}")
	private String tempData;*/
    //项目时间记录集合
    @Value("${mongodb.temp.tempPro}")
    private  String tempPro;

    @Value("${config.common.timespan}")
    private  Integer timespan;
    @Autowired
    private DispenseUtil dispenseUtil;
    @Autowired
    private DataOfTableDao dataOfTableDao;
	/**
	 * @author huhy
	 * @ClassName:SpringbootMongdbApplicationTests
	 * @date 2018/5/29 11:20
	 * @Description: 测试分发
	 */
	@Test
	public void testDispense(){
        String collectionAndDate = queryByCollectionAndDate();
        System.out.println(collectionAndDate);

    }
    /**
     * @author huhy
     * @ClassName:XfCommonApplicationTests
     * @date 2018/5/31 9:27 
     * @Description:做分发的情况模拟
     */
	public String queryByCollectionAndDate(){
        Map<String, List<Map<String,List<Sl6512014>>>> resultMaps = new HashMap<>();

		//从数据库中获取项目和集合的对应关系
        List<DataOfTable> dataOfTableList = dataOfTableDao.findAll();
        //对维护表的结果进行分析，然后根据站点做分发
        for (DataOfTable dataOfTable : dataOfTableList) {
            List<Map<String,List<Sl6512014>>> tempList = new ArrayList<>();
            Map<String,List<Sl6512014>> tempMap = new HashMap<>();
            //    proNo  --- siteNo1,siteNo2...
            //获取项目下对应的站点信息 "123,124,125"
            String siteNo = dataOfTable.getSiteNo();
            if(siteNo != null){
                //项目下对应的站点集合
                List<String> siteNos = StringUtils.spliteStrToList(siteNo);
                for (String no : siteNos) {
                    //先判断在mongodb的集合下是否存在该信息
                    boolean b = mongoTemplate.collectionExists(tempPro);
                    //获取当前集合下的最小时间
                    Date startDate = selectCollectionSttMinValue(no);
                    Query query = new Query(Criteria.where("proNo").is(dataOfTable.getProNo()).and("siteNo").is(no));
                    if(b){
                        //集合存在，获取集合下是否存在该对应数据
                       // Query query = new Query(Criteria.where("proNo").is(dataOfTable.getProNo()).and("siteNo").is(no));
                        List<TempPro> tempPros = mongoTemplate.find(query, TempPro.class, tempPro);
                        //判断是否存在这条记录
                        if(tempPros.size() > 0){
                            //记录存在  获取该时间段
                            //mongodb中记录的时间，也是项目号对应集合站点最后一次分发的时间
                            Date mongoDate = tempPros.get(0).getStartTime();
                            //通过指定东西获取的分发数据包
                            tempMap = getSiteInfoByCollectionNameAndDate(no, mongoDate,query);
                        }else{
                            //不存在
                            mongoTemplate.insert(new TempPro(dataOfTable.getProNo(),no,startDate),tempPro);
                            tempMap = getSiteInfoByCollectionNameAndDate(no, startDate,query);
                        }
                    }else {
                        //创建该集合
                        mongoTemplate.insert(new TempPro(dataOfTable.getProNo(),no,startDate),tempPro);
                        tempMap = getSiteInfoByCollectionNameAndDate(no, startDate,query);
                    }
                    tempList.add(tempMap);
                }
            }else{
                //项目下对应的站点暂时没配置，所以数据暂时不分发
                continue;
            }
            resultMaps.put(dataOfTable.getProNo(),tempList);
        }
        return StringUtils.objToJson(resultMaps);
	}
	/**
	 * @author huhy
	 * @ClassName:XfCommonApplicationTests
	 * @date 2018/5/31 8:38 
	 * @Description: 只通过起始时间，然后获取到一个时间区间
	 */
	public Map<String,Date> getstartTimeAndendTime(Date starTime) {
		//时间跨度
		//int num=15;
		Calendar calendar=Calendar.getInstance();
		calendar.setTime(starTime);
		calendar.add(Calendar.DAY_OF_MONTH,timespan);
		Date endTime;
		Date nowtime=new Date();
		if (calendar.getTime().getTime()>nowtime.getTime()){
			endTime=nowtime;
		}else {
			endTime=calendar.getTime();
		}
		Map<String,Date> map=new HashMap<>();
		map.put("startTime",starTime);
		map.put("endTime",endTime);
		return map;
	}
	/**
	 * @author huhy
	 * @ClassName:XfCommonApplicationTests
	 * @date 2018/5/31 8:40
	 * @Description: 根据集合名称获取当前站点下的最小时间
	 */
	public  Date selectCollectionSttMinValue(String collectionName){
		Date sttTime = null;
		Query query = new Query().with(new Sort(Sort.Direction.ASC,"stt"));
		List<Sl6512014> list = mongoTemplate.find(query,Sl6512014.class,collectionName);
		if(list != null && list.size() > 0){
			sttTime = list.get(0).getStt();
		}
		return sttTime;
	}

    /**
     * @author huhy
     * @ClassName:XfCommonApplicationTests
     * @date 2018/5/31 10:03 
     * @Description: 通过站点名称和时间查出对应的数据
     */
	public Map<String,List<Sl6512014>> getSiteInfoByCollectionNameAndDate(String collectionName,Date startDate,Query query){
	    Map<String,List<Sl6512014>> resultMap = new HashMap<>();
	    //通过起始时间获取查询的时间间隔，顺便把
        Map<String, Date> dateMap = getstartTimeAndendTime(startDate);
        Query query1 = new Query(Criteria.where("stt").lt(dateMap.get("endTime")).gte(dateMap.get("startTime")));
        List<Sl6512014> sl6512014s = mongoTemplate.find(query1, Sl6512014.class, collectionName);
        //往mongodb中插入一条记录这项目-站点-时间的文档
        Update update  = new Update().set("startTime",dateMap.get("endTime"));
        mongoTemplate.updateFirst(query,update,tempPro);
        resultMap.put(collectionName,sl6512014s);
        return resultMap;
    }


    @Test
    public void testGroup(){
        System.out.println(selectDataByGroupFildes("1","serial"));
    }
    /**
     * @author huhy
     * @ClassName:XfCommonApplicationTests
     * @date 2018/5/31 10:43 
     * @Description: 分组
     */
    public  List<Sl6512014> selectDataByGroupFildes(String collectionName,String... fildes){
        Aggregation agg = Aggregation.newAggregation(
                // 第一步：挑选所需的字段，类似select *，*所代表的字段内容
                Aggregation.project("_id", "serial", "ut", "st", "tt", "pj",
                        "pt", "va", "vt", "z","zt","sbl1","turb","stt"),
                // 第二步：sql where 语句筛选符合条件的记录
               /* Aggregation.match(
                        Criteria.where("companyName").is(companyName).and("addedDate").gte(startTime).lte(endTime)),*/
                // 第三步：分组条件，设置分组字段
                Aggregation.group(fildes)
                        .count().as("allCount")// 增加COUNT为分组后输出的字段
                        .last("_id").as("id").last("serial").as("serial").last("ut").as("ut")
                        .last("st").as("st").last("tt").as("tt").last("pj").as("pj")
                        .last("pt").as("pt").last("va").as("va")
                        .last("vt").as("vt").last("z").as("z")
                        .last("zt").as("zt").last("sbl1").as("sbl1")
                        .last("turb").as("turb").last("stt").as("stt"),
                // 第四步：重新挑选字段
                Aggregation.project("id", "serial", "ut", "st", "tt", "pj",
                        "pt", "va", "vt", "z","zt","sbl1","turb","stt")
        );
        AggregationResults<Sl6512014> results = mongoTemplate.aggregate(
                agg, collectionName, Sl6512014.class);
        List<Sl6512014> mappedResults = results.getMappedResults();
        return mappedResults;
    }

    /**
     * @author huhy
     * @ClassName:DisposeRequestFromMongodb
     * @date 2018/6/28 10:42
     * @Description: 处理用户的请求
     */
    @Test
    public void getData() throws Exception{
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
// Date -> String
        Date date = new Date(System.currentTimeMillis());
        System.out.println(format.format(date));
// String -> Date
        String timeString = "1999-06-22 18:00:00";
        Date newDate = format.parse(timeString);
        String proNo  = "001";
        String siteNO = "1";
        Date startDate = newDate;
        Date endDate = new Date();
        JsonResult dataFromMongodb = dispenseUtil.getSiteInfoByCollectionNameAndDate(proNo, siteNO, startDate, endDate);
        System.out.println(dataFromMongodb);
    }

}
