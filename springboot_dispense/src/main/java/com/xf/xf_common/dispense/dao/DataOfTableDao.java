package com.xf.xf_common.dispense.dao;

import com.xf.xf_common.config.annotation.TargetDataSource;
import com.xf.xf_common.dispense.entity.DataOfTable;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2018/5/30.
 *
 * @Project_name:xf_common
 * @LOCAL:com.xf.xf_common.dao
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@Repository
public interface DataOfTableDao {
    @TargetDataSource("sqlservertest")
    public List<DataOfTable> findAll();

    /**
     * @author huhy
     * @ClassName:DataOfTableDao
     * @date 2018/6/28 9:52
     * @Description: 查询出指定项目id下对应的站点信息
     */

    @TargetDataSource("sqlservertest")
    DataOfTable queryById(@Param("proNo") String proNo);
}
