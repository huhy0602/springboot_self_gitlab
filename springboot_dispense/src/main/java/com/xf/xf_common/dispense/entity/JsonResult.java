package com.xf.xf_common.dispense.entity;

import com.xf.xf_common.dispense.enums.ResultCode;
import lombok.Data;

/**
 * @author : huhy on 2018/6/28.
 * @Project_name:springboot_xf
 * @LOCAL:com.xf.water.entity
 * @blog : http://www.cnblog,com/huhongy/
 * @description:为响应的统一返回类
 */
@Data
public class JsonResult {
    private ResultCode code;
    private String message;
    private Object data;

    public JsonResult() {
        this.setCode(ResultCode.SUCCESS);
        this.setMessage("成功！");
    }

    public JsonResult(ResultCode code) {
        this.setCode(code);
        this.setMessage(code.getMsg());
    }

    public JsonResult(ResultCode code, Object data) {
        this.setCode(code);
        this.setMessage(code.getMsg());
        this.setData(data);
    }

    public JsonResult(ResultCode code, String message, Object data) {
        this.setCode(code);
        this.setMessage(message);
        this.setData(data);
    }
}
