package com.xf.xf_common.dispense.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by Administrator on 2018/5/30.
 *
 * @Project_name:xf_common
 * @LOCAL:com.xf.xf_common.entity
 * @blog : http://www.cnblog,com/huhongy/
 * @description:模拟项目和站点的对应关系，可以来源于任何关系型数据库或者非关系型数据库
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TempPro {
    //项目号
    private String proNo;
    //集合站点号
    private String siteNo;
    //以项目分发数据的时间点
    private Date startTime;
}
