package com.xf.xf_common.dispense.enums;

import lombok.Getter;
import lombok.ToString;

/**
 * @author : huhy on 2018/6/28.
 * @Project_name:springboot_xf
 * @LOCAL:com.xf.water.enums
 * @blog : http://www.cnblog,com/huhongy/
 * @description:用enum类型做code值
 */
@Getter
@ToString
public enum ResultCode {
    /** 成功 */
    SUCCESS("200", "成功"),
    RESULT_NOT_SITE_INFO("300","项目下没有对应站点");

    private String code;
    private String msg;

    ResultCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }


}
