package com.xf.xf_common.dispense.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by Administrator on 2018/5/30.
 *
 * @Project_name:springboot_mongodb
 * @LOCAL:com.xf.water.springboot_mongdb.entity
 * @blog : http://www.cnblog,com/huhongy/
 * @description:项目id和集合对应   1---n的关系
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DataOfTable {
    //主键
    private Long id;
    //项目编号
    private String proNo;
    //站点编号
    private String siteNo;
}
