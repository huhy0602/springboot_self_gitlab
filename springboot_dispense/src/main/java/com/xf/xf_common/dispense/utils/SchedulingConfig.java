package com.xf.xf_common.dispense.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by Administrator on 2018/5/16.
 *
 * @Project_name:springboot_mongdb
 * @LOCAL:com.xf.water.springboot_mongdb.util
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@Configuration
@EnableScheduling // 启用定时任务
public class SchedulingConfig {


    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Scheduled(cron = "0/10 * * * * ?") // 每20秒执行一次
    public void scheduler() {
        logger.info("定时任务已启动");
        //sqlServerTransMongodbUtil.transformData();
    }

}
