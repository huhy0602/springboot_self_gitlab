package com.xf.xf_common.dispense.utils;

import com.xf.xf_common.dispense.dao.DataOfTableDao;
import com.xf.xf_common.dispense.entity.DataOfTable;
import com.xf.xf_common.dispense.entity.JsonResult;
import com.xf.xf_common.dispense.enums.ResultCode;
import com.xf.xf_common.etl.entity.Sl6512014;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by Administrator on 2018/5/29.
 *
 * @Project_name:springboot_mongodb
 * @LOCAL:com.xf.water.springboot_mongdb.util
 * @blog : http://www.cnblog,com/huhongy/
 * @description:  为了业务分发
 */
@Component
public class DispenseUtil {

    @Autowired
    private   MongoTemplate mongoTemplate;
    @Autowired
    private DataOfTableDao dataOfTableDao;

    //项目时间记录集合
    @Value("${mongodb.temp.tempPro}")
    private  String tempPro;

    @Value("${config.common.timespan}")
    private  Integer timespan;
    /**
     * @author huhy
     * @ClassName:DispenseUtil
     * @date 2018/5/29 11:17
     * @Description: 根据站点分发指定程序
     */

    public   void  dispenseToSite(){
        System.out.println(mongoTemplate.getDb());
    }

    /**
     * @author huhy
     * @ClassName:DispenseUtil
     * @date 2018/6/27 14:47
     * @Description: 获取时间
     */
    public    Map<String,Date> getStarttimeAndEndtime(Date time) {
        int num=15;
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(time);
        calendar.add(Calendar.DAY_OF_MONTH,num);
        Date endTime;
        Date nowtime=new Date();
        if (calendar.getTime().getTime()>nowtime.getTime()){
            endTime=nowtime;
        }else {
            endTime=calendar.getTime();
        }
        Map<String,Date> map=new HashMap<String,Date>();
        map.put("startTime",time);
        map.put("endTime",endTime);
        return map;
    }
    /**
     * @author huhy
     * @ClassName:DispenseUtil
     * @date 2018/6/28 9:22
     * @Description: 获取mongodb集合下的最小值
     */
    public  Date selectCollectionSttMinValue(String dName){
        Date sttTime = null;
        Query query = new Query();
        Sort sort = new Sort(Sort.Direction.ASC,"stt");
        query.with(sort);
        List<Sl6512014> list = mongoTemplate.find(query,Sl6512014.class,dName);
        if(list != null && list.size() > 0){
            sttTime = list.get(0).getStt();
        }
        return sttTime;
    }
    /**
     * @author huhy
     * @ClassName:XfCommonApplicationTests
     * @date 2018/5/31 10:03
     * @Description: 通过站点名称和时间查出对应的数据
     */
    public JsonResult getSiteInfoByCollectionNameAndDate(String proNo, String siteNo, Date startDate, Date endDate){
        //返回的结果
        Map<String,List<Sl6512014>> resultMap = new HashMap<>();
        //通过项目id获取到对应的站点信息
        DataOfTable dataOfTable = dataOfTableDao.queryById(proNo);
        //先判断是否存在站点信息
        boolean exist = StringUtils.isEmpty(dataOfTable.getSiteNo());
        //站点信息不存在时
        if (exist){
            return new JsonResult(ResultCode.RESULT_NOT_SITE_INFO);
        }
        //站点信息不为空时，把拼接的字符串进行拆分
        List<String> siteNos = StringUtils.spliteStrToList(dataOfTable.getSiteNo());
        Query query = new Query(Criteria.where("stt").lte(endDate).gte(startDate));
        //判断站点是否传进来
        if(!StringUtils.isEmpty(siteNo)){
            List<Sl6512014> sl6512014s = mongoTemplate.find(query, Sl6512014.class, siteNo);
            resultMap.put(siteNo,sl6512014s);
            return new JsonResult(ResultCode.SUCCESS,resultMap);
        }
        for (String tempSiteNo : siteNos) {
            List<Sl6512014> sl6512014s = mongoTemplate.find(query, Sl6512014.class, tempSiteNo);
            resultMap.put(siteNo,sl6512014s);
        }
        return new JsonResult(ResultCode.SUCCESS,resultMap);
    }
}
