package com.xf.xf_common.dispense.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Administrator on 2018/5/30.
 *
 * @Project_name:xf_common
 * @LOCAL:com.xf.xf_common.utils
 * @blog : http://www.cnblog,com/huhongy/
 * @description:对字符串操作的工具类
 */
public class StringUtils {
    private final static   ObjectMapper mapper = new ObjectMapper();
    /**
     * @author huhy
     * @ClassName:StringUtils
     * @date 2018/5/30 17:04
     * @Description: 把字符串拆开放到list中
     */
    public static List<String> spliteStrToList(String strs){
        List<String> siteNos = new ArrayList<String>();
        //先判断是否含有“,”
        if(strs.indexOf(",") != -1){
            String[] split = strs.split(",");
            //经过asList处理
            siteNos = Arrays.asList(split);
        }else{
            //不存在，说明项目就对应一个集合点
            siteNos.add(strs);
        }
        return siteNos;
    }

    /**
     * @author huhy
     * @ClassName:StringUtils
     * @date 2018/5/31 15:48
     * @Description: 把对象转成json
     */
    public static String objToJson(Object obj){
        String jsonData = null;
        try {
            jsonData = mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {

        }
        return jsonData;
    }
    /**
     * 判断一个字符串是否是null或者空格或者空
     *
     * @param str 字符串
     * @return
     */
    public static boolean isEmpty(String str) {
        if (str != null && str.trim().length() > 0) {
            return false;
        }
        return true;
    }
}
