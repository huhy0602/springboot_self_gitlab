package com.xf.xf_common.etl.util;

import com.xf.xf_common.etl.dao.Sl6512014Dao;
import com.xf.xf_common.etl.entity.MaxValue;
import com.xf.xf_common.etl.entity.Sl6512014;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by Administrator on 2018/5/15.
 *
 * @Project_name:springboot_mongdb
 * @LOCAL:com.xf.water.springboot_mongdb.util
 * @blog : http://www.cnblog,com/huhongy/
 * @description:sqlserver抽取到mongodb的工具类
 */
@Component
public class SqlServerTransMongodbUtil{
    @Autowired
    private Sl6512014Dao sl6512014Dao;

    /* @Autowired
    private  RedisTemplate redisTemplate;*/

    @Autowired
    private  MongoTemplate mongoTemplate;


    /**
     * @author huhy
     * @ClassName:SqlServerTransMongodbUtil
     * @date 2018/5/15 14:51
     * @Description: ELT方法
     */
    public  void transformData(){
        //执行抽取之前执行异常回滚机制
        rollbackResult();
        //获取serial的最大值，作为判断是否终止循环的条件
        long serialMax = sl6512014Dao.serialMax();
        //System.out.println(serialMax);
        boolean flag = false;
        //保存sqlserver的表数据量
        //保存在mongodb的最大值或者异常值
        MaxValue maxValue = getMaxSize();
        //保存在mongodb的最大值或者异常值
        long maxSize = maxValue.getMaxSize();
        long startItem = maxValue.getStartItem();
        while (!flag){
            /**
             * 防止实时库插入过程中，不调用定时的判断
             */
            if(startItem+1 > serialMax){
                //serialMax 固定的
                flag = true;
                break;
            }
            //获取到between的数据
            List<Sl6512014> sl6512014s = sl6512014Dao.selectCountNew(startItem+1);
            long size = sl6512014s.size();
            if(size == 0 && startItem < serialMax){
                startItem += 49999;
                continue;
            }
            if(size == 0){
                //当数量为0时，跳出循环
                flag = true;
                break;
            }
            //存入mongodb中
            insertMongodb(sl6512014s);
            //获取到最后一条的那个serial的值
            startItem = sl6512014s.get(sl6512014s.size() - 1).getSerial();
            //把下次的startItem  中存入mongodb中
            maxSize = maxSize + sl6512014s.size();
            //把值插入到mongodb中
            updateCollections(maxSize,startItem);
        }
    }
    /**
     * @author huhy
     * @ClassName:SpringbootMongdbApplicationTests
     * @date 2018/6/6 15:38
     * @Description: 记录成功条数
     */
    public  void updateCollections(long maxSize,long serial){
        //long maxSize = 0L;
        boolean flag = mongoTemplate.collectionExists("maxValue");
        //判断集合是否存在
        if(flag){
            Query query=new Query(Criteria.where("author").is("mongodb"));
            Update update= new Update();
            //set("maxSize", maxSize).set("startItem",serial)
            update.set("maxSize", maxSize);
            update.set("startItem",serial);
            //更新查询返回结果集的第一条
            mongoTemplate.updateFirst(query,update,"maxValue");
        }else{
            mongoTemplate.insert(new MaxValue("mongodb",maxSize,serial),"maxValue");
        }

        //return maxSize;
    }
    /**
     * @author huhy
     * @ClassName:SqlServerTransMongodbUtil
     * @date 2018/6/7 14:57 
     * @Description: 非系统异常的异常处理值
     */
    public  void updateCollectionNames(String collectionNames,boolean f){

            //long maxSize = 0L;
            boolean flag = mongoTemplate.collectionExists("maxValue");
            //判断集合是否存在
            if(flag){
                Query query=new Query(Criteria.where("author").is("mongodb"));
                Update update= new Update();
                //f是执行更新的判断符号
                if(f){
                    update.set("collectionNames", collectionNames);
                }else{
                    update.set("collectionNames","");
                }
                //set("maxSize", maxSize).set("startItem",serial)
                //更新查询返回结果集的第一条
                mongoTemplate.updateFirst(query,update,"maxValue");
            }else{
                mongoTemplate.insert(new MaxValue("mongodb",collectionNames),"maxValue");
            }
    }

    /**
     * 把标示插入redis中
     */
    /*public  void insertRedis(long maxSize){
        ValueOperations operations = redisTemplate.opsForValue();
        operations.set("maxSize",maxSize);
    }*/

    /**
     * 判断最终的 maxSize值
     *      redis和mongodb中是否一样
     */
    public MaxValue getMaxSize() {
        MaxValue result = new MaxValue();
        try {
            //先判断mongodb是否 存在集合
            //如果不存在集合说明是第一次请求
            boolean maxValue = mongoTemplate.collectionExists("maxValue");
            if (!maxValue) {
                //不存在
                result.setMaxSize(0L);
                result.setStartItem(0L);
                return result;
            } else {
                //存在
                //从mongodb中取出
                long startItem = (long) mongoTemplate.getCollection("maxValue").find().next().get("startItem");
                long maxSize = (long) mongoTemplate.getCollection("maxValue").find().next().get("maxSize");
               /* long redisSize = 0L;
                try {
                    //从redis中获取对应值进行比较
                     redisSize = (long) redisTemplate.opsForValue().get("maxSize");
                    if (startItem != redisSize) {
                        //把redis中的值付给mongodb,让sqlserver 从mongodbSize起始位置开始查询
                        startItem = redisSize;
                    }
                } catch (Exception e) {
                    result.setMaxSize(maxSize);
                    result.setStartItem(startItem);
                    return result;
                }*/
                result.setMaxSize(maxSize);
                result.setStartItem(startItem);
                return result;
            }
        } catch (Exception e) {
            //mongdb中库不存在  会报空指针    捕获异常   给它初始值0
            result.setMaxSize(0L);
            result.setStartItem(0L);
            return result;
        }
    }
    /**
     * @author huhy
     * @ClassName:SqlServerTransMongodbUtil
     * @date 2018/5/23 14:32 
     * @Description: 批量导入
     */
    public void insertMongodb(List<Sl6512014> sl6512014s){
        Map<String,List<Sl6512014>> map = new HashMap<String,List<Sl6512014>>();
        for (Sl6512014 sl6512014 : sl6512014s) {
            List<Sl6512014> sl6512014s1 = new ArrayList<>();
            String st = sl6512014.getSt();
            if(st == "" || st == null ){
                st = "error";
            }
            //判断key是否存在
            if(map.containsKey(st)){
                sl6512014s1 = map.get(st);
            }
            sl6512014s1.add(sl6512014);
            map.put(st,sl6512014s1);
        }
        Set<String> strings = map.keySet();
        //把集合名称存在了mongodb中
        updateCollectionNames(strings.toString(),true);
        for (String string : strings) {
            //临时  防止异常
            Map<String,List<Sl6512014>> temp = new HashMap<String,List<Sl6512014>>();
            temp.put(string,map.get(string));
            //批量插入
            try {
                mongoTemplate.insert(map.get(string),string);
            } catch (Exception e) {
                //异常删除增加成功的方式
                insertError(temp);
            }
        }
        //for循环执行结束，就把collectionNames清空，存下次来的值
        updateCollectionNames(strings.toString(),false);
    }


    /**
     * @author huhy
     * @ClassName:SpringbootMongdbApplicationTests
     * @date 2018/5/23 15:26
     * @Description: mongodb批量插入异常回滚机制代码实现
     *
     * */
    public void insertError(Map<String,List<Sl6512014>> temp){
        //获取异常时的map集合
        Set<String> str = temp.keySet();
        //获取所有的key
        for (String s : str) {
            //拿到所有的value
            List<Sl6512014> sl6512014s = temp.get(s);
            //遍历删除
            for (Sl6512014 sl6512014 : sl6512014s) {
                Query query=new Query(Criteria.where("serial").is(sl6512014.getSerial()));
                mongoTemplate.remove(query,s);
            }
        }
    }
    /**
     * @author huhy
     * @ClassName:SqlServerTransMongodbUtil
     * @date 2018/6/7 15:08 
     * @Description:系统异常的处理机制
     */

    public void rollbackResult(){
        boolean maxValue = mongoTemplate.collectionExists("maxValue");
        if(maxValue){
            String collectionNames = (String) mongoTemplate.getCollection("maxValue").find().next().get("collectionNames");
            if("".equals(collectionNames)){
                return;
            }
            //用逗号将字符串分开，得到字符串数组
            String[] strs=collectionNames.split(",");
            //将字符串数组转换成集合list
            List<String> list=Arrays.asList(strs);
            //mongodb中存储的插入成功的serial值
            Object serial = mongoTemplate.getCollection("maxValue").find().next().get("startItem");
            Query query = new Query(Criteria.where("serial").gt(serial));
            for (String s : list) {
                mongoTemplate.remove(query,s);
            }
        }else{
            return;
        }
    }


}
