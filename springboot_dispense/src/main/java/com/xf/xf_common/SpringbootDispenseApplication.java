package com.xf.xf_common;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.xf.xf_common.*.dao")
public class SpringbootDispenseApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringbootDispenseApplication.class, args);
	}
}
