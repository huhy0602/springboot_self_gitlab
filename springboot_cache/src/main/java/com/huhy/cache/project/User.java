package com.huhy.cache.project;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author : huhy on 2018/12/6.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.cache.project
 * @description:{todo}
 */
@Entity
@Table(name = "t_user")
@Data

public class User implements Serializable {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String password;
    private int age;
}
