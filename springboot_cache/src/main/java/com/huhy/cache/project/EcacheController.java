package com.huhy.cache.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * @author : huhy on 2018/12/6.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.cache.project
 * @description:
 * @CacheConfig(cacheNames = {“myCache”})设置ehcache的名称，这个名称必须在ehcache.xml已配置 。
 */
@RestController
@CacheConfig(cacheNames = {"huhy"})
public class EcacheController {

    @Autowired
    private UserDao userDao;

    /**
     * Cacheable  : 先查询是否已经有缓存，有会使用缓存，没有则会执行方法并缓存。
     * @param id
     * @return
     */
    @Cacheable(key = "targetClass + methodName +#p0")
    @RequestMapping("get1")
    public User queryAll(Integer id) {
        System.out.println(System.getProperty("java.io.tmpdir"));
        Optional<User> user = userDao.findById(id);
        return user.get();
    }
}
