package com.huhy.cache.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * @author : huhy on 2018/12/6.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.cache.project
 * @description:{todo}
 */
@RestController
@RequestMapping("cache")
public class CacheController {
    @Autowired
    private UserDao userDao;
    /**
     * Cacheable  : 先查询是否已经有缓存，有会使用缓存，没有则会执行方法并缓存。
     */
    @Cacheable(value = "huhy_1" ,key = "targetClass + methodName +#p0")
    @RequestMapping("get1")
    public User queryAll(Integer id) {
        Optional<User> user = userDao.findById(id);
        return user.get();
    }
    /**
     * CachePut : 保证方法被调用，又希望结果被缓存。
     * 与@Cacheable区别在于是否每次都调用方法，常用于更新
     * @CachePut注解的作用 主要针对方法配置，能够根据方法的请求参数对其结果进行缓存，
     * 和 @Cacheable 不同的是，它每次都会触发真实方法的调用 。简单来说就是用户更新缓存数据。
     * 但需要注意的是该注解的value 和 key 必须与要更新的缓存相同，也就是与@Cacheable 相同id
     */
    @CachePut(value = "huhy_1" ,key = "targetClass + methodName +#p0")
    @RequestMapping("get2")
    public User queryAll_2(Integer id) {
        Optional<User> user = userDao.findById(id);
        return user.get();
    }

    /** CacheEvict   */

    /**
     * @CacheEvict(value = "huhy_2" ,key = "#id")
     *  去除huhy_2的缓存
     *
     *
     *  allEntries           是否清空所有缓存内容，缺省为 false，如果指定为 true，则方法调用后将立即清空所有缓存
     *  beforeInvocation	 是否在方法执行前就清空，缺省为 false，如果指定为 true，则在方法还没有执行的时候就清空缓存，
     *  缺省情况下，如果方法执行抛出异常，则不会清空缓存
     * @param id
     */
    @CacheEvict(value = "huhy_2" ,key = "#id")
    @RequestMapping("get3")
    public void queryAll_3(Integer id) {
        userDao.deleteById(id);
    }
    @Cacheable(value = "huhy_2" ,key = "#id")
    @RequestMapping("get4")
    public User queryAll_4(Integer id) {
        Optional<User> user = userDao.findById(id);
        return user.get();
    }

    /** CacheEvict   */

    @CacheEvict(value = "accountCache" ,allEntries = true)
    @RequestMapping("get5")
    public void queryAll_5() {
        userDao.deleteAll();
    }
}
