package com.huhy.cache.project;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author : huhy on 2018/12/6.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.cache.project
 * @description:{todo}
 */
public interface UserDao extends JpaRepository<User,Integer> {
}
