package com.huhy.cache;

import com.huhy.cache.project.User;
import com.huhy.cache.project.UserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootCacheApplicationTests {

    @Autowired
    private UserDao userDao;

    private  static List<User> users = new ArrayList<>();
    static {
        int i  = 1;
        while (i <= 10){
            users.add(new User());
            i++;
        }
    }
    @Test
    public void contextLoads() {
        System.out.println("jpa");
        userDao.saveAll(users);

    }

}
