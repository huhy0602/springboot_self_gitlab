package com.huhy.vue.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : huhy
 * @Project_name:springboot_self_gitlab
 * @date:2019/2/21 14:06
 * @email:hhy_0602@163.com
 * @description:{todo}
 * @Exception: throw {todo}
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/")
    public  void testVue(){

    }
}
