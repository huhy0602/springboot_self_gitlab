package com.huhy.springboot_easyui.common.easyui.model.enums;

/**
 * 资源类型
 * 
 * @author huhy
 *
 */
public enum ResourceType {
	MENU, FUNCTION, BLOCK
}
