package com.huhy.rabbit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRabbitAnnoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootRabbitAnnoApplication.class, args);
    }

}
