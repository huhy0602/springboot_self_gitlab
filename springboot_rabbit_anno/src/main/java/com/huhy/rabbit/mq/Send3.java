package com.huhy.rabbit.mq;

import com.huhy.rabbit.util.ConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @author : huhy
 * @Project_name:springboot_self_gitlab
 * @date:2019/5/17 13:55
 * @email:hhy_0602@163.com
 * @description:{todo}
 * @Exception: throw {todo}
 */
public class Send3 {

    private final static String EXCHANGE_NAME = "test_exchange_fanout";

    public static void main(String[] argv) throws Exception {
        // 获取到连接以及mq通道
        Connection connection = ConnectionUtil.getConnection();
        Channel channel = connection.createChannel();

        // 声明exchange
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        // 声明队列
        channel.queueDeclare("11111", false, false, false, null);
        // 绑定队列到交换机
        channel.queueBind("11111", EXCHANGE_NAME, "huh.*");
        // 消息内容
        String message = "Hello World!";
        channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());
        System.out.println(" [x] Sent '" + message + "'");

        channel.close();
        connection.close();
    }
}
