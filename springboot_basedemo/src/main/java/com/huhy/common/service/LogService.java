package com.huhy.common.service;

import com.huhy.common.domain.LogDO;
import com.huhy.common.domain.PageDO;
import com.huhy.common.utils.Query;
import org.springframework.stereotype.Service;

@Service
public interface LogService {
	void save(LogDO logDO);
	PageDO<LogDO> queryList(Query query);
	int remove(Long id);
	int batchRemove(Long[] ids);
}
