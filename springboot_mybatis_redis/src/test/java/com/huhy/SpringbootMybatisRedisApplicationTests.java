package com.huhy;

import com.huhy.cache.dao.UserDao;
import com.huhy.redis2.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootMybatisRedisApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private UserDao userDao;
	@Autowired
	private UserService userService;
	/**
	 * @author huhy
	 * @ClassName:SpringbootMybatisRedisApplicationTests
	 * @date 2018/6/14 10:46 
	 * @Description: ${todo}
	 */	
	@Test
	public void testRedis(){
		System.out.println(userService.findByAppId("1"));
	}

}
