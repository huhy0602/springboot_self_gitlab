package com.huhy.cache.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/6/14.
 *
 * @Project_name:springboot_self
 * @LOCAL:com.huhy.entity
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@Data
@ToString
public class UserEntity implements Serializable {
    private String id;
    private String name;
    private long age;

}
