package com.huhy.cache.dao;

import com.huhy.cache.entity.UserEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2018/6/14.
 *
 * @Project_name:springboot_self
 * @LOCAL:com.huhy.dao
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@Repository
public interface UserDao {
    List<UserEntity> findAll();

    UserEntity queryId(String id);
}
