package com.huhy.redis2;

import com.huhy.cache.dao.UserDao;
import com.huhy.cache.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author : huhy on 2018/8/14.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.redis2
 * @description:{todo}
 */
public interface UserService {

    public UserEntity findByAppId(String id);
}
