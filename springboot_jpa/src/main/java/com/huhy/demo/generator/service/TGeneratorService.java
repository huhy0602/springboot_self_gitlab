package com.huhy.demo.generator.service;

import com.huhy.demo.generator.model.TGenerator;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huhy
 * @since 2018-08-24
 */
public interface TGeneratorService extends IService<TGenerator> {

}
