package com.huhy.demo.generator.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huhy
 * @since 2018-08-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_generator")
public class TGenerator extends Model<TGenerator> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    private String pass;
    private Integer age;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
