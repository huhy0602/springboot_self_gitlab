package com.huhy.demo.generator.service.impl;

import com.huhy.demo.generator.model.TGenerator;
import com.huhy.demo.generator.mapper.TGeneratorMapper;
import com.huhy.demo.generator.service.TGeneratorService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huhy
 * @since 2018-08-24
 */
@Service
public class TGeneratorServiceImpl extends ServiceImpl<TGeneratorMapper, TGenerator> implements TGeneratorService {

}
