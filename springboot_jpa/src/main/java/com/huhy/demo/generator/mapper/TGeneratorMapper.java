package com.huhy.demo.generator.mapper;

import com.huhy.demo.generator.model.TGenerator;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huhy
 * @since 2018-08-24
 */
public interface TGeneratorMapper extends BaseMapper<TGenerator> {

}
