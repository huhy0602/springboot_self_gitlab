package com.huhy.demo;

import com.huhy.demo.generator.Generator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ClassUtils;

import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootJpaApplicationTests {
    @Autowired
    private Generator generator;

	@Test
	public void contextLoads() throws Exception{
		String canonicalPath = new File("").getCanonicalPath();
		System.out.println(canonicalPath);
	}

	@Test
	public  void testPath(){
        String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
        System.out.println(path);
    }

    @Test
    public void test(){
        String[] tables = {"t_generator"};
        generator.generator(tables);
    }

}
