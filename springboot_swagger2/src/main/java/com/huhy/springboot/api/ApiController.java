package com.huhy.springboot.api;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : huhy on 2018/11/1.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.springboot.api
 * @description:{todo}
 */
@RestController
public class ApiController {

    @GetMapping("index")
    @ApiOperation("胡红阳")
    public String gertIInde(){
        return "huhy";
    }

    @PostMapping("/huhy")
    @ApiOperation("测试参数")
    public String getIndex(String name){

        return name;
    }
}
