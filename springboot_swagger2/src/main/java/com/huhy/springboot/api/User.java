package com.huhy.springboot.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author : huhy on 2018/11/2.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.springboot.api
 * @description:{todo}
 */
@Data
@AllArgsConstructor
@ApiModel
public class User {
    @ApiModelProperty("用户id")
    private Long id;
    @ApiModelProperty("用户name")
    private String name;
    @ApiModelProperty("用户age")
    private int age;

}
