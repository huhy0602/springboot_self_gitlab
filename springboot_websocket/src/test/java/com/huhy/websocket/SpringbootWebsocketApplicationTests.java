package com.huhy.websocket;

import com.huhy.websocket.project.dao.UserDao;
import com.huhy.websocket.project.entity.User;
import okio.BufferedSink;
import okio.Okio;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootWebsocketApplicationTests {

    @Autowired
    private UserDao userDao;
    @Test
    public void contextLoads() {
        List<User> users = userDao.selectList(null);
        System.out.println(users.size());
    }

    @Test
    public void test1(){
        try {
            File file = new File("file/test.txt"); //如果文件不存在，则自动创建
            BufferedSink sink = Okio.buffer(Okio.sink(file));
            sink.writeUtf8("Hello, World");
            sink.writeString("测试信息", Charset.forName("UTF-8"));
            sink.writeInt(111);
            sink.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
