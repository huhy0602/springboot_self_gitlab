package com.huhy.websocket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @author : huhy
 * @Project_name:springboot_self_gitlab
 * @date:2019/3/11 9:30
 * @email:hhy_0602@163.com
 * @description:{todo}
 * @Exception: throw {todo}
 */
@Configuration
public class WebSocketConfig {
    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
        return new ServerEndpointExporter();
    }
}
