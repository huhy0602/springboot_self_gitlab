package com.huhy.websocket.config;

import com.huhy.websocket.project.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * @author : huhy
 * @Project_name:springboot_self_gitlab
 * @date:2019/3/11 9:47
 * @email:hhy_0602@163.com
 * @description:{todo}
 * @Exception: throw {todo}
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String getIndex(){
        return "socket";
    }


    @RequestMapping("/index")
    public String getIndex2(){
        return "controllerTest";
    }

    @Autowired
    private Socket webSocket;

    @Autowired
    private UserDao userDao;

    @RequestMapping("/web")
    @ResponseBody
    @Scheduled(fixedRate = 5000)
    public String getWeb(){
        try {
          webSocket.sendMessage(userDao.selectList(null).toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "发送成功";

    }
}
