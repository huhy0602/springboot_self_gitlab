package com.huhy.websocket.project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @author : huhy on 2018/11/1.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.project.entity
 * @description:{todo}
 */
@Data
@AllArgsConstructor
public class User implements Serializable {
    private Long id;
    private String name;
    private Integer age;
    private String email;
}
