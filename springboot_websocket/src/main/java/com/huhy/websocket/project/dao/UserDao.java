package com.huhy.websocket.project.dao;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huhy.websocket.project.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author : huhy on 2018/12/12.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.project.plus1
 * @description:{todo}
 */
@Repository
public interface UserDao extends BaseMapper<User> {


}
