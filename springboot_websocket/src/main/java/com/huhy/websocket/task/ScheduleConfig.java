package com.huhy.websocket.task;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executors;

/**
 * @author : huhy
 * @Project_name:springboot_self_gitlab
 * @date:2019/3/12 15:04
 * @email:hhy_0602@163.com
 * @description:{todo}
 * @Exception: throw {todo}
 */

@Configuration
public class ScheduleConfig implements SchedulingConfigurer {
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {

        taskRegistrar.setScheduler(Executors.newScheduledThreadPool(5));
    }
}