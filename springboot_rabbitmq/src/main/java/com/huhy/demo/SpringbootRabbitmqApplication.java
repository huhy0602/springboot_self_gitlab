package com.huhy.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author huhy
 * @ClassName:SpringbootRabbitmqApplication
 * @date 2019/1/21 13:25 
 * @Description: 验证rabbitmq
 */
@SpringBootApplication
public class SpringbootRabbitmqApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootRabbitmqApplication.class, args);
	}
}
