package com.huhy.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
public class SpringbootWebuploadApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootWebuploadApplication.class, args);
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		//允许上传的文件最大值
		factory.setMaxFileSize("1500MB"); //KB,MB
		/// 设置总上传数据总大小
		factory.setMaxRequestSize("1500MB");
		return factory.createMultipartConfig();
	}
}
