package com.huhy.springboot_advanced.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @author : huhy on 2018/12/8.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.springboot_advanced.listener
 * @description:{todo}
 */
@WebFilter(filterName="myFilter",urlPatterns="/*")
public class MyFilter implements Filter {


    @Override
    public void destroy() {
        System.out.println("过滤器销毁");
    }


    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        System.out.println("执行过滤操作");
        chain.doFilter(request, response);
    }


    @Override
    public void init(FilterConfig config) throws ServletException {
        System.out.println("过滤器初始化");
    }


}