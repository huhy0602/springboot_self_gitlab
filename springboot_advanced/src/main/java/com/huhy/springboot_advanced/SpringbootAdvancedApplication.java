package com.huhy.springboot_advanced;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class SpringbootAdvancedApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootAdvancedApplication.class, args);
    }
}
