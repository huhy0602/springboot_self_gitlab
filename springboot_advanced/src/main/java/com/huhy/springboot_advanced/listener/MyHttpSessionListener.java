package com.huhy.springboot_advanced.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author : huhy on 2018/12/8.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.springboot_advanced.listener
 * @description:{todo}
 */
@WebListener
public class MyHttpSessionListener implements HttpSessionListener {


    @Override
    public void sessionCreated(HttpSessionEvent se) {
        System.out.println("Session 被创建");
    }


    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        System.out.println("ServletContex初始化");
    }



}
