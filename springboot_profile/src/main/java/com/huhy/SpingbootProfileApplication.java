package com.huhy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpingbootProfileApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpingbootProfileApplication.class, args);
    }
}
