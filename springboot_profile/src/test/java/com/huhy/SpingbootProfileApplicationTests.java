package com.huhy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestMapping;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpingbootProfileApplicationTests {

    @Value("${person.age}")
    private Integer age;

    @Value("${person.lastName}")
    private String lastName;




    @Test
    public void contextLoads() {
        System.out.println(age);
    }

}
