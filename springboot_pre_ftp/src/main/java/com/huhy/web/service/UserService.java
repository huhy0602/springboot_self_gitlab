package com.huhy.web.service;

import com.huhy.web.entity.User;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/8/5 22:18
 * @Class Description:
 */

public interface UserService {
    public User queryById(String id);

    public User queryByName(String name,String id);
}
