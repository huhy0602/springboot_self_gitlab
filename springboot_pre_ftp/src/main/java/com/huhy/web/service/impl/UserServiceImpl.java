package com.huhy.web.service.impl;

import com.huhy.web.entity.User;
import com.huhy.web.mapper.UserMapper;
import com.huhy.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/8/5 22:18
 * @Class Description:
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public User queryById(String id) {
        User user = userMapper.selectByPrimaryKey(id);
        return user;
    }

    @Override
    public User queryByName(String name, String id){
        User user = userMapper.selectByName(name,id);
        return user;
    }
}
