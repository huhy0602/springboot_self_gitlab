package com.huhy.web.common.mqtt;

/**
 * @Author:huhy
 * @DATE:Created on 2018/1/18 13:03
 * @Modified By:
 * @Class Description:
 */
public interface MsgHandler {
    /**
     * 消息
     * @param type 消息类型
     * @param data 数据
     */
    void onMessage(String type, Object data);
    /**
     * 事件
     * @param event x
     */
    void onEvent(int event);
}
