package com.huhy.web.common.SCADA;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RegeditEntity {
	//注册表root下的名称
	private String protocolName;
	//exe所在位置
	private String urlName;
	//是否有参数
	private String paramName;



	public RegeditEntity(String protocolName, String urlName) {
		super();
		this.protocolName = protocolName;
		this.urlName = urlName;

	}
}
