package com.huhy.web.common;

import com.huhy.web.common.SCADA.RegeditEntity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @Author:huhy
 * @DATE:Created on 2017/12/18 15:17
 * @Modified By:
 * @Class Description:
 */
public class CommonUtil {
    /**
     * 方法描述:把 \ 转出\\   2017年12月18日
     */
    public static String transString(String dir) {

        return dir.replace("\\", "\\\\");
    }
    /**
     * 方法描述:java 项目路径   2017年12月18日
     */
    public static String getPath()  {
        //获取工程项目路径
        return System.getProperty("user.dir");
    }

    /**
     * 方法描述:reg文件的创建于写入文本内容   2017年12月18日
     */

    public static void testFile(RegeditEntity entity) {
       /**
        *@Author: huhy
        *@Date:10:18 2017/12/21
        *@params: * @param domain
        *@Description:
        */
        //在项目上创建一个文件位置
        String path = getPath()+"\\file";
        File f = new File(path);
        if (!f.exists()) {
            f.mkdirs();
        }
        // fileName
        String fileName = entity.getProtocolName()+".reg";
        File file = new File(f, fileName);
        //如果文件存在就删除   这种情况是不允许的
        //解决方案： 1  前端控制，鼠标离开就入库查询   2  文件命名采用时间戳
        if(file.exists()){
            file.delete();
        }
        System.out.println(entity.getUrlName());
        if (!file.exists()) {
            try {
                file.createNewFile();
                // 文本写入
                FileWriter fw = new FileWriter(file, true);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write("Windows Registry Editor Version 5.00");
                bw.newLine();bw.newLine();
                bw.write("[HKEY_CLASSES_ROOT\\"+entity.getProtocolName()+"]");
                bw.newLine();
                bw.write("@="+"\""+entity.getProtocolName()+" Protocol\"");
                bw.newLine();
                bw.write("\"URL Protocol\"="+"\"\"");
                bw.newLine();
                bw.newLine();
                bw.write("[HKEY_CLASSES_ROOT\\"+entity.getProtocolName()+"\\DefaultIcon]");
                bw.newLine();
                bw.write("@="+"\""+entity.getUrlName()+"\"");
                bw.newLine();bw.newLine();
                bw.write("[HKEY_CLASSES_ROOT\\"+entity.getProtocolName()+"\\shell]");
                bw.newLine();
                bw.write("@="+"\"\"");
                bw.newLine();bw.newLine();
                bw.write("[HKEY_CLASSES_ROOT\\"+entity.getProtocolName()+"\\shell\\open]");
                bw.newLine();
                bw.write("@="+"\"\"");
                bw.newLine();bw.newLine();
                bw.write("[HKEY_CLASSES_ROOT\\"+entity.getProtocolName()+"\\shell\\open\\command]");
                bw.newLine();
                bw.write("@="+"\""+"\\"+"\""+entity.getUrlName()+"\\"+"\"\"");
                bw.newLine();
                bw.flush();
                bw.close();
                fw.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /**
     * 执行reg文件
     */
    public static void exec(RegeditEntity entity){
        try {
            String cmd = "cmd /c "+ CommonUtil.getPath()+"\\file"+"\\"+entity.getProtocolName()+".reg";
            //System.out.println(CommonUtil.getPath()+"\\"+domain.getProtocolName()+".reg");
            Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
