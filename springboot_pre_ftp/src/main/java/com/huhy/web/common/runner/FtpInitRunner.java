package com.huhy.web.common.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @Author:huhy
 * @DATE:Created on 2018/1/3 12:47
 * @Modified By:
 * @Class 项目启动后运行此方法:
 */
@Component
@Order(value=2)
public class FtpInitRunner implements CommandLineRunner {
    @Override
    /**
     *@Author: huhy
     *@Package_name:com.huhy.web.common.runner
     *@Date:13:20 2018/1/3
     *@Description:项目启动后运行此方法 CommandLineRunner
     * //项目路径 path =  E:\IDE\workspace\ideaWorkspace\spring boot\spring-boot
    String path = System.getProperty("user.dir");
    CommandLine.main(new String[]{path+"\\src\\main\\resources\\ftpserver\\ftpd-typical.xml"});
    System.out.println("----------------------"+path);
     */
    public  void run(String... var1) throws Exception{
        //System.out.println("2222222222222222222222");
    }

}
