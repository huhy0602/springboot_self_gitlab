package com.huhy.web.common.SCADA;

import com.huhy.web.common.CommonUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author:huhy
 * @DATE:Created on 2017/12/18 15:17
 * @Modified By:
 * @Class
 *  暂定：协议名和保存的reg文件名相同:
 */
@Controller
public class RegistryController {

    @RequestMapping(value = "/regedit",method = RequestMethod.POST)
    @ResponseBody
    public String registry(RegeditEntity entity){

        //生成一个reg文件
        try {
            CommonUtil.testFile(entity);
        } catch (Exception e) {
            return "305";//生成reg文件错误
        }
        //调用JDK的Runtime去执行这个reg文件
        try {
            CommonUtil.exec(entity);
        } catch (Exception e) {
            return "306";//执行reg文件错误
        }
        return "200"; //注册表注册成功，可以调用
    }

}
