package com.huhy.web.common.mqtt;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.Test;

/**
 * @Author:huhy
 * @DATE:Created on 2018/1/18 13:41
 * @Modified By:
 * @Class Description:
 */
public class TestMqtt {

    public static void main(String[] args) throws MqttException {
        MqttUtil instance = MqttUtil.getInstance();
        instance.initMqtt();
        System.out.println(instance.isConnected());

    }
    @Test
    /**
     *@Author: huhy
     *@Package_name:com.huhy.web.common.mqtt
     *@Date:14:58 2018/1/25
     *@Description:
     */
    public void test(){
        MQTTSource mq = new MQTTSource();
        mq.start();
    }
}
