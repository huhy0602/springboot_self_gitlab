package com.huhy.web.common;

/**
 * @Author:huhy
 * @DATE:Created on 2017/12/18 15:28
 * @Modified By:
 * @Class Description:
 */
public class Code {
    public static final int SUCCESS=200;  //成功
    public static final int FAIL_=500;    //失败
    public static final int BADREQUEST=400;  //错误的请求
    public static final int NO_DATA=204;     //没有数据
    public static final int HAS_MORE=304;     //数据存在
    public static final int CREATE_FILE_FATL=305;     //生成reg文件错误
    public static final int EXEC_FILE_FATL=306;     //执行reg文件错误
    public static final int SYSERROR=505;     //  系统错误
    public static final int AUTHORITY_IS_ENOUGH=510;     //  权限不足
    public static final int USERNAME_OR_PASSWORD_ERROR=520;  //登录失败
    public static final int OLDPASSWORD_ERROR=521;  //登录失败
}
