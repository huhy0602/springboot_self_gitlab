package com.huhy.web.common;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * @Author:huhy
 * @DATE:Created on 2018/1/3 15:32
 * @Modified By:
 * @Class Description: 多数据源的配置问题
 */
@Configuration
public class DataSourceConfig {
    /**
     *@Author: huhy
     *@Package_name:com.huhy.web.common
     *@Date:16:27 2018/1/3
     *@Description:   根据配置文件找到指定数据库，创建指定数据库的连接   数据库【主数据库】
     */
    @Bean(name = "primaryDataSource")
    @Qualifier("primaryDataSource")
    //根据前缀生成知道数据源
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource primaryDataSource(){
        return DataSourceBuilder.create().build();
    }
    /**
     *@Author: huhy
     *@Package_name:com.huhy.web.common
     *@Date:16:27 2018/1/3
     *@Description:根据配置文件找到指定数据库，创建指定数据库的连接 【第二个数据库】
     */
    @Bean(name = "secondaryDataSource")
    @Qualifier("secondaryDataSource")
    @Primary
    //根据前缀生成知道数据源
    @ConfigurationProperties(prefix = "spring.datasource.secondary")
    public DataSource secondaryDataSource(){
        return DataSourceBuilder.create().build();
    }

    /**
     *@Author: huhy
     *@Package_name:com.huhy.web.common
     *@Date:16:29 2018/1/3
     *@Description: 返回JdbcTemplate 对象
     */
    @Bean(name = "primaryJdbcTemplate")
    public JdbcTemplate primaryJdbcTemplate(@Qualifier("primaryDataSource")DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
    /**
     *@Author: huhy
     *@Package_name:com.huhy.web.common
     *@Date:16:29 2018/1/3
     *@Description: 返回JdbcTemplate 对象
     * @Qualifier("secondaryDataSource")   采用byName方式
     */
    //放在方法的上面，而不是类，意思是产生一个bean,并交给spring管理
    @Bean(name = "secondaryJdbcTemplate")
    public JdbcTemplate secondaryJdbcTemplate(@Qualifier("secondaryDataSource")DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
}

