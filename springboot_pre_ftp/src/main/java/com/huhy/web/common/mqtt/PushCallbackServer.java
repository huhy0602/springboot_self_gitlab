package com.huhy.web.common.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * @Author:huhy
 * @DATE:Created on 2017/12/1 17:13
 * @Modified By:
 * @Class Description:
 */
public class PushCallbackServer implements MqttCallback {
    @Override
    public void connectionLost(Throwable cause) {
        // 连接丢失后，一般在这里面进行重连
        System.out.println("连接断开，可以做重连");
    }
    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        System.out.println("server");
    }
    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        // subscribe后得到的消息会执行到这里面
        System.out.println("server消息主题 : " + topic);
        System.out.println("server消息Qos : " + message.getQos());
        System.out.println("server消息内容 : " + new String(message.getPayload()));
    }
}
