package com.huhy.web.common.ftpserver;

import org.apache.ftpserver.main.CommandLine;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author:huhy
 * @DATE:Created on 2017/12/22 17:55
 * @Modified By:
 * @Class Description:
 */
@Controller
public class ScadaController {
    @RequestMapping(value = "startFtp",method = RequestMethod.GET)
    public void startFtp(){
        CommandLine.main(new String[]{"E:\\IDE\\workspace\\ideaWorkspace\\spring boot\\spring-boot\\src\\main\\resources\\ftpserver\\ftpd-mysql.xml"});
    }
}
