package com.huhy.web.common.runner;

import org.apache.ftpserver.main.CommandLine;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @Author:huhy
 * @DATE:Created on 2018/1/3 13:20
 * @Modified By:
 * @Class Description:
 */
@Component
@Order(value = 1)
public class FtpInitRunner2 implements ApplicationRunner{
    @Override
    /**
     *@Author: huhy
     *@Package_name:com.huhy.web.common.runner
     *@Date:13:29 2018/1/3
     *@Description:   ApplicationRunner方式实现
     */
    public void run(ApplicationArguments applicationArguments) throws Exception {
        //项目路径 path =  E:\IDE\workspace\ideaWorkspace\spring boot\spring-boot
        String path = System.getProperty("user.dir");
        CommandLine.main(new String[]{path+"\\src\\main\\resources\\ftpserver\\ftpd-typical.xml"});
        //System.out.println("----------------------"+path);
        //System.out.println("111111111111111111111111");


    }


}
