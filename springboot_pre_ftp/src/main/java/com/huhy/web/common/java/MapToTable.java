package com.huhy.web.common.java;

import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author:huhy
 * @DATE:Created on 2018/1/15 9:47
 * @Modified By:
 * @Class 对比hashmap和hashtable:
 */
public class MapToTable {
    /**
     *@Author: huhy
     *@Package_name:com.huhy.web.common.java
     *@Date:9:48 2018/1/15
     *@Description:  hashmap
     */
    @Test
    public void test(){
        /**
         * 对比：
         * 1.继承类不同，实现接口相同<Map>
         *      Hashtable继承自Dictionary类，而HashMap继承自AbstractMap类。但二者都实现了Map接口
         * 2.默认大小
         *      hashtable是原始出现就存在的，hashmap是在jdk1.2之后引入的
         *      hashtale是11，hashmap是16
         *          hashmap源码分析：
         *              static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16   默认容量16
         *              static final float DEFAULT_LOAD_FACTOR = 0.75f;     //填充因子    这是控制存储的界点效率   也就是在16*0.75  达到这个值后会自动扩容
         *          hashtable源码：
         *              public Hashtable() {
         *                   this(11, 0.75f);
         *                }
         *
         * 3.使用场景
         *      hashtable是线程安全的【同步处理】，有关键字synchronized，适用于多线程下;
         *      hashmap是线程不安全的【异步处理】，适用于单线程下，相对于hashtable,处理效率较高【注意：多线程环境下可以采用concurrent并发包下的concurrentHashMap】
         *      concurrentHashMap：在jdk1.5之后出现。现在一般用concurrentHashMap就可以规避掉hashtable
         * 4.存储要求
         *      hashtable不允许key的空值插入（null）,hashmap则是允许的
         * 5.原理实现
         *      hashtable基于单链表实现，hashmap基于哈希表实现
         *          都是链表+数组实现
         *
         *
         */
        Map hashMap = new HashMap();
        Map concurrentHashMap = new ConcurrentHashMap();
        Map table = new Hashtable();
        Collections.synchronizedMap(hashMap);
        System.gc();

    }
}
