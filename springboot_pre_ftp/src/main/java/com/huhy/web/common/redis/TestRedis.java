package com.huhy.web.common.redis;

import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.Iterator;
import java.util.Set;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/10/12 11:01
 * @Class 测试java操作redis的一些操作:
 */
public class TestRedis {

    private  static String host = "127.0.0.1";
    private  static int port = 6379;
    private Jedis jedisUtil = getRedisUtil(host,port);

    /**
     * 测试连接
     */
    @Test
    public void test1(){
        System.out.println(jedisUtil);
    }




    /**
     *
     * @param host 主机ip
     * @param port 端口号
     * @return  redis的连接
     */
    public static Jedis getRedisUtil(String host, int port){
        return new Jedis(host,port);
    }
}
