package com.huhy.web.common.fastjson;

import com.alibaba.fastjson.JSON;
import com.huhy.web.entity.User;
import org.junit.Test;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/10/12 14:22
 * @Class 测试fastjson的使用:
 */
public class TestFastJson {

    @Test
    public void test1(){
        User user = new User("123","huhy");
        String s = JSON.toJSONString(user);
        System.out.println(s);

        User u = JSON.parseObject(s,User.class);
        System.out.println(u);
    }
}
