package com.huhy.web.common.SCADA;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author:huhy
 * @DATE:Created on 2017/12/14 11:27
 * @Modified By:
 * @Class 测试浏览器打开exe的方法:
 */
@Controller
public class TestJS {
    @RequestMapping("/scada")
    public String changerTran(){
        return "/scada/1";
    }
}
