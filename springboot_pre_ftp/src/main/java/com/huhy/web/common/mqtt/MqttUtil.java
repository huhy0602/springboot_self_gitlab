package com.huhy.web.common.mqtt;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * @Author:huhy
 * @DATE:Created on 2018/1/18 12:59
 * @Modified By:
 * @Class Description:
 */
public class MqttUtil {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MqttUtil.class);
    //region 网络设定
    private static String serverIP = "183.230.40.39";               // 服务器ip
    private static int serverPort = 6002;                           // 服务器端口
    private static String deviceId = "24805679";                            // 设备id
    private static String userId = "118173";                           // 产品id
    private static String password = "pub1234";                         // 鉴权信息
    //endregion
    //region 消息类型
    public static final int MQTT_CONNECTED = 1000;                 // mqtt连接成功
    public static final int MQTT_CONNECTFAIL = 1001;               // mqtt连接失败
    public static final int MQTT_DISCONNECT = 1002;                // mqtt断开连接
    public static final int MQTT_SUBSCRIBED = 1010;                // 订阅成功
    public static final int MQTT_SUBSCRIBEFAIL = 1011;             // 订阅失败
    public static final int MQTT_MSG_TEST = 2001;                  // 接收到TEST消息
    public static final int MQTT_PUBLISHED = 2010;                 // 发布成功
    public static final int MQTT_PUBLISHFAIL = 2011;               // 发布失败
    //endregion
    //region MQTT客户端服务
    private static MqttUtil instance;                               // 单例对象
    private MqttClient mqttClient;                           // mqtt客户端
    private MqttConnectOptions option;                              // mqtt设置
    private MqttCallback clientCallback;                            // 客户端回调
   // private Context mContext;                                       // 上下文
    private ArrayList<MsgHandler> listenerList = new ArrayList<MsgHandler>(); // 消息接收者
    //endregion
    //封闭构造函数
    private MqttUtil(){}
    //获取单例
    public static MqttUtil getInstance(){
        if(instance == null){
            instance = new MqttUtil();
            //deviceId =  android.os.Build.SERIAL + (int)(Math.random() * 10);
        }
        return instance;
    }
    //初始化连接
    /*public void initMqtt(Context context) {
        mContext = context;
        connect();
    }*/
    //初始化连接
    public void initMqtt() {
        connect();
        System.out.println("初始化成功");
    }
    //重连
    public void reConnect() {
        //尝试重连
        connect();
    }
    //发布消息
    public void publish(String topic, String payload) {
        MqttMessage msg = new MqttMessage();
        msg.setPayload(payload.getBytes());
        msg.setQos(1);
        if (mqttClient != null){
            if(mqttClient.isConnected()) {
                try {
                    mqttClient.publish(topic, msg);
                    DispachEvent(MQTT_PUBLISHED);
                } catch (MqttException e) {
                    //发布失败
                    logger.info("mqtt", "发布失败" + e);
                    DispachEvent(MQTT_PUBLISHFAIL);
                }
            } else {
                logger.info("mqtt", "网络未连接 - 尝试重新连接" );
                connect();
                DispachEvent(MQTT_PUBLISHFAIL);
            }
        } else {
            logger.info("mqtt", "客户端初始化失败" );
            DispachEvent(MQTT_PUBLISHFAIL);
        }
    }
    //订阅主题
    public void subscribe(String topic){
        try {
            //订阅消息
            mqttClient.subscribe(topic, 0);
            DispachEvent(MQTT_SUBSCRIBED);
        } catch (MqttException e) {
            DispachEvent(MQTT_SUBSCRIBEFAIL);
            logger.info("mqtt", "订阅错误:" + e);
        }
    }
    //返回是否连接
    public boolean isConnected(){
        return mqttClient.isConnected();
    }
    //获取本机deviceId
    public String getDeviceId(){
        return deviceId;
    }


    //连接
    private void connect() {
        if (mqttClient == null) {
            option = new MqttConnectOptions();
            // 设置超时时间 单位为秒
            option.setConnectionTimeout(10);
            // 设置会话心跳时间 单位为秒 服务器会每隔1.5*20秒的时间向客户端发送个消息判断客户端是否在线，但这个方法并没有重连的机制
            option.setKeepAliveInterval(20);
            option.setUserName(userId);
            option.setPassword(password.toCharArray());
            option.setCleanSession(false);
            //设置回调
            clientCallback = new MqttCallback() {

                public void connectionLost(Throwable cause) {
                    //断开连接
                    DispachEvent(MQTT_DISCONNECT);
                }

                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    //接收到消息
                    logger.info("mqtt", "接收到信息:" + topic);
                    DispachMessage(topic, message);
                }

                public void deliveryComplete(IMqttDeliveryToken token) {
                    //publish成功后调用
                    logger.info("mqtt","发送成功");
                }
            };
            try {
               //String host = "tcp://"+serverIP+":"+serverPort;
                String host = "tcp://"+serverIP+":"+serverPort;
               // host += "\r\n"; 错误拼接字符串
                System.out.println(host);//tcp:
                mqttClient = new MqttClient(host, deviceId, new MemoryPersistence());
                mqttClient.setCallback(clientCallback);
                mqttClient.connect(option);
            } catch (Exception e) {
                logger.info("mqtt", "启动服务错误:" + e);
            }
        }

    }
    //region 消息转发部分
    //添加接收者
    public void addListener(MsgHandler msgHandler){
        if(!listenerList.contains(msgHandler)) {
            listenerList.add(msgHandler);
        }
    }
    //移除接收者
    public void removeListener(MsgHandler msgHandler){
        listenerList.remove(msgHandler);
    }
    //移除所有接收者
    public void removeAll(){
        listenerList.clear();
    }
    //发送消息
    public void DispachMessage(String type, Object data){
        if(listenerList.isEmpty()) {
            logger.info("mqtt", "没有消息接收者:" + type);
            return;
        }
        logger.info("mqtt", "发送消息:" + type);
        for (MsgHandler msgHandler : listenerList)
        {
            msgHandler.onMessage(type, data);
        }
    }
    //发送事件
    public void DispachEvent(int event){
        if(listenerList.isEmpty()) {
            logger.info("mqtt", "没有消息接收者:");
            return;
        }
        logger.info("mqtt", "派发事件:" + event);
        for (MsgHandler msgHandler : listenerList)
        {
            msgHandler.onEvent(event);
        }
    }
    //endregion


}
