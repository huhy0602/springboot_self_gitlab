package com.huhy.web.common.Pattern;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author:huhy
 * @DATE:Created on 2017/12/21 10:04
 * @Modified By:
 * @Class 测试正则表达式:
 */
public class TestPattern {
    @Test
    /**
     *@Author: huhy
     *@Package_name:com.huhy.web.common.Pattern
     *@Date:10:14 2017/12/21
     *@Description:
     */
    public void test1() {

        // 要验证的字符串
        String str = "service@xsoftlab.net";
        // 邮箱验证规则
        String regEx = "[a-zA-Z_]{1,}[0-9]{0,}@(([a-zA-z0-9]-*){1,}\\.){1,3}[a-zA-z\\-]{1,}";
        // 编译正则表达式
        Pattern pattern = Pattern.compile(regEx);
        // 忽略大小写的写法
        // Pattern pat = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(str);
        // 字符串是否与正则表达式相匹配
        boolean rs = matcher.matches();
        System.out.println(rs);
    }

}
