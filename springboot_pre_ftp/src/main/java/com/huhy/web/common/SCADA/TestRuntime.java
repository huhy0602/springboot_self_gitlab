package com.huhy.web.common.SCADA;

import org.junit.Test;

import java.io.IOException;

/**
 * @Author:huhy
 * @DATE:Created on 2017/12/19 13:41
 * @Modified By:
 * @Class 测试Runtime的的简单实用:
 */
public class TestRuntime {

    private Runtime run = Runtime.getRuntime();
    /**
     *@Author: huhy
     *@Package_name:com.huhy.web.common.SCADA
     *@Date:13:42 2017/12/19
     *@Description:
     */
    @Test
    public void TestRuntime1(){
        try {
            String cmd =  "calc";

            run.exec(cmd);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test(){
        //cmd /c dir \\windows
        try {
            run.exec("write.exe");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
