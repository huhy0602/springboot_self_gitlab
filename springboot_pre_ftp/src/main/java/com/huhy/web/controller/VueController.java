package com.huhy.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/10/13 13:42
 * @Class Description:
 */
@Controller
public class VueController {
    /**
     * 测试vue的用法
     * @return
     */
    @RequestMapping("/vue")
    public String testVue(){
        System.out.println("1111hhhhh11111");
        System.out.println("1111hhhhh11111");
        return "/vue/vue";//如果指定文件夹下的，吧文件夹写上即可，如、vue/vue
    }


}
