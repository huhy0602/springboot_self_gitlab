package com.huhy.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @Author:huhy
 * @DATE:Created on 2018/1/3 15:46
 * @Modified By:
 * @Class Description:
 */
@RestController
@RequestMapping("/home")
public class BController {
    @Autowired
    @Qualifier("primaryJdbcTemplate")
    JdbcTemplate jdbcTemplate1;

    @Autowired
    @Qualifier("secondaryJdbcTemplate")
    JdbcTemplate jdbcTemplate2;

    @RequestMapping("/test1")
    public String test1(){
        System.out.println(jdbcTemplate1);
        List<Map<String,Object>> list = jdbcTemplate1.queryForList("select * from user");
        return Arrays.asList(list).toString();
    }

    @RequestMapping("/test2")
    public String test2(){
        List<Map<String,Object>> list = jdbcTemplate2.queryForList("SELECT * from ftp_user");
        System.out.println("123");
        return Arrays.asList(list).toString()+"------------123";
    }

}
