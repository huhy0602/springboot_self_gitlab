package com.huhy.web.controller;

import com.huhy.web.entity.User;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * Created by hhy_0 on 2017/7/7.
 */
@RestController
public class HelloController
{
    @RequestMapping("/hello")
    public String index(){
        System.out.println("22222222222222");
        return "Hello World";
    }

    @RequestMapping("/")
    public String index(ModelMap map) {
        // 加入一个属性，用来在模板中读取
        map.addAttribute("host", "http://blog.didispace.com");

        // return模板文件的名称，对应src/main/resources/templates/index.html
        return "index";
    }
    @RequestMapping("/getUser")
    public User getUser() {
        User user = new User();
        user.setId("12");
        user.setName("hhy");
        return user;
    }

}
