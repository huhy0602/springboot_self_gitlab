package com.huhy.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/9/25 14:09
 * @Class 整合前端框架awesome:
 */
@Controller
public class AwesomeController {
    protected static Logger logger= LoggerFactory.getLogger(AwesomeController.class);


    @RequestMapping("/awesome")
    public String  awesome(){
        logger.debug("访问 awesome");
        return "awesome1";
    }

}
