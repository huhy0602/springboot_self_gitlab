package com.huhy.web.controller;

import org.springframework.web.bind.annotation.*;

/**
 * @Author:huhy
 * @DATE:Created on 2018/1/11 14:46
 * @Modified By:
 * @Class Description:
 */
@RestController
@RequestMapping("/test")

public class TestRestful {
    @RequestMapping(value = "/{id}/{name}",method = RequestMethod.GET)
    @ResponseBody
    public void test1(@PathVariable("id") int id,@PathVariable("name") String name){
        if(name == null){
            System.out.println("name = null");
        }
        System.out.println("webflux");
    }
    @RequestMapping(value = "/{id}/{name}",method = RequestMethod.POST)
    @ResponseBody
    public void test2(@PathVariable("id") int id,@PathVariable("name") String name){
        if(name == null){
            System.out.println("name = null111");
        }
        System.out.println("webflux");
    }



}
