package com.huhy.web.controller;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 * @Author:huhy
 * @DATE:Created on 2018/1/11 17:38
 * @Modified By:
 * @Class Description:
 */
@Component
@RequestMapping
@Path("/welcome")
public class JerseyResource {
    @GET
    @Path("/wrong")
    public void getPage(@Context HttpServletRequest request) {
        System.out.println("这是一个get方法！");
    }
    @POST
    @Path("/wrong")
    public void postPage(@Context HttpServletRequest request) {
        System.out.println("这是一个post方法！");
    }
    @Path("/book")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String sayHello() {
        return "Hello Books!";
    }

}
