package com.huhy.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/11/20 10:21
 * @Class Description:
 */
@Controller
public class WebSocketController {
    @RequestMapping("/web")
    public String testVue(){
        return "/websocket/testWebSocket";//如果指定文件夹下的，吧文件夹写上即可，如、vue/vue
    }

}
