package com.huhy.web.controller;

import com.huhy.web.entity.User;
import com.huhy.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ${huhy} on 2017/8/5.
 */


@RestController
public class JDBCController {
   /* @Autowired
    private JdbcTemplate jdbcTemplate;
    */
    @Autowired
    private UserService userService;

    /**
     * 测试数据库连接
     */
    /*@RequestMapping("/jdbc/getUsers")
    public List<Map<String, Object>> getDbType(){
        String sql = "select * from user";
        List<Map<String, Object>> list =  jdbcTemplate.queryForList(sql);
        for (Map<String, Object> map : list) {
            Set<Entry<String, Object>> entries = map.entrySet( );
            if(entries != null) {
                Iterator<Entry<String, Object>> iterator = entries.iterator( );
                while(iterator.hasNext( )) {
                    Entry<String, Object> entry =(Entry<String, Object>) iterator.next( );
                    Object key = entry.getKey( );
                    Object value = entry.getValue();
                    System.out.println(key+":"+value);
                }
            }
        }
        return list;
    }*/


    @RequestMapping("/jdbcTest/{id}")
    public User jdbcTest(@PathVariable("id") String id){
        return userService.queryById(id);
    }


    @RequestMapping("/jdbcTestName/{name}/{id}")
    public User queryByName(@PathVariable("name") String name,@PathVariable("id") String id){
        return userService.queryByName(name,id);
    }
    @RequestMapping("/name")
    public  String getName(){
        return "huhy";
    }

}
