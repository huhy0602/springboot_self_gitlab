package com.huhy.web.mapper;

import com.huhy.web.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;


/**
 * @Author:{huhy}
 * @DATE:Created on/8/5 22:19
 * @Class Description:
 */

@Repository
public interface UserMapper {
    /*
    * 通过id查询对应信息
    * */
    @Select("SELECT * FROM USER WHERE id = #{id}")
    User selectByPrimaryKey(String id);
    /*
    * 通过name 查询对应信息
    * */
    @Select("Select * from user where name = #{name} and id = #{id}")
    User selectByName(@Param("name") String name, @Param("id") String id);

    /**
     * @param id
     * @param name
     * @return
     */
    @Insert("INSERT INTO USER(id,NAME) VALUES(#{id},#{name})")
    int insert(@Param("id") String id,@Param("name") String name);
}
