package com.huhy.web.mapper;

import com.huhy.web.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/10/9 15:05
 * @Class Description:
 */
public interface PersonRepository extends JpaRepository<Person,Long>{

    Person findByUserName(String userName);
    Person findByUserNameOrEmail(String username, String email);
}
