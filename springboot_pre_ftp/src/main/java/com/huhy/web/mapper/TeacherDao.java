package com.huhy.web.mapper;

import com.huhy.web.entity.Teacher;
import org.springframework.data.repository.CrudRepository;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/10/13 10:59
 * @Class Description:
 */
public interface TeacherDao   extends CrudRepository<Teacher, Long> {
}
