package com.huhy.web.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/10/12 17:54
 * @Class 测试spring boot的定时任务:
 */
@Configuration
@EnableScheduling // 启用定时任务
public class SchedulingConfig {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Scheduled(cron = "0 0/5 * * * ?") // 每20秒执行一次
    public void scheduler() {
        logger.info("定时任务已启动---------------- ");
    }

}
