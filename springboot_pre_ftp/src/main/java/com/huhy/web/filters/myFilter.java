package com.huhy.web.filters;

import org.springframework.beans.factory.annotation.Configurable;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/10/9 14:49
 * @Class
 *    自定义拦截器:
 * 1  实现Filter接口，实现Filter方法
 * 2  添加@Configuration 注解，将自定义Filter加入过滤链
 */
@Configurable
public class myFilter {
    public class MyFilter implements Filter {
        @Override
        public void destroy() {
            // TODO Auto-generated method stub
        }

        @Override
        public void doFilter(ServletRequest srequest, ServletResponse sresponse, FilterChain filterChain)
                throws IOException, ServletException {
            // TODO Auto-generated method stub
            HttpServletRequest request = (HttpServletRequest) srequest;
            //System.out.println("this is MyFilter,url :"+request.getRequestURI()+"-------------------");
            filterChain.doFilter(srequest, sresponse);
        }

        @Override
        public void init(FilterConfig arg0) throws ServletException {
            // TODO Auto-generated method stub
        }
    }
}


