package com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;


@SpringBootApplication
@MapperScan("com.huhy.web.mapper")//扫描dao接口
public class DemoApplication extends SpringBootServletInitializer {
    /**
     *
     * @param args
     */

	public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class,args);
    }
}
