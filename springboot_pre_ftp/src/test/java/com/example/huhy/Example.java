package com.example.huhy;

/**
 * @Author:huhy
 * @DATE:Created on 2018/2/1 10:31
 * @Modified By:
 * @Class Description:接口回调是指：
 * 可以把实现某一接口的类创建的对象的引用赋给该接口声明的接口变量，
 * 那么该接口变量就可以调用被类实现的接口的方法。
 * 实际上，当接口变量调用被类实现的接口中的方法时，
 * 就是通知相应的对象调用接口的方法，这一过程称为对象功能的接口回调。
 *
 * 类似于   web项目中  service接口 --> service的impl实现接口，------>controller通过接口对象调用接口方法
 */
interface People{
    void peopleList();
}
class Student implements People{
    public void peopleList(){
        System.out.println("student的接口回调-------》");
    }
}
class Teacher implements People{
    public void peopleList(){
        System.out.println(" teacher的接口回调------》");
    }
}
public class Example{
    public static void main(String args[]){
        People a;             //声明接口变量
        a=new Student();      //实例化，接口变量中存放对象的引用
        a.peopleList();        //接口回调
        a=new Teacher();     //实例化，接口变量中存放对象的引用
        a.peopleList();       //接口回调
    }
}
