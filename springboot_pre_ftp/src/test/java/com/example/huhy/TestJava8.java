package com.example.huhy;

import org.junit.Test;

import java.util.*;
import java.util.function.Predicate;
/**
 * @Author:{huhy}
 * @DATE:Created on 2017/9/29 13:40
 * @Class Description:
 */
public class TestJava8 {

    /**
     * 测试java8的新特性
     * @param args
     */
    public static void  main(String[] args){
        List<String> names1 = new ArrayList<String>();
        names1.add("Mahesh ");
        names1.add("Suresh ");
        names1.add("Ramesh ");
        names1.add("Naresh ");
        names1.add("Kalpesh ");

        List<String> names2 = new ArrayList<String>();
        names2.add("Mahesh ");
        names2.add("Suresh ");
        names2.add("Ramesh ");
        names2.add("Naresh ");
        names2.add("Kalpesh ");

        TestJava8 tester = new TestJava8();

        System.out.println("Sort using Java 7 syntax: ");
        tester.sortUsingJava7(names1);
        System.out.println(names1);

        System.out.println("Sort using Java 8 syntax: ");
        tester.sortUsingJava8(names2);
        System.out.println(names2);
    }

//   java7   的排序
    private void sortUsingJava7(List<String> names){
        //sort using java 7
        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        });
    }
//java8   的排序
    private void sortUsingJava8(List<String> names){
        //sort using java 8
        Collections.sort(names, (s1, s2) ->  s1.compareTo(s2));
    }

    /**
     * 测试lambda表达式
     */
   @Test
    public void test1(){
        TestJava8 tester = new TestJava8();

        //with type declaration
        MathOperation addition = ( a,  b) -> a + b;

        //with out type declaration
        MathOperation subtraction = (a, b) -> a - b;

        //with return statement along with curly braces
        MathOperation multiplication = ( a,  b) -> { return a * b; };
        //without return statement and without curly braces
        MathOperation division = ( a,  b) -> a / b;

        System.out.println("10 + 5 = " + tester.operate(10, 5, addition));
        System.out.println("10 - 5 = " + tester.operate(10, 5, subtraction));
        System.out.println("10 x 5 = " + tester.operate(10, 5, multiplication));
        System.out.println("10 / 5 = " + tester.operate(10, 5, division));

        //with parenthesis
        GreetingService greetService1 = message -> System.out.println("Hello " + message);

        //without parenthesis
        GreetingService greetService2 = (message) -> System.out.println("Hello " + message);

        greetService1.sayMessage("Mahesh");
        greetService2.sayMessage("Suresh");
    }

    interface MathOperation {
        int operation(int a, int b);
    }

    interface GreetingService {
        void sayMessage(String message);
    }

    private int operate(int a, int b, MathOperation mathOperation){
        return mathOperation.operation(a, b);
    }

    //测试java8的方法引用
    @Test
    public  void test2(){
        List names = new ArrayList();
        names.add("Mahesh");
        names.add("Suresh");
        names.add("Ramesh");
        names.add("Naresh");
        names.add("Kalpesh");
        names.forEach(System.out::println);
        System.out.println("================================");

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        // Predicate<Integer> predicate = n -> true
        // n is passed as parameter to test method of Predicate interface
        // test method will always return true no matter what value n has.
        System.out.println("Print all numbers:");
        //pass n as parameter
        eval(list, n->true);

        // Predicate<Integer> predicate1 = n -> n%2 == 0
        // n is passed as parameter to test method of Predicate interface
        // test method will return true if n%2 comes to be zero
        System.out.println("Print even numbers:");
        eval(list, n-> n%2 == 0 );

        // Predicate<Integer> predicate2 = n -> n > 3
        // n is passed as parameter to test method of Predicate interface
        // test method will return true if n is greater than 3.
        System.out.println("Print numbers greater than 3:");
        eval(list, n-> n > 3 );
    }


    public static void eval(List<Integer> list, Predicate<Integer> predicate) {
        for(Integer n: list)  {
            if(predicate.test(n)) {
                System.out.println(n + " ");
            }
        }
    }
        @Test
        public  void test11(){
            FourWheeler vehicle = new Car();
            vehicle.print();
        }


    interface Vehicle {
        default void print(){
            System.out.println("I am a vehicle!");
        }
        static void blowHorn(){
            System.out.println("Blowing horn!!!");
        }
    }

    interface FourWheeler {
        default void print(){
            System.out.println("I am a four wheeler!");
        }
    }

    class Car implements Vehicle, FourWheeler {
        public void print(){
            Vehicle.super.print();
            System.out.println("1111111111111111111111");
            FourWheeler.super.print();
            System.out.println("222222222222222222222");
            Vehicle.blowHorn();
            System.out.println("I am a car!!!!!!!!!!!");
        }
    }

}
