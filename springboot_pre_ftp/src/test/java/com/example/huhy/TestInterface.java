package com.example.huhy;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @Author:huhy
 * @DATE:Created on 2018/2/1 10:53
 * @Modified By:
 * @Class Description:
 */
public class TestInterface {
    /**
     * 接口对象以匿名对象方式实现
     */
    @Test
    public void  test1(){
        //接口的实现，如果有实现类
        Formula formula = new Formula() {
            @Override
            public double calculate(int a) {
                return 0;
            }
        };
        formula.calculate(12);
        //多态的一种实现
        A a = new B();
        a.sub("message");
        List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");

        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String a, String b) {
                return b.compareTo(a);
            }
        });
        names.forEach(System.out::println);
        System.out.println("--------------------");
        Collections.sort(names, (c, b) -> b.compareTo(c));
        names.forEach(System.out::println);
    }
}
interface Formula {
    double calculate(int a);

    default double sqrt(int a) {
        return Math.sqrt(a);
    }
}
interface A{
    void sub(String message);
}
class B implements A{
    @Override
    public void sub(String message){
        System.out.println(message+"----->");
    }
}