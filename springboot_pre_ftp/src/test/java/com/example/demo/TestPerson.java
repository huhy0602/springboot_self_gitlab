package com.example.demo;

import com.DemoApplication;
import com.huhy.web.entity.Person;
import com.huhy.web.mapper.PersonRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author:{huhy}
 * @DATE:Created on 2017/10/9 15:06
 * @Class Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=DemoApplication.class)
@WebAppConfiguration
public class TestPerson {
    @Autowired
    private PersonRepository personRepository;

    @Test
    public void test() throws Exception{
        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG);
        String formattedDate = dateFormat.format(date);

       /* personRepository.save(new Person("aa1", "aa@126.com", "aa", "aa123456", formattedDate));
        personRepository.save(new Person("bb2", "bb@126.com", "bb", "bb123456",formattedDate));
        personRepository.save(new Person("cc3", "cc@126.com", "cc", "cc123456",formattedDate));
*/
       //personRepository.delete(personRepository.findByUserName("aa1"));
        List<Person> all = personRepository.findAll();
        System.out.println(all.size());


    }

    /**
     * 查看ThymeleafProperties源码
     */
    @Test
    public void  test2(){
        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG);
        String formattedDate = dateFormat.format(date);
        personRepository.save(new Person("aa1", "aa@126.com", "aa", "aa123456", formattedDate));

    }

}
