package com.example.demo;

import com.DemoApplication;
import com.huhy.web.mapper.PersonRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @Author:huhy
 * @DATE:Created on 2018/1/3 18:00
 * @Modified By:
 * @Class Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@WebAppConfiguration
public class TestDemoServer {
    @Autowired
    private PersonRepository personRepository;

    @Test
    public void test(){
        System.out.println(personRepository.toString());
    }
}
