package com.example.swagger;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : HUHY    http://www.cnblogs.com/huhongy/
 * @Project_name:demo
 * @date:2020/5/29 16:02
 * @email:hhy_0602@163.com
 * @description:{todo}
 */
@Data
@ApiModel(value = "班级")
public class Entity {
    @ApiModelProperty(value = "班级")
    private String clazz;
    @ApiModelProperty(value = "组")
    private String group;
}
