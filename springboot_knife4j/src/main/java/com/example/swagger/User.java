package com.example.swagger;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author : HUHY    http://www.cnblogs.com/huhongy/
 * @Project_name:demo
 * @date:2020/5/29 14:37
 * @email:hhy_0602@163.com
 * @description:{todo}
 */
@Data
@ApiModel
@ToString
public class User implements Serializable {
    @ApiModelProperty(value = "逻辑主键")
    private long id;
    @ApiModelProperty(value = "姓名",example = "huhy")
    private String name;
    @ApiModelProperty(value = "年龄")
    private int age;
    @ApiModelProperty(value = "密码")
    private String pass;
    @ApiModelProperty(value = "地址")
    private String addr;
    @ApiModelProperty(value = "班级")
    private Entity entity;

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", pass='" + pass + '\'' +
                ", addr='" + addr + '\'' +
                '}';
    }
}
