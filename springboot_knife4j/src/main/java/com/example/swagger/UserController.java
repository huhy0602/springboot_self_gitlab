package com.example.swagger;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : HUHY    http://www.cnblogs.com/huhongy/
 * @Project_name:demo
 * @date:2020/5/29 14:38
 * @email:hhy_0602@163.com
 * @description:{todo}
 */
@RestController
@Api(tags = "用户接口类")// tag指的是swagger上面的中文描述
@ApiSupport(order = 100)
public class UserController {


    @ApiOperationSupport(author = "huhongyang",order = 10)
    @ApiOperation("测试接口排序接口10")
    @GetMapping("/getMap2")
    public String getMap2(){
        return "huhy";
    }

    @ApiOperationSupport(author = "huhy",order = 99)
    @ApiOperation("测试排序接口99")
    @GetMapping("/getMap")
    public String getMap(){
        return "huhy";
    }

    @ApiOperationSupport(author = "huhy",order = 100)
    @ApiOperation("测试排序接口100")
    @GetMapping("/getMap3")
    public String getMap3(){
        return "huhy";
    }

    @ApiOperationSupport(author = "huhy",order = 15,ignoreParameters = {"id"})
    @ApiOperation("测试排序接口15")
    @GetMapping("/getMap4")
    public String getMap4(User user){
        return user.toString();
    }
    @ApiOperationSupport(author = "huhy",order = 31,ignoreParameters = {"id"})
    @ApiOperation("忽略属性接口测试json")
    @PostMapping("/getMap5")
    public String getMap5(@RequestBody User user){
        return user.toString();
    }

    @ApiOperationSupport(author = "huhy",order = 31,includeParameters = {"user.entity.clazz"})
    @ApiOperation("属性接口测试json")
    @PostMapping("/getMap6")
    public String getMap6(@RequestBody User user){
        return user.toString();
    }
}
