package com.example.swagger;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : HUHY    http://www.cnblogs.com/huhongy/
 * @Project_name:demo
 * @date:2020/5/29 15:03
 * @email:hhy_0602@163.com
 * @description:{todo}
 */
@RestController
@Api(tags = "测试接口类1",position = 300)
@RequestMapping("/test1")
public class TestController {

    @GetMapping("/getname")
    @ApiOperation(value = "测试接口顺序方法")
    public String getName(String name){
        return "hello "+ name;
    }

    @GetMapping(value = "/getname2")
    @ApiOperation(value = "测试接口顺序方法")
    public String getName2(@RequestParam(name = "yang") String name){
        return "hello "+ name;
    }
}
