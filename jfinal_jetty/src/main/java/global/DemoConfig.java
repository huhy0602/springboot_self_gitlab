package global;

import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.plugin.redis.RedisPlugin;
import com.jfinal.template.Engine;
import jfinal_config.JfinalController;
import jfinal_config.ProkitUtils;
import jfinal_db.Jfinal_db_record;
import jfinal_db.RenderController;
import jfinal_db.User;
import jfinal_db.UserController;

/**
 * @author : huhy on 2018/9/28.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:jfinal_1
 * @description:全局配置
 */
public class DemoConfig extends JFinalConfig {

    /**
     * @author huhy
     * @ClassName:DemoConfig
     * @date 2018/9/28 14:56 
     * @Description: 配置JFinal常量值
     */
    @Override
    public void configConstant(Constants me) {


        //开发模式
        /**
         * 在开发模式下，JFinal会对每次请求输出报告，如输出本次请求的URL、Controller、Method以及请求所携带的参数。
         *      eg：
         *       JFinal-3.4 action report -------- 2018-09-28 14:57:19 --------------------------
                 Url         : GET /
                 Controller  : jfinal_config.JfinalController.(JfinalController.java:1)
                 Method      : index
                 --------------------------------------------------------------------------------
         * */
        // devMode 配置为 true，将支持模板实时热加载
        me.setDevMode(true);
        /*加载mysql配置文件*/
        //loadPropertyFile("config_mysql.properties");

        // 加载少量必要配置，随后可用PropKit.get(...)获取值
        //PropKit.use("config_mysql.txt");
        PropKit.use("config_mysql.properties");

    }
    /**
     * @author huhy
     * @ClassName:DemoConfig
     * @date 2018/9/28 14:56 
     * @Description: 配置访问路由
     */
    @Override
    public void configRoute(Routes me) {
        /**
         * 1> me.add   的controllerKey只是映射到类上
         *      http://localhost:9091/jfinal  默认请求到类的index方法
         *      http://localhost:9091/jfinal/方法名  这就会请求到对应方法上
         * 2>   finalView = baseViewPath + viewPath + view
         *      当view以 “/” 字符打头时表示绝对路径，baseViewPath 与 viewPath 将被忽略。
         *      当请求页面时。
         *          如果 render("/jfinal_config.html");  代表绝对路径，直接去webapp下寻找对应页面。
         *          如果 render("jfinal_config.html");   寻找路径为：项目路径/123/jfinal/jfinal_config.html
         *      注意：
         *             baseViewPath  通过 setBaseViewPath(baseViewPath)
         *             viewPath :    如果add方法传viewPath，则按照上传viewPath ,当viewPath未指定时默认值为controllerKey。
         *             view     :    render(view);
         * */
        me.setBaseViewPath("123");
        /*测试jfinal的请求，？和urlPara传参*/
        me.add("/jfinal", JfinalController.class);
        /**
         *public Routes add(String controllerKey, Class<? extends Controller> controllerClass, String viewPath)
          public Routes add(String controllerKey, Class<? extends Controller> controllerClass)
         * */
        /**测试读取文件的类*/
        me.add("/prokit", ProkitUtils.class);


        /*测试mysql的连接  并测试表单上传*/
        me.add("/user", UserController.class);

        /** 验证返回视图解析*/
        me.add("/render", RenderController.class);

        /** 验证db+Record*/
        me.add("/record", Jfinal_db_record.class);



    }
    /**
     * @author huhy
     * @Description: 配置Template Engine  模版引擎
     */
    @Override
    public void configEngine(Engine me) {

    }
    /**
     * @author huhy
     * @Description: 配置JFinal的Plugin插件等
     */
    @Override
    public void configPlugin(Plugins me) {
        /* 加载数据库配置文件,连接mysql数据*/
        /*C3p0Plugin c3p0=new C3p0Plugin(getProperty("jdbcUrl"),
                getProperty("user"),
                getProperty("password"));*/
        // 配置 druid 数据库连接池插件
        DruidPlugin druidPlugin = createDruidPlugin();
        me.add(druidPlugin);
        /**
         * 以上代码中arp.addMapping("user", User.class)，表的主键名为默认为 "id"，如果主键名称为 "user_id" 则需要手动指定，
         *          如：arp.addMapping("user", "user_id", User.class)。
         * */
        ActiveRecordPlugin activeRecord=new ActiveRecordPlugin(druidPlugin);
        activeRecord.setDialect(new MysqlDialect());
        //activeRecord.addMapping("user", User.class).setShowSql(Boolean.parseBoolean(getProperty("showSql")));
        activeRecord.addMapping("user", User.class).setShowSql(Boolean.parseBoolean(PropKit.get("showSql")));
        me.add(activeRecord);

        //配置缓存插件
        //me.add(new EhCachePlugin());


        // 用于缓存bbs模块的redis服务
        RedisPlugin bbsRedis = new RedisPlugin("bbs", "localhost");
        me.add(bbsRedis);

    }

    public static DruidPlugin createDruidPlugin() {
        return new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
    }

    /**
     * @author huhy
     * @Description: 配置JFinal的全局拦截器
     */
    @Override
    public void configInterceptor(Interceptors me) {

    }
    /**
     * @author huhy
     * @Description: 配置JFinal的Handler
     */
    @Override
    public void configHandler(Handlers me) {

    }
    /**
     * @author huhy
     * @ClassName:DemoConfig
     * @date 2018/9/28 14:38 
     * @Description: 启动方法
     */
    public static void main(String[] args) {
        JFinal.start("jfinal_jetty/src/main/webapp", 7300, "/");
    }


    // 系统启动完成后回调
    @Override
    public void afterJFinalStart() {
        System.out.println("启动之后jfianl 项目时执行----------");

    }

    // 系统关闭之前回调
    @Override
    public void beforeJFinalStop() {
        System.out.println("关闭之前jfianl 项目时执行----------");
    }
}
