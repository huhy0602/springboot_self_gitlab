package jfinal_db;

import com.jfinal.core.Controller;

/**
 * @author : huhy on 2018/9/29.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:jfinal_db
 * @description:渲染不同类型的视图并返回给客户端
 */
public class RenderController extends Controller{
    /**
     * // 渲染名为test.html的视图，且视图类型为 JFinal Template
     renderTemplate(”test.html”);

     // 生成二维码
     renderQrCode("content");

     // 渲染名为test.html的视图，且视图类型为FreeMarker
     renderFreeMarker(”test.html”);

     // 渲染名为test.html的视图，且视图类型为Velocity
     renderVelocity(“test.html”);

     // 将所有setAttr(..)设置的变量转换成 json 并渲染
     renderJson();

     // 以 "users" 为根，仅将 userList 中的数据转换成 json 并渲染
     renderJson(“users”, userList);

     // 将user对象转换成 json 并渲染
     renderJson(user);

     // 直接渲染 json 字符串
     renderJson("{\"age\":18}" );

     // 仅将setAttr(“user”, user)与setAttr(“blog”, blog)设置的属性转换成json并渲染
     renderJson(new  String[]{"user", "blog"});

     // 渲染名为test.zip的文件，一般用于文件下载
     renderFile("test.zip");

     // 渲染纯文本内容 "Hello JFinal"
     renderText("Hello JFinal");

     // 渲染 Html 内容 "Hello Html"
     renderHtml("Hello Html");

     // 渲染名为 test.html 的文件，且状态为 404
     renderError(404 , "test.html");

     // 渲染名为 test.html 的文件，且状态为 500
     renderError(500 , "test.html");

     // 不渲染，即不向客户端返回数据
     renderNull();

     // 使用自定义的MyRender来渲染
     render(new MyRender());
     * */
    public void index(){
        // 生成二维码
        renderQrCode("huhy12222222222222222222",500,500);
    }
}
