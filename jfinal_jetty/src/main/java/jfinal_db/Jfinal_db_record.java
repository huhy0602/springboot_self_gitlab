package jfinal_db;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @author : huhy on 2018/9/30.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:jfinal_db
 * @description:db_record实现数据库的操作
 */
public class Jfinal_db_record extends Controller{

    public void save(){
        /**
         * 来源 官网demo
         *
         * // 创建name属性为huhy,age属性为18的record对象并添加到数据库
         Record user = new Record().set("name", "huhy").set("age", 18);
         Db.save("user", user);

         // 删除id值为18的user表中的记录
         Db.deleteById("user", 18);

         // 查询id值为18的Record将其name属性改为huhy并更新到数据库
         user = Db.findById("user", 18).set("name", "huhy");
         Db.update("user", user);

         // 获取user的name属性
         String userName = user.getStr("name");
         // 获取user的age属性
         Integer userAge = user.getInt("age");

         // 查询所有年龄大于18岁的user
         List<Record> users = Db.find("select * from user where age > 18");

         // 分页查询年龄大于18的user,当前页号为1,每页10个user
         Page<Record> userPage = Db.paginate(1, 10, "select *", "from user where age > ?", 18);
         * */

        // 创建name属性为huhy,age属性为18的record对象并添加到数据库
        Record user = new Record().set("name", "yang").set("age", 100);
        Db.save("user", user);
        List<Record> records = Db.find("select * from user");
        //List<User> users = User.dao.find("select * from user");

        renderText("插入成功"+records.size());
    }
}
