package jfinal_db;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;
import global.DemoConfig;

import javax.sql.DataSource;

/**
 * @author : huhy on 2018/9/30.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:jfinal_db
 * @description:{todo}
 */
public class _JFinalDemoGenerator {

    public static DataSource getDataSource() {
        //PropKit.use("config_mysql.txt");
        PropKit.use("config_mysql.properties");
        DruidPlugin druidPlugin = DemoConfig.createDruidPlugin();
        druidPlugin.start();
        return druidPlugin.getDataSource();
    }
    /**
     * @author huhy
     * @ClassName:_JFinalDemoGenerator
     * @date 2018/9/30 12:36 
     * @Description: 指定规则
     *          baseModelPackageName  是  baseModelOutputDir d的后缀
     *       modelPackageName 是 baseModelOutputDir 的上一级  [baseModelOutputDir + "/.."]
     */
    public static void main(String[] args) {
        // base model 所使用的包名
        String baseModelPackageName = "generator.base.test";
        // base model 文件保存路径
        String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/generator/base/test";

        // model 所使用的包名 (MappingKit 默认使用的包名)
        String modelPackageName = "generator.base";
        // model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
        String modelOutputDir = baseModelOutputDir + "/..";

        // 创建生成器
        Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
        // 设置是否生成链式 setter 方法
        generator.setGenerateChainSetter(false);
        // 添加不需要生成的表名
        generator.addExcludedTable("user");
        // 设置是否在 Model 中生成 dao 对象
        generator.setGenerateDaoInModel(false);
        // 设置是否生成链式 setter 方法
        generator.setGenerateChainSetter(true);
        // 设置是否生成字典文件
        generator.setGenerateDataDictionary(false);
        // 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
        generator.setRemovedTableNamePrefixes("t_");
        // 生成
        generator.generate();
    }
}
