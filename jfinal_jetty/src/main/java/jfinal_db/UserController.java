package jfinal_db;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;

import java.util.List;

/**
 * @author : huhy on 2018/9/29.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:jfinal_db
 * @description:做连接数据库的测试
 */
public class UserController extends Controller {
    public static final UserController userController = new UserController();
    //默认加载index方法
    public void index(){
        renderText("huhy");
    }
    /**
     * jfinal连接mysql数据库步骤：
     *      1: 加载驱动：
     *               <!--测试数据库的连接测试  start-->
                     <dependency>
                         <groupId>mysql</groupId>
                         <artifactId>mysql-connector-java</artifactId>
                         <version>5.1.36</version>
                         <scope>runtime</scope>
                     </dependency>
                     <!-- https://mvnrepository.com/artifact/com.mchange/c3p0 -->
                     <dependency>
                         <groupId>com.mchange</groupId>
                         <artifactId>c3p0</artifactId>
                         <version>0.9.5.1</version>
                     </dependency>
                     <!--连接数据库   end-->
            2> 在resource下创建config_mysql.properties的数据库连接配置文件
                        jdbcUrl = jdbc:mysql://localhost/jfinal_jetty?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull
                        user = root
                        password = root
                        devMode = true
                        showSql = true
            3> DemoConfig 下configConstant() 方法下加载配置文件
                        //加载mysql配置文件
                        loadPropertyFile("config_mysql.properties");
            4> DemoConfig 下 configPlugin() 写如连接信息
                        // 加载数据库配置文件,连接mysql数据
                         C3p0Plugin c3p0=new C3p0Plugin(getProperty("jdbcUrl"),
                                getProperty("user"),
                                getProperty("password"));
                            me.add(c3p0);
                        ActiveRecordPlugin activeRecord=new ActiveRecordPlugin(c3p0);
                            activeRecord.addMapping("user", User.class).setShowSql(Boolean.parseBoolean(getProperty("showSql")));
                            me.add(activeRecord);
            5> 启动项目，连接测试即可
                http://localhost:9091/user/getData
                result： [{"password":"123","name":"huhy","id":1,"age":18},{"password":"123","name":"huhy","id":2,"age":19}]
     **/

    public void getData(){
        List<User> users=User.dao.find("select * from user");
        //setAttr("users", users);
        renderJson(users);
    }
    /**
     * @author huhy
     * @Description: 跳转到form.html页面
     */
    public  void renderForm(){
        render("/form.html");
    }
    /**
     * @author huhy
     * @Description: 获取提交上来的表单数据
     */
    public void getForm(){
        User user = getModel(User.class);

        // 如果表单域的名称为 "otherName.title"可加上一个参数来获取
        User user1 = getModel(User.class, "otherName");
        renderJson(user);
    }

    /**
     * @author huhy
     * @Description: 测试db方法
     */
    public void db(){
        /*insert*/
        //new User().set("name", "yang").set("age", 25).save();
        /*删除id=4*/
        User.dao.deleteById(4);
        // 查询id值为25的User将其name属性改为yang11并更新到数据库
        User.dao.findById(25).set("name", "yang11").update();

        // 查询id值为25的user, 且仅仅取name与age两个字段的值
        User user = User.dao.findByIdLoadColumns(25, "name, age");

        // 获取user的name属性
        String userName = user.getStr("name");

        // 获取user的age属性
        Integer userAge = user.getInt("age");

        // 查询所有年龄大于18岁的user
        List<User> users = User.dao.find("select * from user where age>18");

        // 分页查询年龄大于18的user,当前页号为1,每页10个user
        Page<User> userPage = User.dao.paginate(1, 10, "select *", "from user where age > ?", 18);
        getData();
    }


}
