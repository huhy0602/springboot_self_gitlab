package jfinal_config;

import com.jfinal.core.Controller;

/**
 * @author : huhy on 2018/9/28.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:jfinal_1
 * @description:jfianl的测试
 */
public class JfinalController extends Controller {
    /**
     * @author huhy
     * @ClassName:JfinalController
     * @date 2018/9/28 14:58 
     * @Description: index方法
     */
    public void index() {
        renderText("Hello, JFinal!");
    }

    public void yang() {
        renderText("Hello, 胡红阳!");
    }

    public void yang2() {
        renderText("Hello, 胡红阳!——z这是我测试jfinal方法");
    }
    /**
     * @author huhy
     * @ClassName:JfinalController
     * @date 2018/9/28 15:48 
     * @Description: 默认用绝对路径了
     */
    public void render1() {
        render("/jfinal_1.html");
    }
    /**
     * @author huhy
     * @ClassName:JfinalController
     * @date 2018/9/28 15:50 
     * @Description: 这种方式只是相对路径，不管加不加/
     *     寻找这个路径：  \webapp/123/jfinal/render2.html
     */
    public String render2() {
       return "/jfinal_config.html";
    }

    /**
     * @author huhy
     * @Description: 参数接收
     *      第一个参数为String类型的将获取表单或者url中问号挂参的域值。第一个参数为int或无参数的将获取urlPara中的参数值。
     */
    public void yang3() {
        /**
         * http://localhost:9091/jfinal/yang3/name=huhy
         *
         *  getPara()  获取到的是name=huhy
         *  getPara(0);   获取为null
         *
         *
         *  http://localhost:9091/jfinal/yang3?name=huhy
         *
         *  getPara();  获取为null
         *  getPara(0);  获取为null
         *  getPara("name");  获取到huhy
         *
         *  http://localhost:9091/jfinal/yang3/name-huhy
         *  getPara();   获取到 name-huhy
         *  getPara(1);  获取到huhy
         *  getPara("name")  获取到为null
         *
         *  关于参数传递的总结：
         *      url的?传参：只能通过对应的getPara(key)获取
         *      参数的v0-v1-v2:  getPara(下标)
         *
         *   多参数传递  v0-v1-v2   中间的-是默认的。可以修改，后期在介绍
         *
         *   除了上面接收参数方法外还有：
         *          getParaMap();
                    getParaToInt();
                    getPara(name,default);
                    getParaNames();等，自己去看吧
         *
         **/

        String para1 = getPara();
        String name = getPara(0);
        String para = getPara(1);
        String name1 = getPara("name");
        renderText("Hello, 胡红阳!     "+ name1+"   "  + para+ "      "+para1);
    }
}
