package jfinal_config;

import com.jfinal.core.Controller;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;

/**
 * @author : huhy on 2018/9/28.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:jfinal_1
 * @description:prokit读取配置文件信息    配置文件放置resources下，这样编译后默认在classes下
 */
public class ProkitUtils  extends Controller {
    /**
     * @author huhy
     * @ClassName:ProkitUtils
     * @date 2018/9/28 16:17 
     * @Description: 测试prokit读取配置文件
     */
    public void prokit(){
        PropKit.use("yang.txt");
        String userName = PropKit.get("userName");
        String email = PropKit.get("email");

        // Prop 配合用法
        Prop p = PropKit.use("yang.txt");
        Boolean devMode = p.getBoolean("devMode");

        renderText("name   "+ userName + "   email  "+ email  + "devMode  " + devMode);
    }
    /**
     * @author huhy
     * @Description: 读取properties文件
     */
    public void  prokitProperties(){
        PropKit.use("config_mysql.properties");
        String jdbcUrl = PropKit.get("jdbcUrl");
        renderText(jdbcUrl);

    }
}
