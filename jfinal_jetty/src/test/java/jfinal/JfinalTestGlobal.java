package jfinal;

import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;
import com.jfinal.plugin.redis.RedisPlugin;
import org.junit.Test;

/**
 * @author : huhy on 2018/9/30.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:PACKAGE_NAME
 * @description:{todo}
 */
public class JfinalTestGlobal {

    @Test
    public  void  testRedis(){
        System.out.println("jfinal测试");
        RedisPlugin rp = new RedisPlugin("myRedis", "localhost","huhy");
        // 与web下唯一区别是需要这里调用一次start()方法
        rp.start();

        Redis.use().set("key", "value");
        Redis.use().get("key");
    }


}
