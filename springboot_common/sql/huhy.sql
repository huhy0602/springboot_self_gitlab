/*
Navicat MySQL Data Transfer

Source Server         : huhy
Source Server Version : 50540
Source Host           : localhost:3306
Source Database       : huhy

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2018-08-23 16:04:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `price` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'new name', '200');

-- ----------------------------
-- Table structure for sys_attachment
-- ----------------------------
DROP TABLE IF EXISTS `sys_attachment`;
CREATE TABLE `sys_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content_type` varchar(64) DEFAULT NULL,
  `file_path` varchar(100) DEFAULT NULL,
  `file_size` bigint(20) DEFAULT NULL,
  `original_name` varchar(255) DEFAULT NULL,
  `suffix` varchar(20) DEFAULT NULL,
  `att_type` varchar(20) DEFAULT NULL,
  `upload_time` datetime DEFAULT NULL,
  `member_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_o3wk3a2lnl6yl4b8hj9g9wmn5` (`file_path`),
  KEY `FKgl1awyuba6jhcdk8jarfgoup5` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for sys_member
-- ----------------------------
DROP TABLE IF EXISTS `sys_member`;
CREATE TABLE `sys_member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `avatar` varchar(256) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `gender` varchar(16) DEFAULT NULL,
  `hiredate` datetime DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `real_name` varchar(64) NOT NULL,
  `status` bit(1) DEFAULT NULL,
  `telephone` varchar(64) DEFAULT NULL,
  `user_name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5oywfvyf27g9xuksxjv62lcdd` (`user_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_member
-- ----------------------------
INSERT INTO `sys_member` VALUES ('1', '', '768870379@qq.com', 'GIRL', '2017-06-30 00:00:00', '9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0', '管理员', '', '18676037292', 'admin');

-- ----------------------------
-- Table structure for sys_member_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_member_role`;
CREATE TABLE `sys_member_role` (
  `member_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  KEY `FKsi6p61ijwnxiipcgw2uq0slis` (`role_id`),
  KEY `FK3fh7hawfjdp9pcmhffboj1l2w` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_member_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fun_urls` varchar(1024) DEFAULT NULL,
  `menu_url` varchar(128) DEFAULT NULL,
  `res_key` varchar(128) NOT NULL,
  `res_name` varchar(128) NOT NULL,
  `res_type` varchar(20) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_lg3swmwgx5ylh8e2tj9e5k14s` (`res_key`),
  KEY `FK3fekum3ead5klp7y4lckn5ohi` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_resource
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(512) DEFAULT NULL,
  `role_name` varchar(30) NOT NULL,
  `status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_7kwex0745q0eycdwi4d2yihi3` (`role_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_resource`;
CREATE TABLE `sys_role_resource` (
  `role_id` bigint(20) NOT NULL,
  `resource_id` bigint(20) NOT NULL,
  KEY `FKkj7e3cva1e2s3nsd0yghpbsnk` (`resource_id`),
  KEY `FK7urjh5xeujvp29nihwbs5b9kr` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_resource
-- ----------------------------

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '1');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '2');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '3');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '4');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '5');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '6');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '7');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '8');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '9');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '10');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '11');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '12');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '13');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '14');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '15');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '16');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '17');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '18');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '19');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '20');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '21');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '22');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '23');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '24');
INSERT INTO `t_user` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '25');

-- ----------------------------
-- Table structure for t_user_copy
-- ----------------------------
DROP TABLE IF EXISTS `t_user_copy`;
CREATE TABLE `t_user_copy` (
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_copy
-- ----------------------------
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '1');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '2');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '3');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '4');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '5');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '6');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '7');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '8');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '9');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '10');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '11');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '12');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '13');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '14');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '15');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '16');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '17');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '18');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '19');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '20');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '21');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '22');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '23');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '24');
INSERT INTO `t_user_copy` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '25');

-- ----------------------------
-- Table structure for t_user_copy1
-- ----------------------------
DROP TABLE IF EXISTS `t_user_copy1`;
CREATE TABLE `t_user_copy1` (
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_copy1
-- ----------------------------
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '1');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '2');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '3');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '4');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '5');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '6');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '7');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '8');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '9');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '10');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '11');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '12');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '13');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '14');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '15');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '16');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '17');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '18');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '19');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '20');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '21');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '22');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '23');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '24');
INSERT INTO `t_user_copy1` VALUES ('admin', '4f251ab52c7b431254a7adc8ea31724b', 'saltadmin', '25');

-- ----------------------------
-- Procedure structure for init_shiro_demo
-- ----------------------------
DROP PROCEDURE IF EXISTS `init_shiro_demo`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `init_shiro_demo`()
BEGIN	
/*
SQLyog 企业版 - MySQL GUI v7.14 
MySQL - 5.6.16-log : Database - 
*********************************************************************
*/
/*表结构插入*/
DROP TABLE IF EXISTS `u_permission`;
CREATE TABLE `u_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(256) DEFAULT NULL COMMENT 'url地址',
  `name` varchar(64) DEFAULT NULL COMMENT 'url描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*Table structure for table `u_role` */
DROP TABLE IF EXISTS `u_role`;
CREATE TABLE `u_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '角色名称',
  `type` varchar(10) DEFAULT NULL COMMENT '角色类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*Table structure for table `u_role_permission` */
DROP TABLE IF EXISTS `u_role_permission`;
CREATE TABLE `u_role_permission` (
  `rid` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `pid` bigint(20) DEFAULT NULL COMMENT '权限ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*Table structure for table `u_user` */
DROP TABLE IF EXISTS `u_user`;
CREATE TABLE `u_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(20) DEFAULT NULL COMMENT '用户昵称',
  `email` varchar(128) DEFAULT NULL COMMENT '邮箱|登录帐号',
  `pswd` varchar(32) DEFAULT NULL COMMENT '密码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `status` bigint(1) DEFAULT '1' COMMENT '1:有效，0:禁止登录',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*Table structure for table `u_user_role` */
DROP TABLE IF EXISTS `u_user_role`;
CREATE TABLE `u_user_role` (
  `uid` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `rid` bigint(20) DEFAULT NULL COMMENT '角色ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*
SQLyog 企业版 - MySQL GUI v7.14 
MySQL - 5.6.16-log : Database - i_wenyiba_com
*********************************************************************
*/
/*所有的表数据插入*/
/*Data for the table `u_permission` */
insert  into `u_permission`(`id`,`url`,`name`) values (4,'/permission/index.shtml','权限列表'),(6,'/permission/addPermission.shtml','权限添加'),(7,'/permission/deletePermissionById.shtml','权限删除'),(8,'/member/list.shtml','用户列表'),(9,'/member/online.shtml','在线用户'),(10,'/member/changeSessionStatus.shtml','用户Session踢出'),(11,'/member/forbidUserById.shtml','用户激活&禁止'),(12,'/member/deleteUserById.shtml','用户删除'),(13,'/permission/addPermission2Role.shtml','权限分配'),(14,'/role/clearRoleByUserIds.shtml','用户角色分配清空'),(15,'/role/addRole2User.shtml','角色分配保存'),(16,'/role/deleteRoleById.shtml','角色列表删除'),(17,'/role/addRole.shtml','角色列表添加'),(18,'/role/index.shtml','角色列表'),(19,'/permission/allocation.shtml','权限分配'),(20,'/role/allocation.shtml','角色分配');
/*Data for the table `u_role` */
insert  into `u_role`(`id`,`name`,`type`) values (1,'系统管理员','888888'),(3,'权限角色','100003'),(4,'用户中心','100002');
/*Data for the table `u_role_permission` */
insert  into `u_role_permission`(`rid`,`pid`) values (4,8),(4,9),(4,10),(4,11),(4,12),(3,4),(3,6),(3,7),(3,13),(3,14),(3,15),(3,16),(3,17),(3,18),(3,19),(3,20),(1,4),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20);
/*Data for the table `u_user` */
insert  into `u_user`(`id`,`nickname`,`email`,`pswd`,`create_time`,`last_login_time`,`status`) values (1,'管理员','admin','9c3250081c7b1f5c6cbb8096e3e1cd04','2016-06-16 11:15:33','2016-06-16 11:24:10',1),(11,'soso','8446666@qq.com','d57ffbe486910dd5b26d0167d034f9ad','2016-05-26 20:50:54','2016-06-16 11:24:35',1),(12,'8446666','8446666','4afdc875a67a55528c224ce088be2ab8','2016-05-27 22:34:19','2016-06-15 17:03:16',1);
/*Data for the table `u_user_role` */
insert  into `u_user_role`(`uid`,`rid`) values (12,4),(11,3),(11,4),(1,1);
   
    END
;;
DELIMITER ;
