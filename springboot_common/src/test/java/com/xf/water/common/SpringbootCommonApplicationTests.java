package com.xf.water.common;

import com.xf.water.common.dao.oth1.ADao1;
import com.xf.water.common.dao.oth2.ADao2;
import com.xf.water.common.dao.pri.ADao;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootCommonApplicationTests {
	@Autowired
	private ADao aDao;
	@Autowired
	private ADao1 aDao1;
	@Autowired
	private ADao2 aDao2;
	@Test
	public void contextLoads() {
		System.out.println(aDao.findAll());
		System.out.println(aDao1.findAll());
		System.out.println(aDao2.findAll());
	}

	/**
	 * @author huhy
	 * @ClassName:CommonApplicationTests
	 * @date 2018/5/25 13:37 
	 * @Description: 验证shrio的加密
	 */
	@Test
	public  void  testMd5(){
		int hashIterations = 2;    //加密的次数
		Object salt = "saltadmin";   //盐值,此处为了简便设置。更精准的做法是randomNumberGenerator生成随机数
		Object credentials = "123456";   //密码
		String hashAlgorithmName = "MD5";   //加密方式
		Object simpleHash = new SimpleHash(hashAlgorithmName, credentials,
				salt, hashIterations);

		if(simpleHash.equals("4f251ab52c7b431254a7adc8ea31724b")){
			// 4f251ab52c7b431254a7adc8ea31724b
			System.out.println("1111");
		}else{
			System.out.println("22222");
		}
		System.out.println("加密后的值----->" + simpleHash);
	}

}
