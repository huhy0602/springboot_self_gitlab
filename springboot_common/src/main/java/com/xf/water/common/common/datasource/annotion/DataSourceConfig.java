package com.xf.water.common.common.datasource.annotion;

import com.xf.water.common.common.datasource.annotion.dds.DynamicDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;


/**
 * @Author:huhy
 * @DATE:Created on 2018/5/6 14:05
 * @BLOG :  http://www.cnblogs.com/huhongy/
 * @Modified By:
 * @Class Description:数据源配置
 */
@Configuration
@EnableScheduling
@Slf4j
public class DataSourceConfig {

	@Autowired
	private DBProperties properties;
	/**
	 *@Author: huhy
	 *@Package_name:com.huhy.example.config
	 *@Date:14:09 2018/5/6
	 *@Description: 动态加载各个数据源信息 默认用数据源HK那个，可修改
	 * 				注意：把数据源信息 加载到对应的map中
	 */
	@Bean(name = "dataSource")
	public DataSource dataSource() {
		//按照目标数据源名称和目标数据源对象的映射存放在Map中
		Map<Object, Object> targetDataSources = new HashMap<>();
		targetDataSources.put("ds", properties.getDs());
		targetDataSources.put("ds1", properties.getDs1());
		targetDataSources.put("ds2", properties.getDs2());
		//采用是想AbstractRoutingDataSource的对象包装多数据源
		DynamicDataSource dataSource = new DynamicDataSource();
		dataSource.setTargetDataSources(targetDataSources);
		//设置默认的数据源，当拿不到数据源时，使用此配置
		dataSource.setDefaultTargetDataSource(properties.getDs());
		return dataSource;
	}

	@Bean
	public PlatformTransactionManager txManager() {
		return new DataSourceTransactionManager(dataSource());
	}

}
