package com.xf.water.common.utils;

import com.xf.water.common.entity.UserEntity;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.security.MessageDigest;
import java.util.Random;

/**
 * Created by Administrator on 2018/5/25.
 *
 * @Project_name:common
 * @LOCAL:com.xf.water.common.utils
 * @blog : http://www.cnblog,com/huhongy/
 * @description:各种加密算法的工具类
 */
@Configuration
public class SecurityUtil {

    @Value("${shrio.security.type}")
    private static String type;
    @Value("${shrio.security.num}")
    private static int num;
    /**
     * @author huhy
     * @ClassName:SecurityUtil
     * @date 2018/5/25 14:19 
     * @Description: 用于用户注册时生成的密码  采用md5的二次加密进行保密操作
     */
    public static String md5ToPass(UserEntity userEntity){
        //加密的次数
        int hashIterations = num;
        //盐值,此处为了简便设置。更精准的做法是randomNumberGenerator生成随机数
        //Object salt = "saltadmin";
        Object salt = userEntity.getSalt();
        //密码
        //Object credentials = "123456";
        Object credentials = userEntity.getPassword();
        //加密方式
        String hashAlgorithmName = type;
        Object simpleHash = new SimpleHash(hashAlgorithmName, credentials,
                salt, hashIterations);
        return (String)simpleHash;
    }

    /***
     * MD5值
     */
    public static String encryptMD5(String inStr){
        MessageDigest md5 = null;
        try{
            md5 = MessageDigest.getInstance("MD5");
        }catch (Exception e){
            System.out.println(e.toString());
            e.printStackTrace();
            return "";
        }
        char[] charArray = inStr.toCharArray();
        byte[] byteArray = new byte[charArray.length];

        for (int i = 0; i < charArray.length; i++)  {
            byteArray[i] = (byte) charArray[i];
        }
        byte[] md5Bytes = md5.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++){
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16)  {
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();

    }

}
