package com.xf.water.common.common.datasource.annotion;


import com.xf.water.common.common.datasource.annotion.annotation.TargetDataSource;
import com.xf.water.common.common.datasource.annotion.dds.DynamicDataSourceHolder;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;


/**
 * @Author:huhy
 * @DATE:Created on 2018/5/6 14:05
 * @BLOG :  http://www.cnblogs.com/huhongy/
 * @Modified By:
 * @Class Description: 数据源AOP切面定义
 * 	注意：对dao接口进行aop注意
 */
@Component
@Aspect
@Slf4j
public class DataSourceAspect {
	//切换放在mapper接口的方法上，所以这里要配置AOP切面的切入点
	@Pointcut("execution( * com.xf.water.common.dao.*.*(..))")
	public void dataSourcePointCut() {
	}

	@Before("dataSourcePointCut()")
	public void before(JoinPoint joinPoint) {
		Object target = joinPoint.getTarget();
		String method = joinPoint.getSignature().getName();
		Class<?>[] clazz = target.getClass().getInterfaces();
		Class<?>[] parameterTypes = ((MethodSignature) joinPoint.getSignature()).getMethod().getParameterTypes();
		try {
			Method m = clazz[0].getMethod(method, parameterTypes);
			//如果方法上存在切换数据源的注解，则根据注解内容进行数据源切换
			if (m != null && m.isAnnotationPresent(TargetDataSource.class)) {
				TargetDataSource data = m.getAnnotation(TargetDataSource.class);
				String dataSourceName = data.value();
				DynamicDataSourceHolder.putDataSource(dataSourceName);
				log.debug("current thread " + Thread.currentThread().getName() + " add " + dataSourceName + " to ThreadLocal");
			} else {
				log.debug("switch datasource fail,use default");
			}
		} catch (Exception e) {
			log.error("current thread " + Thread.currentThread().getName() + " add data to ThreadLocal error", e);
		}
	}

	//执行完切面后，将线程共享中的数据源名称清空
	@After("dataSourcePointCut()")
	public void after(JoinPoint joinPoint){
		DynamicDataSourceHolder.removeDataSource();
	}
}
