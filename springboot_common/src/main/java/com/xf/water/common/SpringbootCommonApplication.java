package com.xf.water.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
//扫描druid的servlet和filter
@ServletComponentScan
@SpringBootApplication
//@MapperScan("com.xf.water.common.dao")  在多数据源配置，如果每个pac都能扫描，就不用总体扫描
public class SpringbootCommonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootCommonApplication.class, args);
	}
}
