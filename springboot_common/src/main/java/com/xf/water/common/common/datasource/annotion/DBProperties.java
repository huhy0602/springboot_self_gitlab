package com.xf.water.common.common.datasource.annotion;

import com.zaxxer.hikari.HikariDataSource;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author:huhy
 * @DATE:Created on 2018/5/6 14:05
 * @BLOG :  http://www.cnblogs.com/huhongy/
 * @Modified By:
 * @Class Description:实际数据源配置entity
 */
@Component
@Data
@ConfigurationProperties(prefix = "spring.datas")
public class DBProperties {
	private HikariDataSource ds;
	private HikariDataSource ds1;
	private HikariDataSource ds2;
}
