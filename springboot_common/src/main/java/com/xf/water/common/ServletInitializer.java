package com.xf.water.common;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.support.MultipartFilter;

import javax.servlet.ServletContext;

/**
 * Created by Administrator on 2018/5/16.
 *
 * @Project_name:springboot_mongdb
 * @LOCAL:com.xf.water.springboot_mongdb
 * @blog : http://www.cnblog,com/huhongy/
 * @description:放在tomcat运行必须有此类
 */
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        logger.info("SpringApplicationBuilder....");
        return application.sources(SpringbootCommonApplication.class);
    }

    @Override
    protected WebApplicationContext createRootApplicationContext(ServletContext servletContext) {
        logger.info("WebApplicationContext....");
        servletContext.addFilter("multipartFilter", new MultipartFilter());
        return super.createRootApplicationContext(servletContext);
    }
}
