package com.xf.water.common.entity;

import lombok.Data;

/**
 * Created by Administrator on 2018/5/25.
 *
 * @Project_name:common
 * @LOCAL:com.xf.water.common.entity
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@Data
public class UserEntit {
    private String username;
    private String password;
    private String salt;
}
