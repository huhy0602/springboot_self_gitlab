package com.xf.water.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by Administrator on 2018/5/25.
 *
 * @Project_name:common
 * @LOCAL:com.xf.water.common.controller
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@Controller
public class TestViewController {
    @RequestMapping("html")
    public String view() {

        return "/view/login";
    }



}
