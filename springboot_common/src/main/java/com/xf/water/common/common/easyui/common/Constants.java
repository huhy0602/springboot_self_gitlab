package com.xf.water.common.common.easyui.common;


public interface Constants {

    String SESSION_MEMBER_KEY = "s_member";
    String WEB_SOCKET_USERNAME = "socket_member";
    String SESSION_VERIFY_CODE_KEY = "verify_code";
}
