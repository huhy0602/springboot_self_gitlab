package com.xf.water.common.common.easyui.model.dao;


import com.xf.water.common.common.easyui.common.AttachmentType;
import com.xf.water.common.common.easyui.model.domain.Attachment;
import com.xf.water.common.common.easyui.model.domain.Member;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AttachmentDao extends PagingAndSortingRepository<Attachment, Long> {
    Attachment findByFilePath(String filePath);

    List<Attachment> findByFilePathIsNotAndMemberAndType(String filePath, Member member, AttachmentType type);

}
