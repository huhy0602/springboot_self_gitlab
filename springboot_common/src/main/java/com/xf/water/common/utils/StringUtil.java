package com.xf.water.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2018/5/25.
 *
 * @Project_name:common
 * @LOCAL:com.xf.water.common.utils
 * @blog : http://www.cnblog,com/huhongy/
 * @description:对String的处理工具类
 */
public class StringUtil {
    /**
     * @author huhy
     * @ClassName:SecurityUtil
     * @date 2018/5/25 14:38
     * @Description: 生成六位随机码，作为注册时的随机salt值
     */
    public static String getCode(){
        char[] str={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9'};
        Random random=new Random();
        StringBuffer result=new StringBuffer();
        for (int i=0;i<6;i++){
            int a=random.nextInt(36);
            result=result.append(str[a]);
        }
        return  result.toString();
    }

    /**
     * 取域名
     *
     * @param url 要取域名的url
     */
    public static String getDomainName(String url) {
        String domainName = "";
        String pattern = "[^//]*?\\.(com|cn|net|org|biz|info|cc|tv)";
        pattern = "(?<=http://|\\.)[^.]*?\\.(com|cn|net|org|biz|info|cc|tv)";
        Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher matcher = p.matcher(url);
        boolean match = matcher.find();
        if (match) {
            domainName = matcher.group();
        }
        return domainName;
    }

    /**
     * 根据|分隔的字符串拼成一个列表
     *
     * @param str |分隔的字符串
     */
    public static List<String> getListByStr(String str) {
        List<String> list = new ArrayList<String>();
        StringBuffer sb = new StringBuffer();
        String tempStr = null;
        char[] strChars = str.toCharArray();
        for (int i = 0; i < strChars.length; i++) {
            if (strChars[i] != '|' && i != strChars.length - 1) {
                sb.append(strChars[i]);
            } else {
                tempStr = sb.toString();
                String[] segArray = tempStr.split(":");
                if (segArray.length > 1) {
                    list.add(segArray[1]);
                }
                sb = new StringBuffer();
            }
        }
        return list;
    }

    /**
     * 根据指定分隔符的字符串拼成一个列表
     *
     * @param str       分隔的字符串
     * @param seperator 分隔符
     */
    public static List<String> getListByStr(String str, char seperator) {
        List<String> list = new ArrayList<String>();
        StringBuffer sb = new StringBuffer();
        String tempStr = null;
        char[] strChars = str.toCharArray();
        for (int i = 0; i < strChars.length; i++) {
            if (strChars[i] != seperator && i != strChars.length - 1) {
                sb.append(strChars[i]);
            } else {
                if (i == strChars.length - 1 && strChars[strChars.length - 1] != seperator) {
                    sb.append(strChars[i]);
                }
                tempStr = sb.toString();
                list.add(tempStr);
                sb = new StringBuffer();
            }
        }
        return list;
    }

    /**
     * 根据|分隔的字符串拼成一个列表
     *
     * @param str |分隔的字符串
     */
    public static List<String> getListByStr2(String str) {
        List<String> list = new ArrayList<String>();
        StringBuffer sb = new StringBuffer();
        String tempStr = null;
        char[] strChars = str.toCharArray();
        for (int i = 0; i < strChars.length; i++) {
            if (strChars[i] != '|' && i != strChars.length - 1) {
                sb.append(strChars[i]);
            } else {
                tempStr = sb.toString();
                String[] segArray = tempStr.split(":");
                if (segArray.length > 0) {
                    list.add(segArray[0]);
                }
                sb = new StringBuffer();
            }
        }
        return list;
    }

    /**
     * 根据|分隔的字符串拼成一个列表
     *
     * @param str |分隔的字符串
     */
    public static List<String> getListByStrForBook(String str) {
        List<String> list = new ArrayList<String>();
        StringBuffer sb = new StringBuffer();
        String tempStr = null;
        char[] strChars = str.toCharArray();
        for (int i = 0; i < strChars.length; i++) {
            if (strChars[i] != '|' && i != strChars.length - 1) {
                sb.append(strChars[i]);
            } else {
                tempStr = sb.toString();
                String[] segArray = tempStr.split(":");
                if (segArray.length > 0) {
                    list.add(tempStr);
                }
                sb = new StringBuffer();
            }
        }
        return list;
    }

    /**
     * 根据|分隔的字符串拼成一个列表
     *
     * @param str |分隔的字符串
     */
    public static List<String> getListByStr3(String str) {
        List<String> list = new ArrayList<String>();
        StringBuffer sb = new StringBuffer();
        String tempStr = null;
        char[] strChars = str.toCharArray();
        for (int i = 0; i < strChars.length; i++) {
            if (strChars[i] != ',' && i != strChars.length - 1) {
                sb.append(strChars[i]);
            } else {
                tempStr = sb.toString();
                String[] segArray = tempStr.split(":");
                if (segArray.length > 0) {
                    list.add(segArray[0]);
                }
                sb = new StringBuffer();
            }
        }
        return list;
    }

    /**
     * 判断一个字符串是否是null或者空格或者空
     *
     * @param str 字符串
     * @return
     */
    public static boolean isEmpty(String str) {
        if (str != null && str.trim().length() > 0) {
            return false;
        }
        return true;
    }

    /**
     * 如果一个字符串为null，则转化为“”；否则，返回本身
     *
     * @param str 字符串
     * @return
     */
    public static String nullToStr(String str) {
        return str == null ? "" : str;
    }

    /**
     * 如果一个字符串为null，则转化为“”；否则，返回本身
     *
     * @param str 字符串
     * @return
     */
    public static String nullToStr(Object str) {
        return str == null ? "" : String.valueOf(str);
    }

    /**
     * <p>
     * 字符的前后几位保持不变，其它字符用代替字符替换（先替换前几位字符，后替换后几位字符）
     * </p>
     *
     * @param str       原字符
     * @param prefix    前几位(0,表示不做替换）
     * @param postfix   后几位(0,表示不做替换）
     * @param character 替换字符(若替换字符为一位，则保持长度不变)
     * @return 替换后的字符
     */
    public static String stringSwitch(String str, int prefix, int postfix, String character) {
        if (prefix < 0 || postfix < 0) {
            return str;
        }
        if (prefix == 0 && postfix == 0) {
            return str;
        }
        if (str != null && str.trim().length() > 0) {
            StringBuffer buf = new StringBuffer();
            int argsLength = str.length();
            // 保证被替换的长度大于原字符长度
            if (argsLength > prefix + postfix) {
                if (prefix != 0) {
                    String stringPrefix = str.substring(0, prefix);
                    buf.append(stringPrefix);
                }
                for (int i = prefix; i < argsLength - postfix; i++) {
                    buf.append(character);
                }
                if (postfix != 0) {
                    String stringPostfix = str.substring(argsLength - postfix);
                    buf.append(stringPostfix);
                }
                return buf.toString();
            } else {
                return str;
            }
        }
        return null;
    }

    /**
     * 生成一个定长的纯0字符串
     *
     * @param length 字符串长度
     * @return 纯0字符串
     */
    public static String generateZeroStr(int length) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            sb.append('0');
        }
        return sb.toString();

    }
}