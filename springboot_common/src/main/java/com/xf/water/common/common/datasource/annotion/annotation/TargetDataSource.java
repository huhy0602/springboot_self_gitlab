package com.xf.water.common.common.datasource.annotion.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author:huhy
 * @DATE:Created on 2018/5/6 14:05
 * @BLOG :  http://www.cnblogs.com/huhongy/
 * @Modified By:
 * @Class Description: 目标数据源注解，注解在方法上指定数据源的名称
 */
@Retention(RetentionPolicy.RUNTIME)
// 在方法上生效
@Target(ElementType.METHOD)
public @interface TargetDataSource {
	//此处接收的是数据源的名称
	String value();
	/**
	 * 多数据源‘
	 *
	 * String[] value();
	 */
}
