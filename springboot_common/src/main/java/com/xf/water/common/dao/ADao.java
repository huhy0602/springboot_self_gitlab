package com.xf.water.common.dao;

import com.xf.water.common.common.datasource.annotion.annotation.TargetDataSource;
import com.xf.water.common.entity.AEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2018/5/25.
 *
 * @Project_name:common
 * @LOCAL:com.xf.water.common.dao
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@Repository
public interface ADao {
    public List<AEntity> findAll();
}
