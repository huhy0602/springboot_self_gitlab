package com.xf.water.common.common.easyui.model.mapper;

import com.xf.water.common.common.easyui.model.domain.Member;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface MemberMapper {

	public List<Member> findAll();
}
