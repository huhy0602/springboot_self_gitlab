# 多数据源实现
1.引入dynamic-datasource-spring-boot-starter。
<dependency>
  <groupId>com.baomidou</groupId>
  <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
  <version>${version}</version>
</dependency>
2. 配置数据源信息
        spring:
          datasource:
            dynamic:
              primary: master #设置默认的数据源或者数据源组,默认值即为master
              datasource:
                master:
                  username: huhy
                  password: Huhy123.
                  driver-class-name: com.mysql.cj.jdbc.Driver
                  url: jdbc:mysql://192.168.30.100:3366/huhy?useSSL=false&serverTimezone=UTC
                slave:
                  username: huhy
                  password: Huhy123.
                  driver-class-name: com.mysql.cj.jdbc.Driver
                  url: jdbc:mysql://localhost:3308/huhy?useSSL=false&serverTimezone=UTC
        mybatis-plus:
          type-aliases-package: com.huhy.project.plus1
          mapper-locations: classpath:mapper/*.xml
3 添加注解
@DS 可以注解在方法上和类上，同时存在方法注解优先于类上注解。

