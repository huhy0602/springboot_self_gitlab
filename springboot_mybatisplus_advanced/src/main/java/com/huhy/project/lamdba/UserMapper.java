package com.huhy.project.lamdba;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huhy.project.plus1.User;

/**
 * @author : huhy on 2018/12/25.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.project.lamdba
 * @description:{todo}
 */
public interface UserMapper extends BaseMapper<User> {
    @DS("slave")
    User  selectByName2(String name);
}
