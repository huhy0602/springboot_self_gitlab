package com.huhy.project.plus1;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author : huhy on 2018/12/12.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.project.plus1
 * @description:{todo}
 */
public interface UserDao extends BaseMapper<User> {

    @DS("master")
    User  selectByName(String name);

    @DS("slave")
    User  selectByName2(String name);
}
