package com.huhy;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.huhy.project.**.*")
@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
public class SpringbootMybatisplusAdvancedApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMybatisplusAdvancedApplication.class, args);
    }
}
