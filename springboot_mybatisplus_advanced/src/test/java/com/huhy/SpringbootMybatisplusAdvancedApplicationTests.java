package com.huhy;

import com.huhy.project.lamdba.UserMapper;
import com.huhy.project.plus1.UserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootMybatisplusAdvancedApplicationTests {

    @Autowired
    private UserDao userDao;



    @Test
    public void contextLoads() {
        //System.out.println(userDao.selectByName("huhy"));
        System.out.println(userDao.selectByName2("slave"));
    }

    @Test
    public void contextMapper() {
        System.out.println(userDao.selectByName2("slave"));
        new Thread(() -> System.out.println("huhy")).start();


    }

}
