# mybatisplus插件的使用

  # 注意：mybatisplus有两种配置方式
  
   第一种配置方式
   
    		<dependency>
    			    <groupId>com.baomidou</groupId>
    			    <artifactId>mybatis-plus</artifactId>
    			    <version>2.1.7</version>
    		</dependency>
    
    		<dependency>
    			    <groupId>com.baomidou</groupId>
    			    <artifactId>mybatisplus-spring-boot-starter</artifactId>
    			    <version>1.0.5</version>
    		</dependency>
   第二种配置方式
   
    		<dependency>
    			    <groupId>com.baomidou</groupId>
    			    <artifactId>mybatis-plus-boot-starter</artifactId>
    			    <version>3.0.5</version>
    		</dependency>
  #使用方式介绍