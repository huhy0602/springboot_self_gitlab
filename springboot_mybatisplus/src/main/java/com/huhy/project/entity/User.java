package com.huhy.project.entity;

import lombok.Data;

/**
 * @author : huhy on 2018/11/1.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.project.entity
 * @description:{todo}
 */
@Data
public class User {
    private Long id;
    private String name;
    private Integer age;
    private String email;
}
