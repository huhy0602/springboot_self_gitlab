package com.huhy.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huhy.project.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author : huhy on 2018/11/2.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.project.dao
 * @description:{todo}
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    /**
     * 存储过程测试  只测试输出
     */
    void callHuhyCountPro(Map<String, Object> map);

    /**
     * 存储过程测试  输入输出
     */
    void callHuhyUserPro(Map<String, Object> map);

    /**
     * 表名作为存储参数传入存储过程
     * @param map
     * @return
     */
    List<User> selectByTName(Map<String, Object> map);
}
