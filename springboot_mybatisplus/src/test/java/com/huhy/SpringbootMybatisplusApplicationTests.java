package com.huhy;

import com.huhy.project.dao.UserMapper;
import com.huhy.project.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootMybatisplusApplicationTests {

	@Autowired
	private UserMapper userMapper;
	@Test
	public void contextLoads() {
        System.out.println("");
	}

	@Test
	public void testPro(){
		Map<String,Object> map = new HashMap<>();
		userMapper.callHuhyCountPro(map);
		System.out.println(map.get("sum"));
	}

	@Test
	public void testUser(){
		Map<String,Object> map = new HashMap<>();
		map.put("a",12);
		map.put("b",20);
		userMapper.callHuhyUserPro(map);
		System.out.println(map.get("sum"));
	}

	@Test
	public void testUser_1(){
		Map<String,Object> map = new HashMap<>();
		map.put("pageNo",2);
		map.put("pageSize",2);
		map.put("tableName","user");
        List<User> users = userMapper.selectByTName(map);
        for (User user : users) {
            System.out.println(user);
        }
        System.out.println(users.size());
	}

	@Test
	public void test(){
		System.out.println(userMapper.selectList(null));
		Map<String,Object> map = new HashMap<>();
		map.put("id",1);
		List<User> users = userMapper.selectByMap(map);
		for (User user : users) {
			System.out.println(user);

		}
	}

}
