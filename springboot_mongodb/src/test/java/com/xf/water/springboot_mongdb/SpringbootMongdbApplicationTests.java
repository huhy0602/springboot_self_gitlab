package com.xf.water.springboot_mongdb;

import com.mongodb.*;
import com.xf.water.springboot_mongdb.dao.Sl6512014Dao;
import com.xf.water.springboot_mongdb.entity.MaxValue;
import com.xf.water.springboot_mongdb.entity.Sl6512014;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootMongdbApplicationTests {
	@Autowired
	private  Sl6512014Dao sl6512014Dao;

	@Autowired
	private MongoTemplate mongoTemplate;
    /*@Value("${data.pageSize}")
	private int pageSize;*/
	@Test
	public void contextLoads() {
		System.out.println(mongoTemplate.getCollectionNames().size());
	}
	/**
	 * @author huhy
	 * @ClassName:SpringbootMongdbApplicationTests
	 * @date 2018/5/15 10:30
	 * @Description: 测试mongodb
	 */
	@Test
	public void testMongodb(){
        Set<String> collectionNames = mongoTemplate.getCollectionNames();
        for (String collectionName : collectionNames) {
            System.out.println(collectionName+"-------------->");
            mongoTemplate.dropCollection(collectionName);
        }
    }



	/**
	 * @author huhy
	 * @ClassName:SpringbootMongdbApplicationTests
	 * @date 2018/5/14 16:56
	 * @Description: 查询有多少条记录
	 */
	/*@Test
	public void test(){
        boolean flag = false;
        //保存sqlsercer的表数据量
        MaxValue maxValue = getMaxSize();
        //保存在mongodb的最大值或者异常值
        long maxSize = maxValue.getMaxSize();
        Long startItem = maxValue.getStartItem();
        long tempMaxSize = maxSize;
        //maxSize = updateCollections(maxSize);
        //循环标示符
        int i = 0;
        while (!flag){
            //sqlserver
            List<Sl6512014> sl6512014s = sl6512014Dao.selectCount(maxSize,pageSize);

            insertMongdb(sl6512014s);
            //获取到最后一条的那个serial的值
            Long serial = sl6512014s.get(sl6512014s.size() - 1).getSerial();
            //判断是不是最后一次遍历，若是最后一次，集合<=pageSize
            if(sl6512014s.size() < pageSize){
                    flag = true;
                    //数据总量
                    maxSize = maxSize  + sl6512014s.size();
                //执行结束记录最大值
                updateCollections(maxSize,serial);
                    //redis
                try {
                    insertRedis(serial);
                } catch (Exception e) {
                    continue;
                }

            }
            i++;

            //没执行一次就把成功代码放到mongdb中
            if(!flag){
                if(tempMaxSize == 0){
                    maxSize = i * pageSize;
                }else{
                    maxSize = tempMaxSize +  i * pageSize;
                }
                updateCollections(maxSize,serial);
                //先往redis中插入
                try {
                    insertRedis(serial);
                } catch (Exception e) {
                    continue;
                }
            }

            sl6512014s = null;
        }
	}*/
    /**
     * @author huhy
     * @ClassName:SpringbootMongdbApplicationTests
     * @date 2018/6/6 15:38 
     * @Description: 记录成功条数
     */
    public  void updateCollections(long maxSize,long serial){
        //long maxSize = 0L;
        boolean flag = mongoTemplate.collectionExists("maxValue");
        //判断集合是否存在
        if(flag){
            Query query=new Query(Criteria.where("author").is("mongodb"));
            Update update= new Update().set("maxSize", maxSize).set("startItem",serial);
            //更新查询返回结果集的第一条
            mongoTemplate.updateFirst(query,update,"maxValue");
        }else{
            mongoTemplate.insert(new MaxValue("mongodb",maxSize,0L),"maxValue");
        }

        //return maxSize;
    }

    /**
     * @author huhy
     * @ClassName:SpringbootMongdbApplicationTests
     * @date 2018/5/16 14:11
     * @Description: ${todo}
     */
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void testRedis(){


        //redisTemplate.opsForValue().set("maxSize1","huhy");
        System.out.println(redisTemplate.opsForValue().get("maxSize"));
    }

    /**
     * 把标示插入redis中
     */
    public void insertRedis(long maxSize){
        ValueOperations operations = redisTemplate.opsForValue();
        operations.set("maxSize",maxSize);
    }
    /**
     * 判断最终的 maxSize值
     *      redis和mongodb中是否一样
     */
    public MaxValue getMaxSize() {
        MaxValue result = new MaxValue();
        try {
            //先判断mongodb是否 存在集合
            //如果不存在集合说明是第一次请求
            boolean maxValue = mongoTemplate.collectionExists("maxValue");
            if (!maxValue) {
                //不存在
                result.setMaxSize(0L);
                result.setStartItem(0L);
                return result;
            } else {
                //存在
                //从mongodb中取出
                long startItem = (long) mongoTemplate.getCollection("maxValue").find().next().get("startItem");
                long maxSize = (long) mongoTemplate.getCollection("maxValue").find().next().get("maxSize");
                //从redis中获取对应值进行比较
                long redisSize = (long) redisTemplate.opsForValue().get("maxSize");
                if (startItem != redisSize) {
                    //把redis中的值付给mongodb,让sqlserver 从mongodbSize起始位置开始查询
                    startItem = redisSize;
                }
                result.setMaxSize(maxSize);
                result.setStartItem(startItem);
                return result;
            }
        } catch (Exception e) {
            //mongdb中库不存在  会报空指针    捕获异常   给它初始值0
            result.setMaxSize(0L);
            result.setStartItem(0L);
            return result;
        }
    }

    @Test
    public  void testMo(){
        Query query=new Query(Criteria.where("author").is("mongodb"));
        Update update= new Update().set("maxSize", 1000L);
        //更新查询返回结果集的第一条
        WriteResult result =mongoTemplate.updateFirst(query,update,MaxValue.class);
        System.out.println(result.toString());
    }
    /**
     * @author huhy
     * @ClassName:SpringbootMongdbApplicationTests
     * @date 2018/5/23 13:53
     * @Description: 测试批量导入
     */

    public void insertMongdb(List<Sl6512014> sl6512014s){

        Map<String,List<Sl6512014>> map = new HashMap<String,List<Sl6512014>>();
       // List<Sl6512014> sl6512014s = sl6512014Dao.selectCount(0, 10);
        for (Sl6512014 sl6512014 : sl6512014s) {
            List<Sl6512014> sl6512014s1 = new ArrayList<>();
            String st = sl6512014.getSt();
            if(st == "" || st == null ){
                st = "error";
            }
            //判断key是否存在
            if(map.containsKey(st)){
                sl6512014s1 = map.get(st);
            }
            sl6512014s1.add(sl6512014);
            map.put(st,sl6512014s1);
        }
        Set<String> strings = map.keySet();
        for (String string : strings) {
            //临时  防止异常
            Map<String,List<Sl6512014>> temp = new HashMap<String,List<Sl6512014>>();
            temp.put(string,map.get(string));
            //批量插入
            try {
                mongoTemplate.insert(map.get(string),string);
            } catch (Exception e) {
                insertError(temp);
            }
        }
    }


    @Test
    public void testMongo() throws Exception {
        MongoClientURI mongoClientURI = new MongoClientURI("mongodb://monitoring:moni_20180522@47.97.26.236:27010/monitoring");
        MongoClient mongoClient = new MongoClient(mongoClientURI);
        System.out.println(mongoClient);

    }

    /*@Test
    public void testRemove(){
        Map<String,List<Sl6512014>> map = new HashMap<String,List<Sl6512014>>();
         List<Sl6512014> sl6512014s = sl6512014Dao.selectCount(0, 10);
        for (Sl6512014 sl6512014 : sl6512014s) {
            List<Sl6512014> sl6512014s1 = new ArrayList<>();
            //判断key是否存在
            if(map.containsKey(sl6512014.getSt())){
                sl6512014s1 = map.get(sl6512014.getSt());
            }
            sl6512014s1.add(sl6512014);
            map.put(sl6512014.getSt(),sl6512014s1);
        }
        Set<String> strings = map.keySet();
        for (String string : strings) {
            //临时  防止异常
            Map<String,List<Sl6512014>> temp = new HashMap<String,List<Sl6512014>>();
            temp.put(string,map.get(string));
            //批量插入
            try {
                mongoTemplate.insert(map.get(string),string);
            } catch (Exception e) {
                //异常删除增加成功的方式
                insertError(temp);
            }
        }
    }*/
    /**
     * @author huhy
     * @ClassName:SpringbootMongdbApplicationTests
     * @date 2018/5/23 15:26
     * @Description: mongodb批量插入异常回滚机制代码实现
     *
     * */
    public void insertError(Map<String,List<Sl6512014>> temp){
        //获取异常时的map集合
        Set<String> str = temp.keySet();
        //获取所有的key
        for (String s : str) {
            //拿到所有的value
            List<Sl6512014> sl6512014s = temp.get(s);
            //遍历删除
            for (Sl6512014 sl6512014 : sl6512014s) {
                Query query=new Query(Criteria.where("serial").is(sl6512014.getSerial()));
                mongoTemplate.remove(query,s);
            }
        }
    }


    /**
     * 测试修改后版本
     */
    @Test
    public void testNew(){
        boolean flag = false;
        //保存sqlsercer的表数据量
        //保存在mongodb的最大值或者异常值
        MaxValue maxValue = getMaxSize();
        //保存在mongodb的最大值或者异常值
        long maxSize = maxValue.getMaxSize();
        Long startItem = maxValue.getStartItem();
        while (!flag){
            //获取到between的数据
            List<Sl6512014> sl6512014s = sl6512014Dao.selectCountNew(startItem+1);

            //存入mongodb中
            insertMongdb(sl6512014s);

            if(sl6512014s.size() == 0){
                //当数量为0时，跳出循环
                flag = true;
                break;
            }
            //获取到最后一条的那个serial的值
            startItem = sl6512014s.get(sl6512014s.size() - 1).getSerial();
            System.out.println(startItem+"-------------startItem");
            //把下次的startItem  中存入mongodb中
            maxSize = maxSize + sl6512014s.size();
            System.out.println(maxSize+"-----------maxSize");
            //把值插入到mongodb中
            updateCollections(maxSize,startItem);
            try {
                insertRedis(startItem);
            } catch (Exception e) {
                continue;
            }
        }
    }
}
