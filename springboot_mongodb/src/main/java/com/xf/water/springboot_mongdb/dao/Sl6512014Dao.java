package com.xf.water.springboot_mongdb.dao;

import com.xf.water.springboot_mongdb.config.annotation.TargetDataSource;
import com.xf.water.springboot_mongdb.entity.Sl6512014;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by Administrator on 2018/5/4.
 */
@Repository
public interface Sl6512014Dao {
    /**
     * @author huhy
     * @ClassName:Sl6512014Dao
     * @date 2018/5/14 16:38 
     * @Description: ${查询出记录数}
     */
    //@TargetDataSource("sqlservertest")
    /*@TargetDataSource("sqlserver")*/
    //List<Sl6512014> selectCount(@Param("pageNum") long pageNum, @Param("size") long size);

    /**/
    @TargetDataSource("sqlservertest")
    List<Sl6512014> selectCountNew(@Param("startItem") long startItem);

    @TargetDataSource("sqlservertest")
    long serialMax();
}
