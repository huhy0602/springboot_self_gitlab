package com.xf.water.springboot_mongdb.config;

import com.zaxxer.hikari.HikariDataSource;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author:huhy
 * @DATE:Created on 2018/5/6 14:05
 * @BLOG :  http://www.cnblogs.com/huhongy/
 * @Modified By:
 * @Class Description:实际数据源配置entity
 */
@Component
@Data
@ConfigurationProperties(prefix = "spring.data")
public class DBProperties {
	private HikariDataSource sqlserver;
	private HikariDataSource sqlservertest;
}
