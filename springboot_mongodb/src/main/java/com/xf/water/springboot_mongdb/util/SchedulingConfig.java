package com.xf.water.springboot_mongdb.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by Administrator on 2018/5/16.
 *
 * @Project_name:springboot_mongdb
 * @LOCAL:com.xf.water.springboot_mongdb.util
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@Configuration
@EnableScheduling // 启用定时任务
public class SchedulingConfig {

    @Autowired
    private  SqlServerTransMongodbUtil sqlServerTransMongodbUtil;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Scheduled(cron = "0 0/3 * * * ?") // 每20秒执行一次
    public void scheduler() {
        logger.info("抽取日志提醒：正在抽取中-------10分钟打印一次日志信息------ ");
        //sqlServerTransMongodbUtil.transformData();
    }

}
