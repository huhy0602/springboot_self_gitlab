package com.xf.water.springboot_mongdb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.xf.water.springboot_mongdb.dao")
public class SpringbootMongdbApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootMongdbApplication.class, args);
	}
}
