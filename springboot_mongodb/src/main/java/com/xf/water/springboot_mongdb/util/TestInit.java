package com.xf.water.springboot_mongdb.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by Administrator on 2018/5/17.
 *
 * @Project_name:springboot_mongdb
 * @LOCAL:com.xf.water.springboot_mongdb.util
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@Component
public class TestInit implements CommandLineRunner {
    @Autowired
    private  SqlServerTransMongodbUtil sqlServerTransMongodbUtil;
    @Override
    public void run(String... strings) throws Exception {
        //项目启动即可执行
        System.out.println("数据抽取中。。。");
        //sqlServerTransMongodbUtil.transformData();
        System.out.println("数据抽取完成！！！");
    }

}
