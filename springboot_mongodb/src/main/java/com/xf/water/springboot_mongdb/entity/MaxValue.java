package com.xf.water.springboot_mongdb.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Set;

/**
 * Created by Administrator on 2018/5/16.
 *
 * @Project_name:springboot_mongdb
 * @LOCAL:com.xf.water.springboot_mongdb.entity
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MaxValue {
    //这是标示字段，用于更新操作中查询
    private String author;
    //保存在mongodb中的一个记录字段，从sqlserver --->  mongodb的成功记录数字
    private Long maxSize;
    //保存起始字段
    private Long startItem;

    //做系统异常时的缓存
    private Set<String> collectionNames;

    public MaxValue(String author, Long maxSize, Long startItem) {
        this.author = author;
        this.maxSize = maxSize;
        this.startItem = startItem;
    }

    public MaxValue(String author, Set<String> collectionNames) {
        this.author = author;
        this.collectionNames = collectionNames;
    }
}
