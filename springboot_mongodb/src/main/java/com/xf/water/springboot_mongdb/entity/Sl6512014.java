package com.xf.water.springboot_mongdb.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Administrator on 2018/4/12.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sl6512014  implements Serializable {

	private static final long serialVersionUID = -7953716419525899867L;
	
	private Long serial;
    private Date ut;
    private String st;
    private Date tt;
    private BigDecimal ai;
    private BigDecimal c;
    private BigDecimal mst;
    private BigDecimal pd;
    private BigDecimal pj;
    private BigDecimal pt;
    private BigDecimal pn01;
    private BigDecimal pn05;
    private BigDecimal pn10;
    private BigDecimal pn30;
    private BigDecimal q;
    private BigDecimal q1;
    private BigDecimal q2;
    private BigDecimal va;
    private BigDecimal vj;
    private BigDecimal vt;
    private BigDecimal z;
    private BigDecimal zb;
    private Integer zt;
    private BigDecimal sbl1;
    private BigDecimal sbl2;
    private BigDecimal ph;
    private BigDecimal turb;
    private BigDecimal m10;
    private BigDecimal m20;
    private BigDecimal m30;
    private BigDecimal m40;
    private BigDecimal m50;
    private BigDecimal m60;
    private BigDecimal m80;
    private BigDecimal m100;
    private Date stt;
    private BigDecimal radio;
    private BigDecimal fl;
    private BigDecimal uc;
    private BigDecimal us;
    private BigDecimal DO;
    private BigDecimal pm25;
    private BigDecimal cond;
    private BigDecimal ss;
    private BigDecimal codmn;
    private BigDecimal gtp;
    private BigDecimal ue;
    private BigDecimal redox;
    private BigDecimal nh4n;
    private BigDecimal chla;
    private BigDecimal hno3;
    private BigDecimal algae;
    private BigDecimal t10;
    private BigDecimal signal;
    private BigDecimal LONG;
    private BigDecimal lati;
    private BigDecimal alti;
    private BigDecimal wp1;
    private BigDecimal wp2;
    private String utTime;
    private String ttTime;
    private String sttTime;
    private Date startDateTime;
    private Date endDateTime;
    private String siteName;



}
