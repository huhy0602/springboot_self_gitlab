package com.huhy.webservice.client;

import com.huhy.webservice.service.UserService;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

import java.util.Arrays;
import java.util.List;
/**
 * @author : huhy on 2018/6/20.
 * @Project_name:springboot_self
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{客户端测试}
 */
public class CxfClient {
	public static void main(String[] args) {

        testCli1();
        //testCli2();
        //testCli3();
        //testCli1();
	}
	/**
	 * @author huhy
	 * @ClassName:CxfClient
	 * @date 2018/6/20 17:30 
	 * @Description: 测试1   不连接数据库
	 */
	public static void testCli1() {
		// 创建动态客户端
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		Client client = dcf.createClient("http://localhost:8083/services/AService?wsdl");
		Object[] objects = new Object[0];
		try {
			objects = client.invoke("getStr", "huhy");
			System.out.println("echo:" + objects[0]);
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 方式1.代理类工厂的方式,需要拿到对方的接口  -------适用与接口发布的服务
     *   接口代理类的发布
	 */
	public static void testCli2() {
		try {
			// 接口地址
			String address = "http://localhost:8083/services/BService?wsdl";
            // 代理工厂
			JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
			// 设置代理地址
			jaxWsProxyFactoryBean.setAddress(address);
			// 设置接口类型
			jaxWsProxyFactoryBean.setServiceClass(UserService.class);
			// 创建一个代理接口实现
			UserService cs = (UserService) jaxWsProxyFactoryBean.create();
			// 数据准备
			String id = "1";
			// 调用代理接口的方法调用并返回结果
			Object result = cs.queryByid(id);
			System.out.println("返回结果:" + result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 动态调用方式
     *  连接数据库
	 */
	public static void testCli3() {
		// 创建动态客户端
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		Client client = dcf.createClient("http://localhost:8083/services/UserService?wsdl");
		Object[] objects = new Object[]{};
		try {
			objects = client.invoke("queryByid", "1");
            List<Object> objects1 = Arrays.asList(objects);
            System.out.println(objects1.get(0));
        } catch (java.lang.Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author huhy
	 * @ClassName:CxfClient
	 * @date 2018/6/21 10:57 
	 * @Description: ${todo}
	 */
	public static void testCli4() {
		// 创建动态客户端
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		Client client = dcf.createClient("http://localhost:8083/services/my?wsdl");
		Object[] objects = new Object[0];
		try {
			objects = client.invoke("sayHello", "huhy");
			System.out.println("echo:" + objects[0]);
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}
	}
}
