package com.huhy.webservice.pub;

import com.huhy.webservice.service.AService;
import com.huhy.webservice.service.MyWebService;
import com.huhy.webservice.service.UserService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.List;
/**
 * @author : huhy on 2018/6/20.
 * @Project_name:springboot_self
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{客户端测试}
 */
@Configuration
public class CxfConfig {
	@Autowired
	private Bus bus;

	@Autowired
	UserService userService;

	@Autowired
	private AService aService;

	@Autowired
	private MyWebService myWeb;


	/** JAX-WS **/
	@Bean
	public Endpoint endpoint() {

		EndpointImpl endpoint = new EndpointImpl(bus, userService);
		endpoint.publish("/UserService");
		return endpoint;
	}
	@Bean
	public Endpoint endpoint2() {
		EndpointImpl endpoint = new EndpointImpl(bus, aService);
		endpoint.publish("/abc");
		return endpoint;
	}

	@Bean
	public Endpoint endpoint3() {
		EndpointImpl endpoint = new EndpointImpl(bus, myWeb);
		endpoint.publish("/my");
		return endpoint;
	}
}
