package com.huhy.webservice.service;

import org.springframework.stereotype.Component;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author : huhy on 2018/6/20.
 * @Project_name:springboot_self
 * @LOCAL:com.huhy.webservice.service
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@Component
@WebService(serviceName = "abc",targetNamespace="http://service.webservice.huhy.com/")
public class AService {
    public String getStr(@WebParam(name="name") String name){
        return "你好 "+ name;
    }
}
