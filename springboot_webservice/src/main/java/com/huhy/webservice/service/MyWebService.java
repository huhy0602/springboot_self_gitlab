package com.huhy.webservice.service;

import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * @author : huhy on 2018/6/21.
 * @Project_name:springboot_self
 * @LOCAL:com.huhy.webservice.service
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@WebService // 默认静态的方式是不能发布ws服务的
        (
                name="huhy",  // 默认服务实现类的名称
                serviceName="MyWeb",  // 默认在发布的服务实现者的名称后面添加Service
                portName="huhyPort",   // 服务类型的名称: 默认在 发布的服务实现者(MyWebService) 后面添加 port
                targetNamespace="service.webservice.huhy.com"    // 发布ws服务的命名空间,此空间默认为当前服务包路径的 "倒写"
        )
@Component
public class MyWebService {

    @WebMethod(exclude=true)  // 默认public方法可以发布为ws服务, 如果要排除则配置  exclude=true
    public String sayHello(String name){
        System.out.println("name:" + name);
        return name + ",你好!";
    }

    //可以指定wsdl中的方法名，参数名和返回值
    @WebMethod(operationName="sayHello1")
    public @WebResult(name="result") String sayHello2(@WebParam(name="name") String name, @WebParam(name="age") int age){
        System.out.println("name:" + name);
        return name + ",你好!,年龄为:" + age;
    }
    @WebMethod  // 默认public方法可以发布为ws服务, 如果要排除则配置  exclude=true
    public String sayHello3(String name,String str){
        System.out.println("name:" + name);
        System.out.println("str:" + str);
        return name + ",你好!";
    }

}
