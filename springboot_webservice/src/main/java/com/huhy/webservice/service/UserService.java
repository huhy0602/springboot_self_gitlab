package com.huhy.webservice.service;

import com.huhy.webservice.dao.UserDao;
import com.huhy.webservice.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author : huhy on 2018/6/19.
 * @Project_name:springboot_self
 * @LOCAL:com.huhy.webservice.service
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@WebService(name = "UserService", // 暴露服务名称
        targetNamespace = "http://service.webservice.huhy.com/"// 命名空间,一般是接口的包名倒序
)
@Component
public class UserService {
    @Autowired
    private UserDao userDao;

    @WebMethod
    public UserEntity queryByid(@WebParam(name = "id")String id){
        return userDao.queryId(id);
    }
}
