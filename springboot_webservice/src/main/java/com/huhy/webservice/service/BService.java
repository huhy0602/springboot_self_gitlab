package com.huhy.webservice.service;

import javax.jws.WebService;

/**
 * @author : huhy on 2018/6/20.
 * @Project_name:springboot_self
 * @LOCAL:com.huhy.webservice.service
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@WebService(name = "BService", // 暴露服务名称
        targetNamespace = "http://service.webservice.huhy.com/"// 命名空间,一般是接口的包名倒序
)
public interface BService {

    public String getStr(String str);
}
