package com.huhy.webservice.service.impl;

import com.huhy.webservice.service.BService;
import org.springframework.stereotype.Component;

import javax.jws.WebService;

/**
 * @author : huhy on 2018/6/20.
 * @Project_name:springboot_self
 * @LOCAL:com.huhy.webservice.service.impl
 * @blog : http://www.cnblog,com/huhongy/
 * @description:{todo}
 */
@WebService(serviceName = "BService", // 与接口中指定的name一致
        targetNamespace = "http://service.webservice.huhy.com", // 与接口中的命名空间一致,一般是接口的包名倒
        endpointInterface = "com.huhy.webservice.service.BService"// 接口地址
)
@Component
public class BServiceImpl implements BService{
    @Override
    public String getStr(String str) {
        return null;
    }
}
