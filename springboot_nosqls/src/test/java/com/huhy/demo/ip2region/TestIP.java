package com.huhy.demo.ip2region;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author : huhy on 2018/10/10.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.ip2region
 * @description:{todo}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestIP {

    @Test
    public  void  testIp(){

        String ip = "120.77.215.143";
        String ip2 = "192.168.30.99";
        String result = IPUtils.getIPInfo(ip);
        String result2 = IPUtils.getIPInfo(ip2);

        System.out.println(result);
        System.out.println(result2);
    }
}
