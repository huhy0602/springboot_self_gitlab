package com.huhy.demo.java8;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;


/**
 * @author : huhy on 2018/9/21.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.java8
 * @description:{todo}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Java8Optional {

    //测试数据
    private String isEmpty= "";
    private String isNull= null;
    private String isStr= "huhy";

    Optional<String> str = Optional.of(isStr);
    Optional<String> empty = Optional.of(isEmpty);
    Optional<String> isNullAble = Optional.ofNullable(isNull);
    //这个在创建就会报NullPointerException错，暂时注掉
    //Optional<String> isnull = Optional.of(isNull);



    /**
     * @author huhy
     * @ClassName:Java8Optional
     * @date 2018/9/21 9:32 
     * @Description: 为非null的值创建一个Optional。
    　　of方法通过工厂方法创建Optional类。需要注意的是，创建对象时传入的参数不能为null。如果传入参数为null，则抛出NullPointerException 。
     */
    @Test
    public void testOf(){
        //调用工厂方法创建Optional实例    获得的是Optional[huhy] 对象
        Optional<String> str = Optional.of(isStr);

        //通过get方法获取optional的值   获取huhy字符串
        System.out.println(Optional.of(isStr).get());

        //传入参数为空字符串，得到Optional[]
        Optional<String> empty = Optional.of(isEmpty);
        System.out.println(empty);

        //传入参数为null，抛出NullPointerException.
        Optional<String> isnull = Optional.of(isNull);

        System.out.println(isnull);
    }
    
    /**
     * @author huhy
     * @ClassName:Java8Optional
     * @date 2018/9/21 9:41 
     * @Description: 为指定的值创建一个Optional，如果指定的值为null，则返回一个空的Optional。
     *
     * 注意，和of方法不同点是  参数可以为空
     *
     * ofNullable的源码如下：
     *          public static <T> Optional<T> ofNullable(T value) {
                        return value == null ? empty() : of(value);
                    }
                public static<T> Optional<T> empty() {
                        @SuppressWarnings("unchecked")
                        Optional<T> t = (Optional<T>) EMPTY;
                        return t;
                    }
            ofnullable 方法在接受null参数时，会调用empty()方法   。返回 empty
     */
    @Test
    public void testOfNull(){
        //接收一个为null参数，返回值是 Optional.empty
        Optional isnull = Optional.ofNullable(isNull);
        System.out.println(isnull);
    }
     /**
      * @author huhy
      * @ClassName:Java8Optional
      * @date 2018/9/21 9:46 
      * @Description: 验证isPresent 方法
      *         如果Optional的值存在返回true，否则返回false
      */   
    @Test
    public void testIsParent(){
        //返回值是true
        System.out.println(str.isPresent());
        //返回值是true
        System.out.println(empty.isPresent());
        //返回值是false
        System.out.println(isNullAble.isPresent());

        //ifPresent方法接受lambda表达式作为参数。
        //lambda表达式对Optional的值调用consumer进行处理。
        str.ifPresent((value) -> {
            System.out.println("The length of the value is: " + value.length());
        });
    }
    /**
     * @author huhy
     * @ClassName:Java8Optional
     * @date 2018/9/21 9:51 
     * @Description: 测试optional的get方法
     *              如果Optional有值则将其返回，否则抛出java.util.NoSuchElementException: No value present
     */
    @Test
    public  void testGet(){
        System.out.println(str.get());
        System.out.println(empty.get()+"-------这是空字符串");
        //要是null值，会报错NoSuchElementException
        System.out.println(isNullAble.get());
    }
    /**
     * @author huhy
     * @ClassName:Java8Optional
     * @date 2018/9/21 10:09 
     * @Description: 测试option的orElse的几个方法
     *     orElse ： 如果有值则将其返回，否则返回指定的其它值。
     *     orElseGet： orElseGet与orElse方法类似，区别在于得到的默认值。orElse方法将传入的字符串作为默认值，orElseGet方法可以接受Supplier接口的实现用来生成默认值
     *     orElseThrow：如果有值则将其返回，否则抛出supplier接口创建的异常
     */
    @Test
    public void testOrElseAndOrElseGetAndOrElseThrow(){
        //如果值不为null，orElse方法返回Optional实例的值。
        //如果为null，返回传入的消息。
        //输出：显示默认值huhy字符串
        System.out.println(isNullAble.orElse("显示默认值huhy字符串"));
        //输出：huhy
        System.out.println(str.orElse("There is some value!"));

        //orElseGet与orElse方法类似，区别在于orElse传入的是默认值，
        //orElseGet可以接受一个lambda表达式生成默认值。
        //输出：Default Value
        System.out.println(isNullAble.orElseGet(() -> "Default Value"));
        //输出：huhy
        System.out.println(str.orElseGet(() -> "Default Value"));

        //orElseThrow 就不再这测试了。去官网看一下就会明白
    }
    
    /**
     * @author huhy
     * @ClassName:Java8Optional
     * @date 2018/9/21 10:14 
     * @Description: 测试map的使用
     *     map:     如果有值，则对其执行调用map函数得到返回值。如果返回值不为null，
     *              则创建包含 mapping返回值 的Optional对象 作为 map方法返回值，否则返回空Optional[]。
     *     flatMap:如果有值，为其执行flatMap函数返回Optional类型返回值，否则返回空Optional。flatMap与map（Funtion）方法类似，
     *              区别在于flatMap中的mapper返回值必须是Optional。调用结束时，flatMap不会对结果用Optional封装。
     */
    @Test
    public  void testMapAndFlatMap(){
        //map方法用来对Optional实例的值执行一系列操作。通过一组实现了Function接口的lambda表达式传入操作
        Optional<String> upperName = str.map((value) -> value.toUpperCase()).map(value -> value.replace('U','d'));
        System.out.println(upperName.orElse("No value found"));

        //flatMap与map（Function）非常类似，区别在于传入方法的lambda表达式的返回类型。
        //map方法中的lambda表达式返回值可以是任意类型，在map函数返回之前会包装为Optional。
        //但flatMap方法中的lambda表达式返回值必须是Optionl实例。
        Optional<String> s = str.flatMap((value) -> Optional.of(value.toUpperCase()));
        System.out.println(s.orElse("No value found"));

        /**
         * 对比，lambda参数区别
         * map 的  (value) -> value.toUpperCase()
         * flatMap的 (value) -> Optional.of(value.toUpperCase()
         * */

    }

    /** optional的简单测试*/

    /**
     * 不为null时进行操作
     */
    @Test
    public  void doNull() {
        if (str != null) {
            System.out.println(str);
        }
    }
    @Test
    public void doNullOptional() {
        Optional.ofNullable(str)
                .ifPresent(System.out::println);
    }


}
