package com.huhy.demo.java8;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * @author : huhy on 2018/9/21.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.java8
 * @description:测试java8的时间类
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class java8Date {
    /**
     * @author huhy
     * @ClassName:java8Date
     * @date 2018/9/25 9:13 
     * @Description: long amountToAdd   代表时间跨度
     */

    @Test
    public void testChromoUnits(){
        //Get the current date
        LocalDate today = LocalDate.now();
        System.out.println("当前时间: " + today);
        //add 1 week to the current date
        LocalDate nextWeek = today.plus(1, ChronoUnit.WEEKS);
        System.out.println("下一周 " + nextWeek);

        LocalDate nextWeek2 = today.plus(2, ChronoUnit.WEEKS);
        System.out.println("下一周2 " + nextWeek2);
        //add 1 month to the current date
        LocalDate nextMonth = today.plus(1, ChronoUnit.MONTHS);
        System.out.println("下一月的今天: " + nextMonth);
        //add 1 year to the current date
        LocalDate nextYear = today.plus(1, ChronoUnit.YEARS);
        System.out.println("下一年: " + nextYear);
        //add 10 years to the current date
        LocalDate nextDecade = today.plus(1, ChronoUnit.DECADES);
        System.out.println("十年之后今天: " + nextDecade);
    }
}
