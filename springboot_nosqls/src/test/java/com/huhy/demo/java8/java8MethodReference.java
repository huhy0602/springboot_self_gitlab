package com.huhy.demo.java8;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import java.util.function.Supplier;

/**
 * @author : huhy on 2018/9/21.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.java8
 * @description:方法引用
 *  在学习lambda表达式之后，我们通常使用lambda表达式来创建匿名方法。然而，有时候我们仅仅是调用了一个已存在的方法。如下:
        Arrays.sort(stringsArray,(s1,s2)->s1.compareToIgnoreCase(s2));
    在Java8中，我们可以直接通过方法引用来简写lambda表达式中已经存在的方法。
        Arrays.sort(stringsArray, String::compareToIgnoreCase);
    这种特性就叫做方法引用(Method Reference)。

    语法：
    方法引用的形式
        方法引用的标准形式是:类名::方法名。（注意：只需要写方法名，不需要写括号）

        有以下四种形式的方法引用:

        类型	                            示例
        引用静态方法	                ContainingClass::staticMethodName
        引用某个对象的实例方法	        containingObject::instanceMethodName
        引用某个类型的任意对象的实例方法	ContainingType::methodName
        引用构造方法	                ClassName::new
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class java8MethodReference {
    Person [] persons = new Person[2];


    /**
     * @author huhy
     * @ClassName:java8MethodReference
     * @date 2018/9/21 16:11 
     * @Description: 测试静态引用
     */
    @Test
    public void testStatic(){
        persons[0] = new Person("yang",LocalDate.now(), Person.Sex.FEMALE,"hhy@163.com");
        persons[1] = new Person("huhy",LocalDate.of(2014, Month.DECEMBER, 12), Person.Sex.FEMALE,"hhy@163.com");
        //使用匿名类
        Arrays.sort(persons, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.birthday.compareTo(o2.birthday);
            }
        });
        //使用lambda表达式
        Arrays.sort(persons, (o1, o2) -> o1.birthday.compareTo(o2.birthday));

        //使用lambda表达式和类的静态方法
        Arrays.sort(persons, (o1, o2) -> Person.compareByAge(o1,o2));

        //使用方法引用
        //引用的是类的静态方法
        Arrays.sort(persons, Person::compareByAge);
        System.out.println(persons[1]);
    }
    /**
     * @author huhy
     * @ClassName:java8MethodReference
     * @date 2018/9/21 16:33 
     * @Description: 引用对象
     */
    @Test
    public  void testObjectReference(){
        persons[0] = new Person("yang",LocalDate.now(), Person.Sex.FEMALE,"hhy@163.com");
        persons[1] = new Person("huhy",LocalDate.of(2014, Month.DECEMBER, 12), Person.Sex.FEMALE,"hhy@163.com");
        ComparisonProvider provider = new ComparisonProvider();

        //使用lambda表达式
        //对象的实例方法
        Arrays.sort(persons,(a,b)->provider.compareByAge(a,b));

        //使用方法引用
        //引用的是对象的实例方法
        Arrays.sort(persons, provider::compareByAge);
        System.out.println();
    }
    /**
     * @author huhy
     * @ClassName:java8MethodReference
     * @date 2018/9/21 16:38 
     * @Description: 引用类型对象的实例方法
     */
    @Test
    public void testClazz(){
        String[] stringsArray = {"Zello","World"};

        //使用lambda表达式和类型对象的实例方法
        Arrays.sort(stringsArray,(s1,s2)->s1.compareToIgnoreCase(s2));
        System.out.println(stringsArray[0]);
        //使用方法引用
        //引用的是类型对象的实例方法
        Arrays.sort(stringsArray, String::compareToIgnoreCase);
    }
    /**
     * @author huhy
     * @ClassName:java8MethodReference
     * @date 2018/9/21 16:48 
     * @Description: 测试构造
     */
    @Test
    public  void testGz(){
        persons[0] = new Person("yang",LocalDate.now(), Person.Sex.FEMALE,"hhy@163.com");
        persons[1] = new Person("huhy",LocalDate.of(2014, Month.DECEMBER, 12), Person.Sex.FEMALE,"hhy@163.com");

        List<Person> personList = Arrays.asList(persons);

        //使用lambda表达式
        Set<Person> personSet = transferElements(personList,()-> new HashSet<>());

        //使用方法引用
        //引用的是构造方法
        Set<Person> personSet2 = transferElements(personList, HashSet::new);
        System.out.println(personSet2.size());
    }
    public static <T, SOURCE extends Collection<T>, DEST extends Collection<T>>
    DEST transferElements(SOURCE sourceColletions, Supplier<DEST> colltionFactory) {
        DEST result = colltionFactory.get();
        for (T t : sourceColletions) {
            result.add(t);
        }
        return result;
    }
}
