package com.huhy.demo.properties;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author : huhy on 2018/9/28.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.properties
 * @description:{todo}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class YmlTest {

    @Autowired
    private YmlYang ymlYang;
    @Test
    public void testYml(){
        System.out.println(ymlYang.getName());
    }
}
