package com.huhy.demo.redis;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author : huhy on 2018/9/21.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.redis
 * @description:测试redis
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTests {

    @Autowired
    private RedisTemplate  redisTemplate;

    @Test
    public  void test(){
        Object huhy = redisTemplate.opsForValue().get("huhy");
        System.out.println(huhy);

    }
}
