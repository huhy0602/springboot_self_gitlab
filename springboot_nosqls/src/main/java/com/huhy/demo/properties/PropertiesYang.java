package com.huhy.demo.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author : huhy on 2018/9/28.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.properties
 * @description:
 *  注意：
 *      1》spring-boot更新到1.5.2版本后locations属性无法使用
 *          @PropertySource注解只可以加载proprties文件,无法加载yaml文件
 *      2》  如果有多个配置文件需要注入，可以用value[]来接收
            @PropertySource(value={"classpath:yang.properties","classpath:yang2.properties"})
        3》文件读取   默认是resource下文件
            @PropertySource("classpath:config/remote.properties") 配置config文件路径
        4》加上encoding = "utf-8"属性防止中文乱码,也可以大写的"UTF-8"
        5》ignoreResourceNotFound = true   扫描文件不存在的处理方式  默认false
 */
@ConfigurationProperties(prefix="huhy")
@PropertySource(value = {"classpath:yang.properties","classpath:yang2.properties"},encoding = "UTF-8",ignoreResourceNotFound = true)
@Data
@Component
public class PropertiesYang {

    private String name;
    private String age;

    /**
     *
     * name的值我们设置的是"classpath:yang.properties","classpath:yang2.properties"。这个值在Springboot的环境中必须是唯一的，如果不设置，
     *  则值为：“class path resource ["classpath:yang.properties","classpath:yang2.properties"]“。
     *  可能很多人比较纳闷，为什么是“class path resource ["classpath:yang.properties","classpath:yang2.properties"]“呢？
     *  这个就涉及到了Spring中对资源文件的封装类Resource。上文我们配置的value值为""classpath:yang.properties","classpath:yang2.properties""，
     *  因此Spring发现是classpath开头的，因此最终使用的是Resource的子类ClassPathResource。如果是file开头的，则最终使用的类是FileSystemResource。
     *  了解了上文所述的Resource类之后。我们再次明确一点，如果@PropertySource中如果没有设置name值，则name值的生成规则是：根据value值查找到最终封装的Resource子类，
     *  然后调用具体的Resource子类实例对象中的getDescription方法，getDescription方法的返回值为最终的name值。
     *  比如ClassPathResource类中的getDescription方法实现如下：
     *  public String getDescription() {
     *          StringBuilder builder = new StringBuilder("class path resource [");
     *          String pathToUse = path;
     *          if (this.clazz != null && !pathToUse.startsWith("/")) {
     *              builder.append(ClassUtils.classPackageAsResourcePath(this.clazz));
     *              builder.append('/');
     *              }
     *           if (pathToUse.startsWith("/")) {
     *              pathToUse = pathToUse.substring(1);
     *              }
     *           builder.append(pathToUse);
     *           builder.append(']');
     *           return builder.toString();}

     * */
}
