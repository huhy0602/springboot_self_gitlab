package com.huhy.demo.properties;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

/**
 * @author : huhy on 2018/9/28.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.properties
 * @description:{todo}
 */
@Component
public class YmlConfig {
    @Bean
    public static PropertySourcesPlaceholderConfigurer properties() {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        //yaml.setResources(new FileSystemResource("yang.yml"));//File引入
        yaml.setResources(new ClassPathResource("yang.yml"));//class引入
        configurer.setProperties(yaml.getObject());
        return configurer;
    }
}
