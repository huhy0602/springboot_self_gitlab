package com.huhy.demo.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @author : huhy on 2018/9/28.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.properties
 * @description:自定义加载yml文件
 *  1> ConfigurationProperties注解的locations属性在1.5.X以后没有了，不能指定locations来加载yml文件
 *      PropertySource注解只支持properties文件加载，详细见官方文档： https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-external-config-yaml-shortcomings
 *  2>
 */
@Data
@Component
@ConfigurationProperties(prefix = "yang")
public class YmlYang{

    private String name;
    private String age;
}
