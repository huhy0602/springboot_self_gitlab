package com.huhy.demo.properties;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * @author : huhy on 2018/10/31.
 * @Project_name:yang_moudles
 * @LOCAL:com.huhy.common.utils
 * @description:通用的验证码
 */
@Component
@Data
@ConfigurationProperties(prefix="code")
@PropertySource(value = {"classpath:code.properties"},encoding = "UTF-8",ignoreResourceNotFound = true)
public class RandomValidateCodeUtil {

    /**图片宽*/
    private int width ;
    /**图片高*/
    private int height ;
    /**干扰线数量*/
    private int lineSize ;
    /**随机产生字符数量*/
    private int stringNum ;



}
