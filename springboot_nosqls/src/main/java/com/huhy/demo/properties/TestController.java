package com.huhy.demo.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author : huhy on 2018/10/31.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.properties
 * @description:{todo}
 */
@RestController
public class TestController {

    @Autowired
    private RandomValidateCodeUtil randomValidateCodeUtil;
    @RequestMapping(value = "/getVerify")
    public String getVerify() {
        return String.valueOf(randomValidateCodeUtil.getHeight());
    }
}
