package com.huhy.demo.rediscluster;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author : huhy on 2018/9/26.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.rediscluster
 * @description:{todo}
 */
public interface UserMapper extends JpaRepository<User,Long> {
}
