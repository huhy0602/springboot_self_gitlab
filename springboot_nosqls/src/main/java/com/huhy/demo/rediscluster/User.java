package com.huhy.demo.rediscluster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author : huhy on 2018/9/26.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.rediscluster
 * @description:{todo}
 */
@Entity
@Table(name="t_user")
@Data
@ToString
@AllArgsConstructor
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private String name;
    private String password;
    private String addr;
    private String phone;
    private int age;
    private String salt;

    public User() {
    }

    public User(String name, String password, String addr, String phone, int age) {
        this.name = name;
        this.password = password;
        this.addr = addr;
        this.phone = phone;
        this.age = age;
    }



}
