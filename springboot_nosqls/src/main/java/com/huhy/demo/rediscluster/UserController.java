package com.huhy.demo.rediscluster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : huhy on 2018/9/26.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.rediscluster
 * @description:{todo}
 */
@RestController
@CacheConfig(cacheNames = "yang")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/findall")
    @Cacheable
    public String getAll(){
        return    userMapper.findAll().toString();
    }
}
