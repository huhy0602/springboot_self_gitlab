package com.huhy.demo.java8;

import lombok.Data;

/**
 * @author : huhy on 2018/9/21.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.java8
 * @description:{todo}
 */
@Data
public class ComparisonProvider {

    public int compareByName(Person a,Person b){
        return a.getName().compareTo(b.getName());
    }

    public int compareByAge(Person a,Person b){
        return a.getBirthday().compareTo(b.getBirthday());
    }
}
