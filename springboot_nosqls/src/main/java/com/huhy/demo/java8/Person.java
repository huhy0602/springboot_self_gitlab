package com.huhy.demo.java8;

import lombok.AllArgsConstructor;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

import java.time.LocalDate;

/**
 * @author : huhy on 2018/9/21.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.java8
 * @description:{todo}
 */
@AllArgsConstructor
@ToString
public class Person {
    public enum Sex{
        MALE,FEMALE
    }

    String name;
    LocalDate birthday;
    Sex gender;
    String emailAddress;

    public String getEmailAddress() {
        return emailAddress;
    }

    public Sex getGender() {
        return gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public String getName() {
        return name;
    }

    public static int compareByAge(Person a,Person b){
        return a.birthday.compareTo(b.birthday);
    }

}
