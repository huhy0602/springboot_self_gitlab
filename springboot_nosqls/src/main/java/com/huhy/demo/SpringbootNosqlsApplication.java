package com.huhy.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
public class SpringbootNosqlsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootNosqlsApplication.class, args);
	}
}
