package com.huhy.demo.system.sys.dao;


import com.huhy.demo.system.sys.entity.SysLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志
 * 
 * @author huhy
 */
@Mapper
public interface SysLogDao extends BaseDao<SysLogEntity> {
	
}
