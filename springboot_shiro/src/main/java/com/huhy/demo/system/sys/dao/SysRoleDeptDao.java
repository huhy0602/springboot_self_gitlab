package com.huhy.demo.system.sys.dao;

import com.huhy.demo.system.sys.entity.SysRoleDeptEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 角色与部门对应关系
 * 
 * @author huhy
 */
@Repository
public interface SysRoleDeptDao extends BaseDao<SysRoleDeptEntity> {
	
	/**
	 * 根据角色ID，获取部门ID列表
	 */
	List<Long> queryDeptIdList(Long roleId);
}
