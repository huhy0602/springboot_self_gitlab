package com.huhy.demo.system.sys.dao;

import com.huhy.demo.system.sys.entity.SysRoleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色管理
 * 
 * @author huhy
 */
@Mapper
public interface SysRoleDao extends BaseDao<SysRoleEntity> {
	

}
