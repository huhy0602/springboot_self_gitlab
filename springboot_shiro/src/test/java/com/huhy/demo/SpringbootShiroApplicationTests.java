package com.huhy.demo;

import com.huhy.demo.system.sys.dao.SysRoleDeptDao;
import com.huhy.demo.system.sys.dao.SysUserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootShiroApplicationTests {
	@Autowired
	private SysUserDao sysUserDao;
	@Test
	public void contextLoads() {
		System.out.println(sysUserDao);
		System.out.println(sysUserDao.queryByUserName("admin"));
	}

}
