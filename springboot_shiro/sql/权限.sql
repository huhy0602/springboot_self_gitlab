/*
Navicat MySQL Data Transfer

Source Server         : huhy
Source Server Version : 50540
Source Host           : localhost:3306
Source Database       : 权限

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2018-08-13 11:10:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级部门ID，一级部门为0',
  `name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='部门管理';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '0', '秦汉集团', '0', '0');
INSERT INTO `sys_dept` VALUES ('2', '1', '武汉分公司', '1', '0');
INSERT INTO `sys_dept` VALUES ('3', '1', '镇江分公司', '2', '0');
INSERT INTO `sys_dept` VALUES ('4', '3', '技术部', '0', '0');
INSERT INTO `sys_dept` VALUES ('5', '3', '销售部', '1', '0');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('3', 'admin', '保存用户', 'com.huhy.demo.system.sys.controller.SysUserController.save()', '{\"userId\":3,\"username\":\"yang\",\"password\":\"c22b83c6c9b22fe577986dca2ab3ce362ad4db4e61473297f2b80a81b9be4f21\",\"salt\":\"jX5AVn6FCDPoB6JjsYHg\",\"email\":\"yang@qq.com\",\"mobile\":\"13712349876\",\"status\":1,\"roleIdList\":[],\"createTime\":\"Aug 13, 2018 10:10:13 AM\",\"deptId\":2,\"deptName\":\"武汉分公司\"}', '210', '0:0:0:0:0:0:0:1', '2018-08-13 10:10:13');
INSERT INTO `sys_log` VALUES ('4', 'admin', '保存角色', 'com.huhy.demo.system.sys.controller.SysRoleController.save()', '{\"roleId\":1,\"roleName\":\"超级管理员\",\"remark\":\"超级管理员，具有系统权限\",\"deptId\":1,\"deptName\":\"秦汉集团\",\"menuIdList\":[1,31,32,33,34,35,3,19,20,21,22,4,23,24,25,26,29],\"deptIdList\":[],\"createTime\":\"Aug 13, 2018 10:11:48 AM\"}', '521', '0:0:0:0:0:0:0:1', '2018-08-13 10:11:48');
INSERT INTO `sys_log` VALUES ('5', 'admin', '保存角色', 'com.huhy.demo.system.sys.controller.SysRoleController.save()', '{\"roleId\":2,\"roleName\":\"超级管理员\",\"remark\":\"超级管理员，具有系统权限\",\"deptId\":1,\"deptName\":\"秦汉集团\",\"menuIdList\":[1,31,32,33,34,35,3,19,20,21,22,4,23,24,25,26,29],\"deptIdList\":[],\"createTime\":\"Aug 13, 2018 10:11:48 AM\"}', '608', '0:0:0:0:0:0:0:1', '2018-08-13 10:11:49');
INSERT INTO `sys_log` VALUES ('6', 'admin', '删除角色', 'com.huhy.demo.system.sys.controller.SysRoleController.delete()', '[2]', '107', '0:0:0:0:0:0:0:1', '2018-08-13 10:12:00');
INSERT INTO `sys_log` VALUES ('7', 'admin', '修改角色', 'com.huhy.demo.system.sys.controller.SysRoleController.update()', '{\"roleId\":1,\"roleName\":\"超级管理员\",\"remark\":\"超级管理员，具有系统权限\",\"deptId\":1,\"deptName\":\"秦汉集团\",\"menuIdList\":[1,2,15,16,17,18,31,32,33,34,35,3,19,20,21,22,4,23,24,25,26,29],\"deptIdList\":[1,2,3,4,5],\"createTime\":\"Aug 13, 2018 10:11:48 AM\"}', '49', '0:0:0:0:0:0:0:1', '2018-08-13 10:12:22');
INSERT INTO `sys_log` VALUES ('8', 'admin', '修改用户', 'com.huhy.demo.system.sys.controller.SysUserController.update()', '{\"userId\":1,\"username\":\"admin\",\"salt\":\"YzcmCZNvbXocrsz9dm8e\",\"email\":\"root@renren.io\",\"mobile\":\"13612345678\",\"status\":1,\"roleIdList\":[1],\"createTime\":\"Nov 11, 2016 11:11:11 AM\",\"deptId\":1,\"deptName\":\"秦汉集团\"}', '79', '0:0:0:0:0:0:0:1', '2018-08-13 10:12:39');
INSERT INTO `sys_log` VALUES ('9', 'admin', '保存角色', 'com.huhy.demo.system.sys.controller.SysRoleController.save()', '{\"roleId\":3,\"roleName\":\"用户\",\"deptId\":2,\"deptName\":\"武汉分公司\",\"menuIdList\":[1,31,32,33,34,3,19,21,4,23,24,25,29],\"deptIdList\":[2,3,4,5],\"createTime\":\"Aug 13, 2018 10:14:40 AM\"}', '56', '0:0:0:0:0:0:0:1', '2018-08-13 10:14:40');
INSERT INTO `sys_log` VALUES ('10', 'admin', '修改角色', 'com.huhy.demo.system.sys.controller.SysRoleController.update()', '{\"roleId\":3,\"roleName\":\"用户\",\"remark\":\"普通用户\",\"deptId\":2,\"deptName\":\"武汉分公司\",\"menuIdList\":[1,31,32,33,34,3,19,21,4,23,24,25,29],\"deptIdList\":[2,3,4,5],\"createTime\":\"Aug 13, 2018 10:14:40 AM\"}', '79', '0:0:0:0:0:0:0:1', '2018-08-13 10:15:07');
INSERT INTO `sys_log` VALUES ('11', 'admin', '修改用户', 'com.huhy.demo.system.sys.controller.SysUserController.update()', '{\"userId\":3,\"username\":\"yang\",\"salt\":\"jX5AVn6FCDPoB6JjsYHg\",\"email\":\"yang@qq.com\",\"mobile\":\"13712349876\",\"status\":1,\"roleIdList\":[3],\"createTime\":\"Aug 13, 2018 10:10:13 AM\",\"deptId\":2,\"deptName\":\"武汉分公司\"}', '79', '0:0:0:0:0:0:0:1', '2018-08-13 10:16:09');
INSERT INTO `sys_log` VALUES ('12', 'yang', '修改菜单', 'com.huhy.demo.system.sys.controller.SysMenuController.update()', '{\"menuId\":2,\"parentId\":1,\"parentName\":\"系统管理\",\"name\":\"用户管理\",\"url\":\"modules/sys/user.html\",\"type\":1,\"icon\":\"fa fa-user\",\"orderNum\":1}', '40', '0:0:0:0:0:0:0:1', '2018-08-13 10:17:49');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', null, null, '0', 'fa fa-cog', '0');
INSERT INTO `sys_menu` VALUES ('2', '1', '用户管理', 'modules/sys/user.html', null, '1', 'fa fa-user', '1');
INSERT INTO `sys_menu` VALUES ('3', '1', '角色管理', 'modules/sys/role.html', null, '1', 'fa fa-user-secret', '2');
INSERT INTO `sys_menu` VALUES ('4', '1', '菜单管理', 'modules/sys/menu.html', null, '1', 'fa fa-th-list', '3');
INSERT INTO `sys_menu` VALUES ('15', '2', '查看', null, 'sys:user:list,sys:user:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '2', '新增', null, 'sys:user:save,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('17', '2', '修改', null, 'sys:user:update,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('18', '2', '删除', null, 'sys:user:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('19', '3', '查看', null, 'sys:role:list,sys:role:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '3', '新增', null, 'sys:role:save,sys:menu:perms', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('21', '3', '修改', null, 'sys:role:update,sys:menu:perms', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('22', '3', '删除', null, 'sys:role:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('23', '4', '查看', null, 'sys:menu:list,sys:menu:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('24', '4', '新增', null, 'sys:menu:save,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('25', '4', '修改', null, 'sys:menu:update,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('26', '4', '删除', null, 'sys:menu:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('29', '1', '系统日志', 'modules/sys/log.html', 'sys:log:list', '1', 'fa fa-file-text-o', '7');
INSERT INTO `sys_menu` VALUES ('31', '1', '部门管理', 'modules/sys/dept.html', null, '1', 'fa fa-file-code-o', '1');
INSERT INTO `sys_menu` VALUES ('32', '31', '查看', null, 'sys:dept:list,sys:dept:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('33', '31', '新增', null, 'sys:dept:save,sys:dept:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('34', '31', '修改', null, 'sys:dept:update,sys:dept:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('35', '31', '删除', null, 'sys:dept:delete', '2', null, '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', '超级管理员，具有系统权限', '1', '2018-08-13 10:11:48');
INSERT INTO `sys_role` VALUES ('3', '用户', '普通用户', '2', '2018-08-13 10:14:40');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='角色与部门对应关系';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES ('1', '1', '1');
INSERT INTO `sys_role_dept` VALUES ('2', '1', '2');
INSERT INTO `sys_role_dept` VALUES ('3', '1', '3');
INSERT INTO `sys_role_dept` VALUES ('4', '1', '4');
INSERT INTO `sys_role_dept` VALUES ('5', '1', '5');
INSERT INTO `sys_role_dept` VALUES ('10', '3', '2');
INSERT INTO `sys_role_dept` VALUES ('11', '3', '3');
INSERT INTO `sys_role_dept` VALUES ('12', '3', '4');
INSERT INTO `sys_role_dept` VALUES ('13', '3', '5');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('35', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('36', '1', '2');
INSERT INTO `sys_role_menu` VALUES ('37', '1', '15');
INSERT INTO `sys_role_menu` VALUES ('38', '1', '16');
INSERT INTO `sys_role_menu` VALUES ('39', '1', '17');
INSERT INTO `sys_role_menu` VALUES ('40', '1', '18');
INSERT INTO `sys_role_menu` VALUES ('41', '1', '31');
INSERT INTO `sys_role_menu` VALUES ('42', '1', '32');
INSERT INTO `sys_role_menu` VALUES ('43', '1', '33');
INSERT INTO `sys_role_menu` VALUES ('44', '1', '34');
INSERT INTO `sys_role_menu` VALUES ('45', '1', '35');
INSERT INTO `sys_role_menu` VALUES ('46', '1', '3');
INSERT INTO `sys_role_menu` VALUES ('47', '1', '19');
INSERT INTO `sys_role_menu` VALUES ('48', '1', '20');
INSERT INTO `sys_role_menu` VALUES ('49', '1', '21');
INSERT INTO `sys_role_menu` VALUES ('50', '1', '22');
INSERT INTO `sys_role_menu` VALUES ('51', '1', '4');
INSERT INTO `sys_role_menu` VALUES ('52', '1', '23');
INSERT INTO `sys_role_menu` VALUES ('53', '1', '24');
INSERT INTO `sys_role_menu` VALUES ('54', '1', '25');
INSERT INTO `sys_role_menu` VALUES ('55', '1', '26');
INSERT INTO `sys_role_menu` VALUES ('56', '1', '29');
INSERT INTO `sys_role_menu` VALUES ('70', '3', '1');
INSERT INTO `sys_role_menu` VALUES ('71', '3', '31');
INSERT INTO `sys_role_menu` VALUES ('72', '3', '32');
INSERT INTO `sys_role_menu` VALUES ('73', '3', '33');
INSERT INTO `sys_role_menu` VALUES ('74', '3', '34');
INSERT INTO `sys_role_menu` VALUES ('75', '3', '3');
INSERT INTO `sys_role_menu` VALUES ('76', '3', '19');
INSERT INTO `sys_role_menu` VALUES ('77', '3', '21');
INSERT INTO `sys_role_menu` VALUES ('78', '3', '4');
INSERT INTO `sys_role_menu` VALUES ('79', '3', '23');
INSERT INTO `sys_role_menu` VALUES ('80', '3', '24');
INSERT INTO `sys_role_menu` VALUES ('81', '3', '25');
INSERT INTO `sys_role_menu` VALUES ('82', '3', '29');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) DEFAULT NULL COMMENT '盐',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'e1153123d7d180ceeb820d577ff119876678732a68eef4e6ffc0b1f06a01f91b', 'YzcmCZNvbXocrsz9dm8e', 'root@renren.io', '13612345678', '1', '1', '2016-11-11 11:11:11');
INSERT INTO `sys_user` VALUES ('2', 'huhy', 'a949b0f46332881d3d6804cf39ca00b8ea8f5edf4963a05f7eb140e17e411f87', 'M6C68izUp5NpdT9hazvB', 'huhy@qq.com', '13978639085', '1', '2', '2018-08-11 16:48:19');
INSERT INTO `sys_user` VALUES ('3', 'yang', 'c22b83c6c9b22fe577986dca2ab3ce362ad4db4e61473297f2b80a81b9be4f21', 'jX5AVn6FCDPoB6JjsYHg', 'yang@qq.com', '13712349876', '1', '2', '2018-08-13 10:10:13');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '3', '3');
