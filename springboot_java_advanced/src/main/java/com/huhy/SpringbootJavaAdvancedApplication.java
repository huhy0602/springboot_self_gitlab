package com.huhy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJavaAdvancedApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJavaAdvancedApplication.class, args);
    }

}

