package com.huhy.thread;

/**
 * @author : huhy on 2018/12/24.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.thread
 * @description:{todo}
 */
public class TestThread extends Thread {

    public TestThread(String name) {
        super(name);
    }
    @Override
    public void run() {
        for(int i = 0;i<10000;i++){
            System.out.println(Thread.currentThread().getName()+" :"+i);
        }
    }

    public static void main(String[] args) throws Exception{
        Thread t1 = new TestThread("huhy");
        Thread t2 = new TestThread("yang");
        t1.start();
        t2.start();
    }
}
