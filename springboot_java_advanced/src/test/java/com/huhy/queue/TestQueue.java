package com.huhy.queue;

import com.huhy.test.Queue;
import com.huhy.test.QueueElement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


/**
 * @author : huhy on 2018/12/26.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.queue
 * @description:验证阻塞队列
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestQueue {

    /**
     * 验证阻塞队列
     */
    @Test
    public  void test1(){
        Queue queue = new Queue(10); //构造一个最多容纳10个元素的队列


        QueueElement element1 = new QueueElement("第一个元素");

        QueueElement element2 = new QueueElement("第二个元素");

        QueueElement element3 = new QueueElement(3);

        QueueElement element4 = new QueueElement(4l);

        QueueElement element5 = new QueueElement(5l);

        QueueElement element6 = new QueueElement(6l);

        QueueElement element7 = new QueueElement(7l);

        QueueElement element8 = new QueueElement(8l);

        QueueElement element9 = new QueueElement(9l);

        QueueElement element10 = new QueueElement(10l);

        QueueElement element11 = new QueueElement(11l);

        System.out.print("初始队列状况 :");       //初始队列状况

        queue.showQueue();

        queue.inQueue(element1); //放入第一个元素

        queue.inQueue(element2); //放入第二个元素

        queue.inQueue(element3); //放入第三个元素

        queue.inQueue(element4); //放入第四个元素

        queue.inQueue(element5); //放入第5个元素

        queue.inQueue(element6); //放入第6个元素

        queue.inQueue(element7); //放入第7个元素

        queue.inQueue(element8); //放入第8个元素

        queue.inQueue(element9); //放入第9个元素

        queue.inQueue(element10); //放入第10个元素

        queue.inQueue(element11); //放入第11个元素



        queue.showQueue();

        System.out.println("队列的队头："+queue.readFront().getValue().toString());

        System.out.println("队列的长度："+queue.length());

        QueueElement element = queue.outQueue();        //出队一次

        System.out.println("第一次出队的元素是："+element.getValue().toString());

        System.out.println("出队后队列的长度："+queue.length());

        element = queue.outQueue();                     //第二次出队

        System.out.println("第二次出队的元素是："+element.getValue().toString());

        System.out.println("出队后队列的长度："+queue.length());

        System.out.print("两次出队之后队列状况:");       //两次出队之后队列状况

        queue.showQueue();

        System.out.println("队列的队头："+queue.readFront().getValue().toString());
    }

    /**
     * @author huhy
     * @ClassName:TestQueue
     * @date 2018/12/26 14:33 
     * @Description: 初步验证队列  String
     */
    @Test
    public void test2() throws InterruptedException {
        int length = 3;
        //创建队列   初始长度为3
        BlockingQueue<String> basket = new ArrayBlockingQueue<String>(length);
        for (int i = 0 ;i < 10;i++){
            if(basket.size() >= length){
                System.out.println("队列已满，阻塞中");
                System.out.println("执行移除操作");
                String take = basket.take();
                System.out.println(take);
            }
            System.out.println("放入队列中");
            basket.put("huhy"+i);
        }
    }

    /**
     * 验证对垒
     */
    @Test
    public  void  test3(){

    }

}
