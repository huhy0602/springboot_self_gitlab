package com.huhy.test;

/**
 * @author : huhy on 2018/12/26.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.test
 * @description:{todo}
 */
public class QueueElement {
    private Object value;             // 队列元素值

    private QueueElement lastElement; // 该元素的上一个元素

    private QueueElement nextElement; // 该元素的下一个元素

    public QueueElement(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public QueueElement getLastElement() {
        return lastElement;
    }

    public void setLastElement(QueueElement lastElement) {
        this.lastElement = lastElement;
    }

    public QueueElement getNextElement() {
        return nextElement;
    }

    public void setNextElement(QueueElement nextElement) {
        this.nextElement = nextElement;
    }
}