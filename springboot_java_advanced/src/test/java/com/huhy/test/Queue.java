package com.huhy.test;

/**
 * @author : huhy on 2018/12/26.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.test
 * @description:{todo}
 */
public class Queue {
    private QueueElement firstElement; // 队列的头元素

    private QueueElement endElement;   // 队列的尾元素

    private int maxLength;             //队列的容量

    private int length;                //当前队列元素个数
    public Queue(int maxlength) {
        super();
        this.maxLength = maxlength;
        this.length = 0;
    }
    public Queue inQueue(QueueElement element) {
        if (this.length == 0) {
            firstElement = element;
            endElement = element;
        }else if(this.length==this.maxLength) {
            System.out.println("队列已满，进队失败！");
        } else {
            element.setLastElement(endElement);
            endElement.setNextElement(element);
            this.endElement = element;
        }
        this.length ++;
        return this;
    }
    public QueueElement outQueue() {
        this.firstElement = this.firstElement.getNextElement();
        this.length --;
        return this.firstElement.getLastElement();
    }
    public QueueElement readFront() {
        return this.firstElement;
    }
    public void showQueue() {
        if (this.length == 0) {
            System.out.println("队列为空！");
            return;
        }
        System.out.println(firstElement.getValue().toString());
        QueueElement temp = firstElement;
        while (temp != endElement) {
            temp = temp.getNextElement();
            System.out.println(temp.getValue().toString());
        }
    }
    public boolean emptyQ() {
        if (firstElement == null) {
            return true;
        }
        return false;
    }
    public boolean fullQ() {
        return this.length==this.maxLength;
    }
    public int length() {
        return this.length;
    }
}
