package com.huhy.demo;

import com.huhy.demo.email.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootEmailApplicationTests {

	@Test
	public void contextLoads() {
	}
	@Autowired
	private MailService mailService;

	//接收邮箱
	private String to = "3403903571@qq.com";

	@Test
	public void sendSimpleMail() {
		mailService.sendSimpleMail(to, "主题：简单邮件", "测试邮件内容");
	}


	@Test
	public void sendHtmlMail() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("time", System.currentTimeMillis());
		model.put("message", "huhy   测试邮件");
		String content = "111111111111111111111111111111111";

		mailService.sendHtmlMail(to, "主题：html邮件", content);
	}

	@Test
	public void sendAttachmentsMail() {
		mailService.sendAttachmentsMail(to, "主题：带附件的邮件", "有附件，请查收！", "C:\\Users\\Administrator\\Desktop\\1.png");
	}

	@Test
	public void sendInlineResourceMail() {
		String rscId = "huhy";
		mailService.sendInlineResourceMail(to,
				"主题：嵌入静态资源的邮件",
				"<html><body>这是有嵌入静态资源：<img src=\'cid:" + rscId + "\' ></body></html>",
				"C:\\Users\\Administrator\\Desktop\\1.png",
				rscId);
	}


}
