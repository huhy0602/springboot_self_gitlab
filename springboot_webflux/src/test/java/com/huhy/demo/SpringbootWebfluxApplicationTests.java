package com.huhy.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootWebfluxApplicationTests {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private MongoDbFactory mongoDbFactory;

	@Test
	public void contextLoads() {

	}
	/**
	 * @author huhy
	 * @ClassName:SpringbootWebfluxApplicationTests
	 * @date 2018/8/15 15:02 
	 * @Description: 生成6位随机数
	 */
	@Test
	public  void testRandom(){
		System.out.println((int)((Math.random()*9+1)*100000));
	}

	@Test
	public void  testMongo(){
		System.out.println(mongoTemplate.getDb().getName());
		System.out.println(mongoTemplate.getCollectionNames().size());
		System.out.println(mongoDbFactory.getDb("huhy").listCollectionNames().first());
		System.out.println(mongoDbFactory.getLegacyDb());
		mongoDbFactory.getDb().createCollection("1222222");
	}

	@Test
	public void webClientTest1() throws InterruptedException {
		WebClient webClient = WebClient.create("http://localhost:7001");   // 1
		Mono<String> resp = webClient
				.get().uri("/hello") // 2
				.retrieve() // 3
				.bodyToMono(String.class);  // 4
		resp.subscribe(System.out::println);    // 5
		TimeUnit.SECONDS.sleep(1);  // 6
	}
}
