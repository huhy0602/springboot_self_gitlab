package com.huhy.demo.webflux.router;

import com.huhy.demo.webflux.handler.HuhyHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * @author : huhy on 2018/8/15.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.router
 * @description:{todo}
 */
@Configuration
public class HuhyRouter {

    /**
     * RouterFunctions 对请求路由处理类，即将请求路由到处理器，这里将一个 GET 请求 /hello路由到处理器
     * HuhyHandler 的 hello方法上。跟 Spring MVC 模式下的 HandleMapping 的作用类似。
     RouterFunctions.route(RequestPredicate, HandlerFunction) 方法，
     对应的入参是请求参数和处理函数，如果请求匹配，就调用对应的处理器函数。
     */

    @Bean
    public RouterFunction<ServerResponse> routeHuhy(HuhyHandler huhyHandler) {
        return RouterFunctions
                .route(RequestPredicates.GET("/helloRoute")
                                .and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),
                        huhyHandler::helloWorld);
    }

}
