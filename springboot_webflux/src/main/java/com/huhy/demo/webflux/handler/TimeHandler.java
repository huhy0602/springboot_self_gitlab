package com.huhy.demo.webflux.handler;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

/**
 * @author : huhy on 2018/8/17.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.test1.handler
 * @description:TimeHandler
 */
@Component
public class TimeHandler {
    /**
     * 注意，此方法应该从数据获取数据进行返回，我只是做测试，就用打印时间代替了
     * */

    /**
     * @author huhy
     * @ClassName:TimeHandler
     * @date 2018/8/17 11:05 
     * @Description: 打印年月日时分秒
     */
    public Mono<ServerResponse> getDate(ServerRequest serverRequest) {
        return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN)
                .body(Mono.just("Today is " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                        .format(new Date())), String.class);
    }
    /**
     * @author huhy
     * @ClassName:TimeHandler
     * @date 2018/8/17 11:05 
     * @Description: 一秒打印一次时间
     */
    public Mono<ServerResponse> sendTimePerSec(ServerRequest serverRequest) {
        return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(
                Flux.interval(Duration.ofSeconds(1)).
                        map(l -> new SimpleDateFormat("HH:mm:ss").format(new Date())),
                String.class);
    }
    /**
     * @author huhy
     * @ClassName:TimeHandler
     * @date 2018/8/17 10:44 
     * @Description: 返回一个字符串
     *
     * mono  返回0-1个
     * flux  0+
     */
    public Mono<ServerResponse> getString(ServerRequest serverRequest){
        return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN)
                .body(Mono.just("huhy"
                        ), String.class);
    }

}
