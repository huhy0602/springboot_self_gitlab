package com.huhy.demo.webflux.router;

import com.huhy.demo.webflux.handler.TimeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

/**
 * @author : huhy on 2018/8/17.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.test1.router
 * @description:{todo}
 */
@Configuration
public class RouterTimter {
    @Autowired
    private TimeHandler timeHandler;
    /**
     * @author huhy
     * @ClassName:RouterTimter
     * @date 2018/8/17 11:04 
     * @Description: 对timeHandle的映射
     * 大家可以发现有两种实现方式
     *       req -> timeHandler.getDate(req)
     *       timeHandler::sendTimePerSec
     */
    @Bean
    public RouterFunction<ServerResponse> timerRouter() {
        return route(GET("/date"), req -> timeHandler.getDate(req))
                // 增加这一行;  // 这种方式相对于上一行更加简洁
                .andRoute(GET("/times"), timeHandler::sendTimePerSec);
    }
    /**
     * @author huhy
     * @ClassName:RouterTimter
     * @date 2018/8/17 11:08 
     * @Description: 这步是测试RouterFunction可以链式直接追加，也可以再写一个，
     */
    @Bean
    public RouterFunction<ServerResponse> getString(){
        return route(GET("/getString"),req -> timeHandler.getString(req));
    }
}
