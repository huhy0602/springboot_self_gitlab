package com.huhy.demo.webflux;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * @author : huhy on 2018/8/17.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.test1
 * @description:{todo}
 */
@RestController
public class HelloController {
    // 返回类型为Mono<String>
    @GetMapping("/hello")
    public Mono<String> hello() {
        //使用Mono.just生成响应式数据
        return Mono.just("webFlux测试  this~s author is huhy~");
    }
}
