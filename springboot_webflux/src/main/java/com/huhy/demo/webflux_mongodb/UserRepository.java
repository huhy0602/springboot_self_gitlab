package com.huhy.demo.webflux_mongodb;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

/**
 * @author : huhy on 2018/8/17.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.webflux_mongodb
 * @description:{todo}
 */
@Repository
public interface UserRepository extends ReactiveMongoRepository<User,String>{
    Mono<User> findByName(String username);
    Mono<Long> deleteByName(String username);
}
