package com.huhy.demo.webflux_mongodb;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * @author : huhy on 2018/8/17.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.webflux_mongodb
 * @description:{todo}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    private String id;
    //name上创建索引,name唯一
    @Indexed(unique = true)
    private String name;
    private int age;
    private String area;
}
