package com.huhy.demo.webflux_mongodb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author : huhy on 2018/8/17.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.webflux_mongodb
 * @description:{todo}
 */
@Service
public class UserHandler {
    @Autowired
    private UserRepository userRepository;

        /** 保存或更新。
            * 如果传入的user没有id属性，由于username是unique的，在重复的情况下有可能报错，
            * 这时找到以保存的user记录用传入的user更新它。
            */
    public Mono<User> save(User user) {
        return userRepository.save(user)
                .onErrorResume(e ->     // 1
                        userRepository.findByName(user.getName())   // 2
                                .flatMap(originalUser -> {      // 4
                                    user.setId(originalUser.getId());
                                    return userRepository.save(user);   // 3
                                }));
    }

    public Mono<Long> deleteByName(String name) {
        return userRepository.deleteByName(name);
    }

    public Mono<User> findByName(String name) {
        return userRepository.findByName(name);
    }

    public Flux<User> findAll() {
        return userRepository.findAll();
    }
}
