package com.huhy.demo.webflux_mongodb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

/**
 * @author : huhy on 2018/8/17.
 * @Project_name:springboot_self_gitlab
 * @LOCAL:com.huhy.demo.webflux_mongodb
 * @description:{todo}
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserHandler userHandler;

    @PostMapping("")
    public Mono<User> save(User user) {
        return this.userHandler.save(user);
    }

    @DeleteMapping("/{name}")
    public Mono<Long> deleteByName(@PathVariable String name) {
        return this.userHandler.deleteByName(name);
    }

    @GetMapping("/{name}")
    public Mono<User> findByName(@PathVariable String name) {
        return this.userHandler.findByName(name);
    }

    @GetMapping(value = "",produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<User> findAll() {
        return this.userHandler.findAll().delayElements(Duration.ofSeconds(1));
    }
}
