package com.huhy.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootWebfluxApplication {

	public static void main(String[] args) throws Exception{
		SpringApplication.run(SpringbootWebfluxApplication.class, args);
	}
}
